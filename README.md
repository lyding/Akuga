# Akuga
A small 2d phantasy "strategy" board game.

All code is published under the GNU General Public License v3.0.
This ist not true for every kind of game asset as images,
sounds, etc. Those game assets are not considered free to use.
