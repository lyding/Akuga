import logging

# Get the logger
logger = logging.getLogger('DatabaseServer.UserCharacteristics')


def get_user_by_name(communicator, client_address, cmd_queue, username):
    """
    Query for the whole user datastructure
    """
    command = ("select * from user_accounts where name=?", (username, ))
    cmd_queue.put((communicator, client_address, command))


def check_username(communicator, client_address, cmd_queue, username):
    """
    Checks if a username is free or not
    """
    command = ("select case when exists (select * from user_accounts where name=?) then cast(1 as bit) else cast(0 as bit) end", (username, ))
    logger.info('Enqueue command from: ' + str(client_address))
    cmd_queue.put((communicator, client_address, command))


def register_user(
        communicator, client_address, cmd_queue,
        username, pass_hash, email_hash, credits, collection,
        fame, exp):
    """
    Registers a user with a given name and pers pass hash in the database
    """
    command = (
        "insert into user_accounts(name, pass_hash, email_hash,"
        "credits, collection, set1, set2, set3, fame, exp, level)"
        "select :name, :pass_hash, :email_hash, :credits, :collection, :set1,\
         :set2, :set3, :fame, :exp, :level where\
        not exists(select 1 from user_accounts where name= :name)", {
            'name': username,
            'pass_hash': pass_hash,
            'email_hash': email_hash,
            'credits': credits,
            'collection': collection,
            'set1': "{}",
            'set2': "{}",
            'set3': "{}",
            'fame': fame,
            'exp': exp,
            'level': 0})
    logger.info('Enqueue command from: ' + str(client_address))
    cmd_queue.put((communicator, client_address, command))


def check_user_credentials(
        communicator, client_address, cmd_queue,
        username, pass_hash):
    """
    Checks the credentials of a user
    """
    command = ("select case when exists (select * from user_accounts where name=? and pass_hash=?) then cast(1 as bit) else cast(0 as bit) end", (username, pass_hash))
    logger.info('Enqueue command from: ' + str(client_address))
    cmd_queue.put((communicator, client_address, command))


def check_user_email(
        communicator, client_address, cmd_queue,
        username, email_hash):
    """
    Checks if the email corresponds to this particular user
    This is neccesarry if a user wants to reset pers password
    """
    command = ("select case when exists (select * from user_accounts where name=? and email_hash=?) then cast(1 as bit) else cast(0 as bit) end", (username, email_hash))
    logger.info('Enqueue command from: ' + str(client_address))
    cmd_queue.put((communicator, client_address, command))


def get_jumon_collection(communicator, client_address, cmd_queue, username):
    """
    Get the collection of a user
    """
    command = (
        "select collection from user_accounts where name=?",
        (username, ))
    cmd_queue.put((communicator, client_address, command))


def update_user(
        communicator, client_address, cmd_queue,
        username, credits, collection, set1, set2, set3, fame, exp, level):
    """
    Update every field except the name and the pass_hash of a user.
    The name of a user cant be changed cause it it used as the id
    and the pass hash should not be send over the network unneccesarily
    """
    try:
        credits_value = int(credits)
    except ValueError:
        logger.info("The credits string was malformed")
        if communicator is not None:
            communicator.send_packet(["ERROR", "A parameter was malformed"])
            return -1

    command = ('''update user_accounts set
        credits=:credits, collection=:collection,
        set1=:set1, set2=:set2, set3=:set3, fame=:fame, exp=:exp, level=:level where name=:name''', {
            'credits': credits_value,
            'collection': collection,
            'set1': set1,
            'set2': set2,
            'set3': set3,
            'fame': fame,
            'exp': exp,
            'level': level,
            'name': username
        })
    cmd_queue.put((communicator, client_address, command))


def reset_password(communicator, client_address,
                   cmd_queue, username, pass_hash):
    """
    Update the pass_hash of a user
    """
    command = ('''update user_accounts set
        pass_hash=:pass_hash where name=:name''', {
            'name': username,
            'pass_hash': pass_hash
        })
    cmd_queue.put((communicator, client_address, command))


def get_jumon_set(communicator, client_address, cmd_queue, username, index):
    """
    Get a jumons set of a user.
    $jumon_set e [0, 1, 2]
    """
    try:
        set_index = int(index)
        # Only 0, 1, 2 is allowed as index
        if set_index < 0 or set_index > 2:
            raise ValueError
    except ValueError:
        logger.info("The index parameter is malformed")
        # If its not a local command
        if communicator is not None:
            communicator.send_packet(["ERROR", "Malformed Parameter"])
        logger.info("Recieved from: " + str(client_address))
        return -1
    command = (
        "select " + ['set1', 'set2', 'set3'][set_index]
        + " from user_accounts where name=?", (username, ))
    cmd_queue.put((communicator, client_address, command))


def set_jumon_set(
        communicator, client_address, cmd_queue,
        username, index, jumon_set):
    """
    Set the jumon set $index to jumon_set
    jumon_set is a string containing the names of all jumons delimited by
    a comma
    """
    try:
        set_index = int(index)
        # Only 0, 1, 2 is allowed as index
        if set_index < 0 or set_index > 2:
            raise ValueError
    except ValueError:
        logger.info("The index parameter is malformed")
        # If its not a local command
        if communicator is not None:
            communicator.send_packet(["ERROR", "Malformed Parameter"])
        logger.info("Recieved from: " + str(client_address))
        return -1
    command = (
        "update user_accounts set " + ['set1', 'set2', 'set3'][set_index]
        + "=? where name=?", (jumon_set, username))
    cmd_queue.put((communicator, client_address, command))


def reward_user(communicator, client_address, cmd_queue, username, credits, fame, exp):
    """
    Increment the credits, fame or exp  of a user by the integer value of credits
    """
    # if one of the paramaters are insecure log it and return
    try:
        credits_int = int(credits)
        fame_int = int(fame)
        exp_int = int(exp)
    except ValueError:
        logger.info("One of the parameter where malformed!")
        logger.info("Received from: " + str(client_address))
        communicator.send_packet(Packet(code=-1, error_message="Malformed Parameter", payload=""))
        return -1
    command = (
        'update user_accounts set credits=credits+?, fame=fame+?, exp=exp+? where name=?',
        (credits_int, fame_int, exp_int, username))
    cmd_queue.put((communicator, client_address, command))
