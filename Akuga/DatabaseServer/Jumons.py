import logging

# Get the logger
logger = logging.getLogger('DatabaseServer.Jumons')


def get_all_jumons(communicator, client_address, cmd_queue):
    """
    Query the database for the all jumon data
    """
    command = ("select * from jumons", ())
    logger.info("Enqueue 'get_all_jumons' from: " + str(client_address))
    cmd_queue.put((communicator, client_address, command))


def get_all_basic_jumon_names(communicator, client_address, cmd_queue):
    """
    Query the database for the the name of all basic jumons
    aka all jumons a user starts with
    """
    command = ("select name from jumons where basic=1", ())
    logger.info(
        "Enqueue 'get_basic_names' from: "
        + str(client_address))
    cmd_queue.put((communicator, client_address, command))


def get_all_vanilla_jumon_stats(communicator, client_address, cmd_queue):
    """
    Query the database for all battle related values of all vanilla jumons
    aka all jumons without a special effect
    """
    command = ("select name, id, tribe, attack, defense "
               "from jumons where rule=''", ())
    logger.info(
        "Enqueue 'get_all_vanilla_jumon_stats' from: "
        + str(client_address))
    cmd_queue.put((communicator, client_address, command))


def get_jumon_by_name(communicator, client_address, cmd_queue, jumon_name):
    """
    Query the database for a jumon by its name
    """
    command = ("select * from jumons where name=?", (jumon_name, ))
    logger.info(
        "Enqueue 'get_jumon_by_name' from: "
        + str(client_address))
    cmd_queue.put((communicator, client_address, command))
