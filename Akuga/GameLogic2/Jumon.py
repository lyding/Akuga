from Akuga.GameLogic2.Position import Position
from Akuga.GameLogic2.AnimationAdvice import (AnimationOnJumon,
    ReduceAttack, IncreaseAttack, ReduceLifepoints, IncreaseLifepoints)

"""
Notes to add a new non vanilla jumon
1. Implement it on the server side
2. Eventually implement it on the client side
3. Add it manually to initialise_jumon_name_constructor_dict
4. Set its rule text in the akuga.db
"""

class Jumon():
    """
    The abstraction class around the Jumon,
    which is the unit to summon by a player
    """
    def __init__(self, name, _id, match_id, color, attack, life_points, owned_by):
        super().__init__()
        self.name = name
        self.id = _id
        self.match_id = match_id
        self.color = color
        self.attack = attack
        self.life_points = life_points
        self.owned_by = owned_by
        self.summoned = False
        # Set an invalid position this will
        # mark this jumon to be not summoned yet
        self.position = Position(-1, -1)

    def reduce_attack(self, amount):
        """
        Use reduce_attack to avoid negative attacks which would heal
        """
        self.attack -= amount
        if self.attack < 0:
            self.attack = 0

    def set_position(self, position):
        self.position = position

    def get_position(self):
        return self.position

    def set_owner(self, owner):
        self.owned_by = owner

    def set_summoned(self, status):
        self.summoned = status

    def get_summoned(self, status):
        return self.summoned

    def get_total_attack(self, arena_tile):
        return self.attack + arena_tile.bonus

    def get_total_life_points(self):
        return self.life_points

    def on_summoning(self, state):
        pass

    def on_movement(self, state):
        pass

    def on_perish(self, state):
        pass

    def on_twotilebattleaftermath(self, state):
        pass


class DrKerze(Jumon):
    def __init__(self, match_id, owned_by):
        super().__init__("Dr. Kerze", 1, match_id, "gal", 2, 3, owned_by)

    def on_twotilebattleaftermath(self, state):
        if self.get_total_life_points() <= 0:
            attacking_jumon = state.state_variables['attacking_jumon'] 
            defending_jumon = state.state_variables['defending_jumon']
            if attacking_jumon is self:
                defending_jumon.attack -= 2
                state.fsm.animation_advices[0].append(AnimationOnJumon(defending_jumon.match_id, "drkerze_perish_animation"))
                state.fsm.animation_advices[0].append(ReduceAttack(defending_jumon.match_id, 2, 1))
                state.fsm.animation_advices[1].append(AnimationOnJumon(defending_jumon.match_id, "drkerze_perish_animation"))
                state.fsm.animation_advices[1].append(ReduceAttack(defending_jumon.match_id, 2, 1))
            else:
                attacking_jumon.attack -= 2
                state.fsm.animation_advices[0].append(AnimationOnJumon(attacking_jumon.match_id, "drkerze_perish_animation"))
                state.fsm.animation_advices[0].append(ReduceAttack(attacking_jumon.match_id, 2, 1))
                state.fsm.animation_advices[1].append(AnimationOnJumon(attacking_jumon.match_id, "drkerze_perish_animation"))
                state.fsm.animation_advices[1].append(ReduceAttack(attacking_jumon.match_id, 2, 1))


class Uzurloc(Jumon):
    def __init__(self, match_id, owned_by):
        super().__init__("Uzurloc", 2, match_id, "evo", 1, 6, owned_by)

    def on_movement(self, state):
        for x in [-1, 0, 1]:
            for y in [-1, 0, 1]:
                try:
                    tile = state.fsm.arena.get_tile_at(self.position + Position(x, y))
                    if tile.get_status() != "wasted" and tile.get_status() != "flourished":
                        tile.set_status("flourished")
                        jumon = tile.occupied_by()
                        if jumon:
                            jumon.attack +=1
                            jumon.life_points += 1
                            state.fsm.animation_advices[0].append(IncreaseLifepoints(jumon.match_id, 1, 1))
                            state.fsm.animation_advices[0].append(IncreaseAttack(jumon.match_id, 1, 1))
                            state.fsm.animation_advices[1].append(IncreaseLifepoints(jumon.match_id, 1, 1))
                            state.fsm.animation_advices[1].append(IncreaseAttack(jumon.match_id, 1, 1))
                except IndexError:
                    pass

class Flowertoad(Jumon):
    def __init__(self, match_id, owned_by):
        super().__init__("Flowertoad", 74, match_id, "evo", 1, 3, owned_by)

    def on_perish(self, state):
        for jumon_match_id in self.owned_by.summoned_jumons:
            # TODO: Also add animation advices
            jumon = state.fsm.jumon_dict[jumon_match_id]
            jumon.life_points += 1


class Georg(Jumon):
    def __init__(self, match_id, owned_by):
        super().__init__("Georg", 13, match_id, "mun", 1, 10, owned_by)

    def on_twotilebattleaftermath(self, state):
        if self.life_points > 0:
            self.attack += 2
            self.life_points += 2
            state.fsm.animation_advices[0].append(IncreaseLifepoints(self.match_id, 2, 2))
            state.fsm.animation_advices[0].append(IncreaseAttack(self.match_id, 2, 2))
            state.fsm.animation_advices[1].append(IncreaseLifepoints(self.match_id, 2, 2))
            state.fsm.animation_advices[1].append(IncreaseAttack(self.match_id, 2, 2))
