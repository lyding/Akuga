"""
This module contain the name_constructor dict and other functions
to build up the pickpool and translate the active sets of all users
to a list of jumon instances
"""
from Akuga.Network import Packet
import Akuga.GameLogic2.Jumon as Jumon
from Akuga.GameLogic2.GlobalDefinitions import USER_DBS_ADDRESS


JUMON_NAME_CONSTRUCTOR_DICT = {}


def get_vanilla_jumons_from_dbs(userdbs_communicator):
    '''
    Read all vanilla jumons from the dbs and create a constructor
    dict out of them. This constructor dict will store preconfigured
    constructor calls which will be used to create vanilla jumon instances
    with different ids.
    '''
    # Get the stats of all vanilla jumons
    userdbs_communicator.send_packet(Packet(command="GET_ALL_VANILLA_JUMON_STATS"))
    response = userdbs_communicator.recv_packet()
    if response.code == -1:
        # If an error is returned just pass it through
        print(response)
        raise Exception("Das erklaert dann mal alles")
    # The response is a list of tuples containing all stats of a jumon in
    # the form: name, color, attack, defense, movement
    # Now add a preconfigured constructor call under the name of the jumon
    jumon_dictionary = {}
    for jumon in response.payload:
        # Here a lambda inside a lambda has to be used as the jumon
        # variable has to be bound, otherwise all lambdaterms would
        # refer to the same (the last) jumon in the response list
        jumon_dictionary[jumon[0]] =\
            (lambda j: lambda match_id, owner: Jumon.Jumon(
                j[0], j[1], match_id, j[2], j[3],
                j[4], owner))(jumon)
    return jumon_dictionary


def initialise_jumon_name_constructor_dict(userdbs_communicator):
    '''
    Initialise the JUMON_NAME_CONSTRUCTOR_DICT
    '''
    global JUMON_NAME_CONSTRUCTOR_DICT
    JUMON_NAME_CONSTRUCTOR_DICT = get_vanilla_jumons_from_dbs(
        userdbs_communicator)
    JUMON_NAME_CONSTRUCTOR_DICT["Dr. Kerze"] = Jumon.DrKerze
    JUMON_NAME_CONSTRUCTOR_DICT["Uzurloc"] = Jumon.Uzurloc
    JUMON_NAME_CONSTRUCTOR_DICT["Flowertoad"] = Jumon.Flowertoad
    JUMON_NAME_CONSTRUCTOR_DICT["Georg"] = Jumon.Georg


if __name__ == '__main__':
    import socket
    from Akuga.Network import StreamSocketCommunicator
    userdbs_connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    userdbs_connection.connect(USER_DBS_ADDRESS)
    communicator = StreamSocketCommunicator(userdbs_connection, 1024)
    initialise_jumon_name_constructor_dict(communicator)
    oro = JUMON_NAME_CONSTRUCTOR_DICT["Oro"](1)
    print(oro.name)
    print(oro.id)
    print(oro.match_id)
    print(oro.attack)
    print(oro.defense)
