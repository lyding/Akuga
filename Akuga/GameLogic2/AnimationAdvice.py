'''
This module defines the animation advices send to the game client
after each round for a smooth animated transition between game states
'''

class SlideJumon:
    '''
    Advice the client to slide a jumon from A to B
    '''
    def __init__(self, jumon_match_id, start_tile, end_tile, duration):
        '''
        :param jumon_match_id: The match id of the jumon
        :param start_tile: The id of arena tile where the jumon starts
        :param end_tile: The id of arena tile where the jumon ends 
        :param duration: duration of the slide in seconds
        '''
        # Convert it into dicts so it can be parsed into .NET objects on the C# side
        self.type = 'SlideJumon'
        self.jumon_match_id = jumon_match_id
        self.start_tile = start_tile 
        self.end_tile = end_tile 
        self.duration = duration


class SlideAlphaValueOfJumon:
    '''
    Advice the client to slide the alpha value of a jumon
    '''
    def __init__(self, jumon_match_id, start_alpha, end_alpha, duration):
        '''
        :param jumon_match_id: The match id of the jumon
        :param start_alpha: The alpha start value
        :param end_alpha: The alpha end value
        :param duration: duration of the slide in seconds
        '''
        self.type = 'SlideAlphaValueOfJumon'
        self.jumon_match_id = jumon_match_id
        self.start_alpha = start_alpha
        self.end_alpha = end_alpha
        self.duration = duration


class DamageJumon:
    '''
    Advice the client to visually damage a jumon
    '''
    def __init__(self, jumon_match_id, damage, duration):
        '''
        :param jumon_match_id: The match id of the jumon
        :param damage: The damage the jumons takes
        :param duration: duration of the slide in seconds
        '''
        self.type = "DamageJumon"
        self.jumon_match_id = jumon_match_id
        self.damage = damage
        self.duration = duration


class RevealAttackBonus:
    '''
    Advice the client to reveal the attack bonus
    '''
    def __init__(self, match_id, attack_bonus):
        self.type = "RevealAttackBonus"
        self.match_id = match_id
        self.attack_bonus = attack_bonus


class FlourishArenaTile:
    '''
    Advice the client to flourishe an arena tile
    '''
    def __init__(self, arena_tile):
        self.type = "FlourishArenaTile"
        self.arena_tile = arena_tile


class WasteArenaTile:
    '''
    Advice the client to flourishe an arena tile
    '''
    def __init__(self, arena_tile):
        self.type = "WasteArenaTile"
        self.arena_tile = arena_tile


class SummonJumon:
    '''
    Advice the client to play the summon animation of a jumon
    '''
    def __init__(self, jumon_match_id, arena_tile_id):
        self.type = 'SummonJumon'
        self.jumon_match_id = jumon_match_id
        self.arena_tile_id = arena_tile_id 

class BuryJumon:
    '''
    Advice the client to play the die animation of a jumon
    '''
    def __init__(self, jumon_match_id):
        self.type = 'BuryJumon'
        self.jumon_match_id = jumon_match_id

class ReduceAttack:
    '''
    Advice the client to play the reduce attack animation
    '''
    def __init__(self, jumon_match_id, amount, duration):
        self.type = 'ReduceAttack'
        self.jumon_match_id = jumon_match_id
        self.amount = amount
        self.duration = duration

class IncreaseAttack:
    '''
    Advice the client to play the increase attack animation
    '''
    def __init__(self, jumon_match_id, amount, duration):
        self.type = 'IncreaseAttack'
        self.jumon_match_id = jumon_match_id
        self.amount = amount
        self.duration = duration

class ReduceLifepoints:
    '''
    Advice the client to play the reduce life points animation
    '''
    def __init__(self, jumon_match_id, amount, duration):
        self.type = 'ReduceLifepoints'
        self.jumon_match_id = jumon_match_id
        self.amount = amount
        self.duration = duration

class IncreaseLifepoints:
    '''
    Advice the client to play the increase lifepoints animation
    '''
    def __init__(self, jumon_match_id, amount, duration):
        self.type = 'IncreaseLifepoints'
        self.jumon_match_id = jumon_match_id
        self.amount = amount
        self.duration = duration

class AnimationOnJumon:
    '''
    Advice the client to play a certain animation ontop of a jumon
    '''
    def __init__(self, jumon_match_id, animation_name):
        '''
        :param animation_name: The name of the animation
        :param duration: duration of the animation in seconds
        '''
        self.type = 'AnimationOnJumon'
        self.jumon_match_id = jumon_match_id
        self.animation_name = animation_name
