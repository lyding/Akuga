import json
from Akuga.Network import Packet
from Akuga.GameLogic2.Position import Position
from Akuga.GameLogic2.StateMachieneState import StateMachieneState
from Akuga.GameLogic2.LastManStanding.CustomJsonEncoder import CustomJsonEncoder
from Akuga.GameLogic2.AnimationAdvice import (
        SlideJumon,
        SlideAlphaValueOfJumon,
        SummonJumon,
        DamageJumon,
        RevealAttackBonus,
        BuryJumon,
        ReduceAttack,
        IncreaseAttack,
        ReduceLifepoints,
        IncreaseLifepoints)


class TurnBeginState(StateMachieneState):
    def __init__(self):
        super().__init__("turn_begin_state")

    def run(self):
        # Set the pass_counter in the first round to 1 so that
        # the first player can only summon one jumon
        # After this two actions are allowed
        pass_counter = 1 if self.fsm.turn_counter == 0 else 0
        if self.fsm.match_phase == "summon":
            return (self.fsm.summoning_idle_for_input_state, {'pass_counter': pass_counter})
        elif self.fsm.match_phase == "battle":
            return (self.fsm.battle_idle_for_input_state, {'pass_counter': pass_counter})
        elif self.fsm.match_phase == "end":
            # Only one action is allowed in the end phase
            return (self.fsm.end_idle_for_input_state, {'pass_counter': 1})
        raise Exception(self.fsm.match_phase + " is not a valid match phase")


class SummoningIdleForInputState(StateMachieneState):
    def __init__(self):
        super().__init__("summoning_idle_for_input_state")

    def run(self):
        pass_counter = self.state_variables['pass_counter']
        current_player = self.fsm.players[self.fsm.player_index]
        print("Waiting for input from " + current_player.name)
        packet = current_player.user.communicator.recv_packet()
        if packet.command == "GET_MATCH_STATE":
            # Send the game state to the client but do nothing else
            # it is free to query the match server for its state
            # and not considered an "action"
            player = self.fsm.players[self.fsm.player_index]
            player_index = self.fsm.player_index
            opponent = self.fsm.players[(self.fsm.player_index + 1) % 2]
            player.user.communicator.send_packet(Packet(code=0, error_message="", payload=self.fsm),
                                                 json_encoder=CustomJsonEncoder(player, player_index, opponent))
            # The GET_MATCH_STATE command is used to get the new match state after the opponents turn
            # With this game state the animation advices are also send to the client and therefor
            # should be deleted, so that the same advices are not send twice
            self.fsm.animation_advices[self.fsm.player_index] = []
            return None
        if packet.command == "MOVE_JUMON":
            match_id = int(packet.jumon_match_id)
            target_arena_tile_id = int(packet.target_arena_tile_id)
            jumon = self.fsm.jumon_dict[match_id]
            if jumon.position == Position(-1, -1):
                return (self.fsm.check_summoning_state,
                        {"jumon": jumon,
                         "target_arena_tile_id": target_arena_tile_id,
                         "pass_counter": self.state_variables['pass_counter']})
            else:
                # Jump to the invalid command state
                # it is illegal to move a jumon while there are jumons to summon
                return (self.fsm.illegal_command_state,
                    {"error_message": "No movement is allowed while in summon phase",
                     "pass_counter": self.state_variables["pass_counter"]})


class BattleIdleForInputState(StateMachieneState):
    def __init__(self):
        super().__init__("battle_idle_for_input_state")

    def run(self):
        pass_counter = self.state_variables['pass_counter']
        current_player = self.fsm.players[self.fsm.player_index]
        print("Waiting for input from " + current_player.name)
        packet = current_player.user.communicator.recv_packet()
        if packet.command == "GET_MATCH_STATE":
            # Send the game state to the client but do nothing else
            # it is free to query the match server for its state
            # and not considered an "action"
            player = self.fsm.players[self.fsm.player_index]
            player_index = self.fsm.player_index
            opponent = self.fsm.players[(self.fsm.player_index + 1) % 2]
            player.user.communicator.send_packet(Packet(code=0, error_message="", payload=self.fsm),
                                                 json_encoder=CustomJsonEncoder(player, player_index, opponent))
            # The GET_MATCH_STATE command is used to get the new match state after the opponents turn
            # With this game state the animation advices are also send to the client and therefor
            # should be deleted, so that the same advices are not send twice
            self.fsm.animation_advices[self.fsm.player_index] = []
            return None

        if pass_counter == 0:
            if packet.command == "MOVE_JUMON":
                match_id = int(packet.jumon_match_id)
                target_arena_tile_id = int(packet.target_arena_tile_id)
                jumon = self.fsm.jumon_dict[match_id]
                if jumon.position == Position(-1, -1):
                    # THIS SHOULD NEVER HAPPEN
                    raise Exception("Match in battle phase while there are jumons to summon")
                else:
                    return (self.fsm.check_movement_state,
                            {"jumon": jumon,
                             "target_arena_tile_id": target_arena_tile_id,
                             "pass_counter": pass_counter})
            else:
                return (self.fsm.illegal_command_state,
                        {'error_message': 'The first action has to be a movement',
                         'pass_counter': pass_counter})
        # This part is reached in the second pass
        if packet.command == "MOVE_JUMON":
            match_id = int(packet.jumon_match_id)
            target_arena_tile_id = int(packet.target_arena_tile_id)
            jumon = self.fsm.jumon_dict[match_id]
            if jumon.position == Position(-1, -1):
                # THIS SHOULD NEVER HAPPEN
                raise Exception("Match in battle phase while there are jumons to summon")
            else:
                return (self.fsm.check_movement_state,
                        {"jumon": jumon,
                         "target_arena_tile_id": target_arena_tile_id,
                         "pass_counter": pass_counter})
        else:
            return (self.fsm.illegal_command_state,
                    {'error_message': packet.command + ' is an invalid command',
                     'pass_counter': pass_counter})


class EndIdleForInputState(StateMachieneState):
    def __init__(self):
        super().__init__("end_idle_for_input_state")

    def run(self):
        pass_counter = self.state_variables['pass_counter']
        current_player = self.fsm.players[self.fsm.player_index]
        print("Waiting for input from " + current_player.name)
        packet = current_player.user.communicator.recv_packet()
        if packet.command == "GET_MATCH_STATE":
            self.fsm.logout_counter += 1
            if self.fsm.logout_counter == len(self.fsm.players):
                player = self.fsm.players[self.fsm.player_index]
                player_index = self.fsm.player_index
                opponent = self.fsm.players[(self.fsm.player_index + 1) % 2]
                player.user.communicator.send_packet(Packet(code=0, error_message="", payload=self.fsm),
                                                     json_encoder=CustomJsonEncoder(player, player_index, opponent))
                # The GET_MATCH_STATE command is used to get the new match state after the opponents turn
                # With this game state the animation advices are also send to the client and therefor
                # should be deleted, so that the same advices are not send twice
                self.fsm.animation_advices[self.fsm.player_index] = []
                return (self.fsm.terminate_match_loop_state, {})
        else:
            return (self.fsm.illegal_command_state,
                    {'error_message': packet.command + ' is an invalid command',
                     'pass_counter': pass_counter})
        # In the end phase the players only have one action left
        # which has to be to query the match state
        return (self.fsm.turn_end_state,
                {'pass_counter' : pass_counter})


class CheckSummoningState(StateMachieneState):
    def __init__(self):
        super().__init__("check_summoning_state")

    def run(self):
        jumon = self.state_variables["jumon"]
        arena_tile_id = self.state_variables["target_arena_tile_id"]
        pass_counter = self.state_variables["pass_counter"]
        # Check if the player owns the jumon
        if jumon.owned_by is not self.fsm.players[self.fsm.player_index]:
            return (self.fsm.illegal_command_state,
                    {"error_mssage": "Target jumon is not owned by you",
                     "pass_counter": pass_counter})

        # Check if target arena tile is part of the arena
        if not self.fsm.arena.is_tile_in_arena(arena_tile_id):
            return (self.fsm.illegal_command_state,
                    {"error_message": "Target arena tile is not in arena",
                     "pass_counter": pass_counter})

        # Check if target arena tile is free
        if self.fsm.arena.get_tile_by_id(arena_tile_id).occupied_by() is not None:
            return (self.fsm.illegal_command_state,
                    {"error_message": "Target arena tile is occupied",
                     "pass_counter": pass_counter})

        return (self.fsm.summoning_state, self.state_variables)


class CheckMovementState(StateMachieneState):
    def __init__(self):
        super().__init__("check_movement_state")

    def run(self):
        jumon = self.state_variables["jumon"]
        arena_tile_id = self.state_variables["target_arena_tile_id"]
        arena_tile = self.fsm.arena.get_tile_by_id(arena_tile_id)
        pass_counter = self.state_variables["pass_counter"]
        # Check if the player owns the jumon
        if jumon.owned_by is not self.fsm.players[self.fsm.player_index]:
            return (self.fsm.illegal_command_state,
                    {"error_message": "You dont control target jumon",
                     "pass_counter": pass_counter})
        # Check if target arena tile is part of the arena
        if not self.fsm.arena.is_tile_in_arena(arena_tile_id):
            return (self.fsm.illegal_command_state,
                    {"error_message": "Target tile is outside the arena",
                     "pass_counter": pass_counter})
        # Check if the movement has a length of one, all jumons have 1 movement
        difference = arena_tile.position - jumon.position
        if abs(difference.x) + abs(difference.y) != 1:
            return (self.fsm.illegal_command_state, {"error_message": "Illegal movement length",
                "pass_counter": pass_counter})

        player = self.fsm.players[self.fsm.player_index]
        occupying_jumon = arena_tile.occupied_by()
        if occupying_jumon is not None:
            if occupying_jumon.owned_by is player:
                return (self.fsm.illegal_command_state,
                        {"error_message": "Target tile is occupied",
                            "pass_counter": pass_counter})
            # If the target arena tile occupied by a jumon
            # of the opponent trigger a two tile battle fight
            return (self.fsm.two_tile_battle_state,
                    {"attacking_jumon": jumon,
                     "defending_jumon": occupying_jumon,
                     "attacker_tile": self.fsm.arena.get_tile_at(jumon.position),
                     "defending_tile": arena_tile,
                     "pass_counter": pass_counter})
        # If the movement is legal and the target arena tile is free
        # just go to the movement state
        return (self.fsm.movement_state, self.state_variables)


class MovementState(StateMachieneState):
    def __init__(self):
        super().__init__("movement_state")

    def run(self):
        jumon = self.state_variables["jumon"]
        target_arena_tile_id = self.state_variables["target_arena_tile_id"]
        pass_counter = self.state_variables["pass_counter"]
        target_arena_tile = self.fsm.arena.get_tile_by_id(target_arena_tile_id)
        original_arena_tile = self.fsm.arena.get_tile_at(jumon.position)

        # Move the jumon
        original_arena_tile.place_jumon(None)
        target_arena_tile.place_jumon(jumon)
        jumon.set_position(target_arena_tile.position)

        # self.fsm.animation_advices[(self.fsm.player_index + 1) % 2].append(SlideJumon(
        #     jumon.match_id,
        #     original_arena_tile.id,
        #     target_arena_tile_id,
        #     0.5))
        self.fsm.animation_advices[0].append(SlideJumon(
            jumon.match_id,
            original_arena_tile.id,
            target_arena_tile_id,
            0.5))
        self.fsm.animation_advices[1].append(SlideJumon(
            jumon.match_id,
            original_arena_tile.id,
            target_arena_tile_id,
            0.5))

        # Evo Jumons get +1/+1 on a flourished tile
        if jumon.color == "evo":
            if original_arena_tile.status != "flourished" and target_arena_tile.status == "flourished":
                jumon.attack += 1
                jumon.life_points += 1
                self.fsm.animation_advices[0].append(IncreaseLifepoints(jumon.match_id, 1, 1))
                self.fsm.animation_advices[0].append(IncreaseAttack(jumon.match_id, 1, 1))
                self.fsm.animation_advices[1].append(IncreaseLifepoints(jumon.match_id, 1, 1))
                self.fsm.animation_advices[1].append(IncreaseAttack(jumon.match_id, 1, 1))
            if original_arena_tile.status == "flourished" and target_arena_tile.status != "flourished":
                jumon.attack -= 1
                jumon.life_points -= 1
                self.fsm.animation_advices[0].append(ReduceLifepoints(jumon.match_id, 1, 1))
                self.fsm.animation_advices[0].append(ReduceAttack(jumon.match_id, 1, 1))
                self.fsm.animation_advices[1].append(ReduceLifepoints(jumon.match_id, 1, 1))
                self.fsm.animation_advices[1].append(ReduceAttack(jumon.match_id, 1, 1))


        jumon.on_movement(self)
        return (self.fsm.turn_end_state, {'pass_counter': pass_counter})


class SummoningState(StateMachieneState):
    def __init__(self):
        super().__init__("summoning_state")

    def run(self):
        jumon = self.state_variables["jumon"]
        arena_tile_id = self.state_variables["target_arena_tile_id"]
        pass_counter = self.state_variables['pass_counter']
        arena_tile = self.fsm.arena.get_tile_by_id(arena_tile_id)
        jumon.set_position(arena_tile.position)
        jumon.set_summoned(True)
        arena_tile.place_jumon(jumon)
        if jumon.color == "evo" and arena_tile.status == "flourished":
            jumon.attack += 1
            jumon.life_points += 1
            self.fsm.animation_advices[0].append(IncreaseLifepoints(jumon.match_id, 1, 1))
            self.fsm.animation_advices[0].append(IncreaseAttack(jumon.match_id, 1, 1))
            self.fsm.animation_advices[1].append(IncreaseLifepoints(jumon.match_id, 1, 1))
            self.fsm.animation_advices[1].append(IncreaseAttack(jumon.match_id, 1, 1))
        jumon.on_summoning(self)
        player = self.fsm.players[self.fsm.player_index]
        player.handle_summoning(jumon)

        self.fsm.animation_advices[(self.fsm.player_index + 1) % 2].append(
            SummonJumon(jumon.match_id, arena_tile_id))

        if len(self.fsm.players[0].jumons_to_summon) + len(self.fsm.players[1].jumons_to_summon) > 0:
            return (self.fsm.turn_end_state, {'pass_counter': pass_counter})
        # If there are no jumons left to summon jump to the end summon phase state
        return (self.fsm.end_summon_phase_state, {'pass_counter': pass_counter})


class TwoTileBattleState(StateMachieneState):
    def __init__(self):
        super().__init__("two_tile_battle_state")

    def run(self):
        attacking_jumon = self.state_variables['attacking_jumon']
        defending_jumon = self.state_variables['defending_jumon']
        attacker_tile = self.state_variables['attacker_tile']
        defending_tile = self.state_variables['defending_tile']
        pass_counter = self.state_variables['pass_counter']

        # If the attacking jumon is owned by the current player per is the attacking
        # player. This is needed to correctly send the animation advices
        attacker_index = self.fsm.player_index\
            if attacking_jumon.owned_by == self.fsm.players[self.fsm.player_index]\
            else (self.fsm.player_index + 1) % 2
        defender_index = (attacker_index + 1) % 2

        # Add the arena tile ids to the list of observed tiles of the attacker
        # this way the players get to know the bonus values of the arena tiles
        if attacker_tile.id not in attacking_jumon.owned_by.observed_tiles:
            attacking_jumon.owned_by.observed_tiles.append(attacker_tile.id)
        if defending_tile.id not in attacking_jumon.owned_by.observed_tiles:
            attacking_jumon.owned_by.observed_tiles.append(defending_tile.id)
        # Add the arena tile ids to the list of observed tiles of the defender
        # this way the players get to know the bonus values of the arena tiles
        if attacker_tile.id not in defending_jumon.owned_by.observed_tiles:
            defending_jumon.owned_by.observed_tiles.append(attacker_tile.id)
        if defending_tile.id not in defending_jumon.owned_by.observed_tiles:
            defending_jumon.owned_by.observed_tiles.append(defending_tile.id)

        self.fsm.animation_advices[0].append(RevealAttackBonus(attacking_jumon.match_id, attacker_tile.bonus))
        self.fsm.animation_advices[0].append(RevealAttackBonus(defending_jumon.match_id, defending_tile.bonus))
        self.fsm.animation_advices[1].append(RevealAttackBonus(attacking_jumon.match_id, attacker_tile.bonus))
        self.fsm.animation_advices[1].append(RevealAttackBonus(defending_jumon.match_id, defending_tile.bonus))

        # Apply the damage
        defending_jumon.life_points -= attacking_jumon.get_total_attack(attacker_tile)
        attacking_jumon.life_points -= defending_jumon.get_total_attack(defending_tile)
        # Advice BOTH clients to play a damage animation
        self.fsm.animation_advices[0].append(DamageJumon(defending_jumon.match_id,
            attacking_jumon.get_total_attack(attacker_tile), 1))
        self.fsm.animation_advices[1].append(DamageJumon(defending_jumon.match_id,
            attacking_jumon.get_total_attack(attacker_tile), 1))

        self.fsm.animation_advices[0].append(DamageJumon(attacking_jumon.match_id,
            defending_jumon.get_total_attack(defending_tile), 1))
        self.fsm.animation_advices[1].append(DamageJumon(attacking_jumon.match_id,
            defending_jumon.get_total_attack(defending_tile), 1))


        return (self.fsm.two_tile_battle_aftermath_state, self.state_variables)
        

class TwoTileBattleAftermathState(StateMachieneState):
    def __init__(self):
        super().__init__("two_tile_battle_aftermath_state")

    def run(self):
        attacking_jumon = self.state_variables['attacking_jumon']
        defending_jumon = self.state_variables['defending_jumon']
        attacker_tile = self.state_variables['attacker_tile']
        defending_tile = self.state_variables['defending_tile']
        pass_counter = self.state_variables['pass_counter']

        attacking_jumon.on_twotilebattleaftermath(self)
        defending_jumon.on_twotilebattleaftermath(self)

        killed_jumons = []
        if attacking_jumon.get_total_life_points() <= 0:
            killed_jumons.append(attacking_jumon)
        if defending_jumon.get_total_life_points() <= 0:
            killed_jumons.append(defending_jumon)
        # If jumons where killed go to the perish state 
        if len(killed_jumons) > 0:
            # Add a variable to the state_variables instead of rebuilding it
            new_state_variables = self.state_variables.copy()
            new_state_variables['killed_jumons'] = killed_jumons
            return (self.fsm.perish_state, new_state_variables)
        return (self.fsm.turn_end_state, {'pass_counter': pass_counter})
        

class PerishState(StateMachieneState):
    def __init__(self):
        super().__init__("perish_state")

    def run(self):
        killed_jumons = self.state_variables['killed_jumons']
        for jumon in killed_jumons:
            # Clear the arena tile and remove the jumon from
            # the list of summoned jumons and add it to the
            # niveana, thats where the jumons go to die...
            self.fsm.animation_advices[0].append(BuryJumon(jumon.match_id))
            self.fsm.animation_advices[1].append(BuryJumon(jumon.match_id))
            self.fsm.arena.place_unit_at(None, jumon.position)
            jumon.on_perish(self)
            jumon.owned_by.handle_jumon_death(jumon)
        return (self.fsm.after_perish_state, self.state_variables)


class AfterPerishState(StateMachieneState):
    def __init__(self):
        super().__init__("after_perish_state")

    def run(self):
        attacking_jumon = self.state_variables['attacking_jumon']
        defending_jumon = self.state_variables['defending_jumon']
        attacker_tile = self.state_variables['attacker_tile']
        defending_tile = self.state_variables['defending_tile']
        killed_jumons = self.state_variables['killed_jumons']
        pass_counter = self.state_variables['pass_counter']
        if attacking_jumon not in killed_jumons and defending_jumon in killed_jumons:
            return (self.fsm.movement_state,
                    {'jumon': attacking_jumon,
                     'target_arena_tile_id': defending_tile.id,
                     'pass_counter': pass_counter})
        return (self.fsm.turn_end_state, {'pass_counter': pass_counter})


class EndSummonPhaseState(StateMachieneState):
    def __init__(self):
        super().__init__("end_summon_phase_state")

    def run(self):
        self.fsm.match_phase = "battle"
        return (self.fsm.turn_end_state, self.state_variables)


class TurnEndState(StateMachieneState):
    def __init__(self):
        super().__init__("turn_end_state")

    def run(self):
        pass_counter = self.state_variables['pass_counter']

        # Check if one of the players lost and therefor if the game ended
        match_ended = False
        if self.fsm.match_phase == "battle":
            for player in self.fsm.players:
                if len(player.summoned_jumons) == 0:
                    player.is_dead = True
                    match_ended = True

        # This should be checked seperated like this, in case the game ended by an alternative win condition
        if match_ended:
            self.fsm.match_phase = "end"

        # If the match ended the second action is ommited
        if pass_counter == 0 and match_ended is False:
            # Send the game state, but do not end the turn, just do the second pass
            player = self.fsm.players[self.fsm.player_index]
            player_index = self.fsm.player_index
            opponent = self.fsm.players[(self.fsm.player_index + 1) % 2]
            player.user.communicator.send_packet(Packet(code=0, error_message="", payload=self.fsm),
                                                 json_encoder=CustomJsonEncoder(player, player_index, opponent))
            # Now after the game state was send, clear the animaion advice list for the current
            # player BUT not for the opponent. This way the current player does not receive the
            # same animation advice twice but the opponent will receive all animation advices in the
            # correct order
            # self.fsm.animation_advices[self.fsm.player_index] = []

            if self.fsm.match_phase == "summon":
                return (self.fsm.summoning_idle_for_input_state,
                        {'pass_counter': pass_counter + 1})
            elif self.fsm.match_phase == "battle":
                return (self.fsm.battle_idle_for_input_state,
                        {'pass_counter': pass_counter + 1})
            elif self.fsm.match_phase == "end":
                # This should not be reached cause of match_ended flag
                pass
            else:
                raise Exception(self.fsm.match_phase + "is an invalid match phase")

        # This part is only reached in the second pass
        # Switch the current player and get player and opponent
        # both are needed by the json encoder
        player = self.fsm.players[self.fsm.player_index]
        player_index = self.fsm.player_index
        opponent = self.fsm.players[(self.fsm.player_index + 1) % 2]

        # Switch the player, this has to happen before the state is send
        # cause the custom json encoder will use it to check if its player A's or player B's turn
        self.fsm.player_index = (self.fsm.player_index + 1) % 2
        player.user.communicator.send_packet(Packet(code=0, error_message="", payload=self.fsm),
                                             json_encoder=CustomJsonEncoder(player, player_index, opponent))
        # Now after the game state was send, clear the animaion advice list for the current
        # player BUT not for the opponent. This way the current player does not receive the
        # same animation advice twice but the opponent will receive all animation advices in the
        # correct order
        self.fsm.animation_advices[player_index] = []
        self.fsm.turn_counter += 1
        return (self.fsm.turn_begin_state, {})


class IllegalCommandState(StateMachieneState):
    def __init__(self):
        super().__init__("illegal_command_state")

    def run(self):
        error_message = self.state_variables['error_message']
        pass_counter = self.state_variables['pass_counter']
        player = self.fsm.players[self.fsm.player_index]
        player.user.communicator.send_packet(Packet(code=-1, error_message=error_message, payload=None))
        if self.fsm.match_phase == "summon":
            return (self.fsm.summoning_idle_for_input_state, 
                    {'pass_counter': pass_counter})
        elif self.fsm.match_phase == "battle":
            return (self.fsm.battle_idle_for_input_state, 
                    {'pass_counter': pass_counter})
        elif self.fsm.match_phase == "end":
            return (self.fsm.end_idle_for_input_state,
                    {'pass_counter': pass_counter})
        else:
            raise Exception(self.fsm.match_phase + " is not a valid match phase")


class TerminateMatchLoopState(StateMachieneState):
    def __init__(self):
        super().__init__("terminate_match_loop_state")

    def run(self):
        print('From TerminateMatchLoopState: set running flag to false')
        self.fsm.running = False
