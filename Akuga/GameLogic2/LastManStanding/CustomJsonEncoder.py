import json
from Akuga.GameLogic2.Jumon import Jumon
from Akuga.GameLogic2.Player import Player
from Akuga.GameLogic2.Arena import ArenaTile, Arena
from Akuga.GameLogic2.StateMachiene import StateMachiene


class CustomJsonEncoder(json.JSONEncoder):
    '''
    This class will override the default
    function to also serialize Jumons, Players
    ArenaTiles and the Arena itself
    '''
    def __init__(self, player, player_index, opponent, **kwargs):
        super(CustomJsonEncoder, self).__init__(**kwargs)
        self.player = player
        self.player_index = player_index
        self.opponent = opponent

    def default(self, o):
        if isinstance(o, Jumon):
            # The client already got a list of all jumon in the loading menu
            # and the fsm contains a dictionary mapping match_ids to jumon_ids
            # so its enough to represent a jumon by its match_id
            tmp_dict = o.__dict__.copy()
            tmp_dict['owned_by'] = tmp_dict['owned_by'].name
            return tmp_dict

        if isinstance(o, Player):
            tmp_dict = o.__dict__.copy()
            # Remove unneeded data
            del tmp_dict['user']
            del tmp_dict['observed_tiles']
            return tmp_dict

        if isinstance(o, ArenaTile):
            tmp_dict = o.__dict__.copy()
            # The position is not needed by the client
            del tmp_dict['position']
            # If the arena tile should be free set the match_id to -1
            # match_ids start at zero so this is easy distinguishable
            # from any valid jumon
            if tmp_dict['_occupied_by'] is None:
                tmp_dict['_occupied_by'] = -1
            else:
                tmp_dict['_occupied_by'] = tmp_dict['_occupied_by'].match_id
            # If the player did not yet observed this arena tile
            # set the bonus value to -2342 which is obviously not
            # a valid bonus
            if o.id not in self.player.observed_tiles:
                tmp_dict['bonus'] = -2342
            return tmp_dict

        if isinstance(o, StateMachiene):
            # Every part of the StateMachiene which has to be send to the game client
            # must be specified here:
            tmp_dict = {}
            tmp_dict['jumon_dict'] = o.jumon_dict
            tmp_dict['player'] = self.player
            tmp_dict['arena'] = o.arena
            tmp_dict['match_phase'] = o.match_phase
            tmp_dict['animation_advices'] = o.animation_advices[self.player_index]
            # Check if the current player in the match state
            # is equivalent to the player attribute of the json encoder
            # This is always the case in the GET_MATCH case but not in the
            # turn_end state
            tmp_dict['your_turn'] = o.players[o.player_index] is self.player

            # Generate an opponent structure which is just an excerpt
            # from the real player structure to hide most of its information
            opponent_dict = {}
            opponent_dict['name'] = self.opponent.name
            opponent_dict['timeout_counter'] = self.opponent.timeout_counter

            tmp_dict['opponent'] = opponent_dict
            return tmp_dict

        # Just dump the dictionary structure
        return o.__dict__
