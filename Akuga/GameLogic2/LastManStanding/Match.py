import socket
import Akuga.JumonSet as JumonSet
from Akuga.GameLogic2.GlobalDefinitions import (
        USER_DBS_ADDRESS,
        CREDITS_PER_WIN)
from Akuga.Network import (Packet, SocketClosed, StreamSocketCommunicator)
import Akuga.GameLogic2.MeepleDict as MeepleDict
from Akuga.GameLogic2.Player import Player
from Akuga.GameLogic2.LastManStanding.ArenaCreator import create_arena
from Akuga.GameLogic2.LastManStanding.CustomJsonEncoder import CustomJsonEncoder
from Akuga.GameLogic2.LastManStanding.LMSStateMachiene import create_last_man_standing_fsm
from time import sleep

def check_set_for_lms(jumon_set):
    return True


def distribute_initial_match_state(player0, player1, fsm):
    # Get the initial GET_STATE command from both players
    # and send them the initial game state

    # I realy dont care at this point if the client sends a correct command...
    packet = player0.user.communicator.recv_packet()
    player0.user.communicator.send_packet(Packet(code=0, error_message="", payload=fsm),
                                         json_encoder=CustomJsonEncoder(player0, 0, player1))
    packet = player1.user.communicator.recv_packet()
    player1.user.communicator.send_packet(Packet(code=0, error_message="", payload=fsm),
                                         json_encoder=CustomJsonEncoder(player1, 1, player0))


def match_server(users, options={}):
    player0 = Player(users[0])
    player1 = Player(users[1])

    print(player0.user.active_set)
    print(player1.user.active_set)
    players = [player0, player1]
    player_index = 0

    # Initialise a connection to the database server
    dbs_connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        dbs_connection.connect(USER_DBS_ADDRESS)
    except ConnectionRefusedError:
        logger.info('Cant connect to akuga database server')
        return -1
    # Create a database communicator from the socket
    dbs_communicator = StreamSocketCommunicator(dbs_connection, 512)
    # Initialise the meeple dict to query the dbs for jumons
    MeepleDict.initialise_jumon_name_constructor_dict(dbs_communicator)

    arena = create_arena(8, 5, 0, 3)
    fsm = create_last_man_standing_fsm(arena, players)

    while not player0.user.get_in_game_scene() or not player1.user.get_in_game_scene():
        sleep(0.1)

    # Now after all users are in the game scene set initiate match to False again
    # This should not change anything as this flag is shadowed in the handle_client
    # function of the game server by the get_in_game_scene flag, but set it to
    # False fore clarity
    for user in users:
        user.set_initiate_match(False)


    distribute_initial_match_state(player0, player1, fsm)

    print("Enter match loop")
    while fsm.running:
        fsm.run()

    # TODO: Send the match result and statistics to the user


    # Reset all flags of all users to reach the original state again
    print("Resetting user status flags")
    for user in users:
        user.set_in_game_scene(False)
        user.set_initiate_match(False)
        user.set_enqueued(False)
