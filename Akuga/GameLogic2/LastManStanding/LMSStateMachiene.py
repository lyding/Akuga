from Akuga.GameLogic2.StateMachiene import StateMachiene
import Akuga.JumonSet as JumonSet
import Akuga.GameLogic2.MeepleDict as MeepleDict
import Akuga.GameLogic2.LastManStanding.LMSStates as LMSStates

def create_last_man_standing_fsm(arena, players):
    fsm = StateMachiene(LMSStates.TurnBeginState())
    fsm.add_state(LMSStates.SummoningIdleForInputState())
    fsm.add_state(LMSStates.BattleIdleForInputState())
    fsm.add_state(LMSStates.EndIdleForInputState())
    fsm.add_state(LMSStates.CheckSummoningState())
    fsm.add_state(LMSStates.SummoningState())
    fsm.add_state(LMSStates.EndSummonPhaseState())
    fsm.add_state(LMSStates.CheckMovementState())
    fsm.add_state(LMSStates.MovementState())
    fsm.add_state(LMSStates.TwoTileBattleState())
    fsm.add_state(LMSStates.TwoTileBattleAftermathState())
    fsm.add_state(LMSStates.PerishState())
    fsm.add_state(LMSStates.AfterPerishState())
    fsm.add_state(LMSStates.TurnEndState())
    fsm.add_state(LMSStates.IllegalCommandState())
    fsm.add_state(LMSStates.TerminateMatchLoopState())

    fsm.add_data("arena", arena)
    fsm.add_data("players", players)
    fsm.add_data("player_index", 0)
    fsm.add_data("turn_counter", 0)
    fsm.add_data("logout_counter", 0)
    # This variable is used in the match module
    # to keep the while loop running
    fsm.add_data("running", True)

    # The list of all jumon instances in the match
    fsm.add_data("jumon_dict", {})
    # A dictionary mapping match ids to jumon ids
    fsm.add_data("jumon_id_map", {})
    # A counter to distribute match ids, this has to
    # be part of the fsm in case that new jumons enter
    # the match e.g. Wastelandlord
    fsm.add_data("jumon_match_id_counter", 0)
    # The match starts in the summon phase
    fsm.add_data("match_phase", "summon")
    # The lists which stores the animation advices which are send with the match state
    fsm.add_data("animation_advices", [])

    for player in players:
        user = player.user
        # Every player has an own animation advices list
        fsm.animation_advices.append([])
        for jumon_name in JumonSet.serialize_set_to_list(user.active_set):
            # Instanciate jumon and add it to the dict of all jumons
            tmp_jumon = MeepleDict.JUMON_NAME_CONSTRUCTOR_DICT[jumon_name](fsm.jumon_match_id_counter, player)
            fsm.jumon_dict[tmp_jumon.match_id] = tmp_jumon
            # Also add the jumon only represented by the match_id to the
            player.jumons_to_summon.append(fsm.jumon_match_id_counter)
            fsm.jumon_match_id_counter += 1
    return fsm
