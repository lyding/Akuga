from random import randint
from Akuga.GameLogic2.Position import Position
from Akuga.GameLogic2.Arena import (ArenaTile, Arena)


def create_arena(width, height, min_bonus, max_bonus):
    """
    Just creates an arena out of a 2d array of tiles
    without any ArenaTile special ability
    """
    tiles = []

    for y in range(height):
        for x in range(width):
            bonus = randint(min_bonus, max_bonus)
            tiles.append(ArenaTile(x + y * width, bonus, Position(x, y)))
    return Arena(tiles, width, height)
