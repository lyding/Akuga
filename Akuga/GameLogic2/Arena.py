from Akuga.GameLogic2.Position import Position
from Akuga.GameLogic2.Jumon import Jumon


class ArenaTile:
    """
    This represents one tile of an arena.
    It stores attack and defense values for the basic colors of jumons
    as a dictionary of the form {$color: ($attack, $defense)}
    as well as persistent and nonpersistent interferences.
    Both are a dict of dicts storing boni like the original boni dict
    under the name of the interference.
    An ArenaTile can have two different special functions for the one tile
    and the two tile battle phase.
    """
    def __init__(self, _id, bonus, position):
        self.id = _id
        self.bonus = bonus
        self.position = position
        self._occupied_by = None
        self.status = ""

    def occupied_by(self):
        """
        Returns the unit aka jumon, equipment ocuppying this tile
        """
        return self._occupied_by

    def get_total_attack_bonus(self, jumon):
        """
        Sum up the attack bonus and all attack interference
        """
        return self.bonus 

    def set_status(self, status):
        """
        Set the status of this tile
        """
        self.status = status

    def get_status(self):
        """
        Return the status of this tile
        """
        return self.status

    def place_jumon(self, jumon):
        """
        Place a jumon at the arena tile
        """
        self._occupied_by = jumon


class Arena:
    """
    This represents the arena, which is made of BOARD_WIDTH * BOARD_HEIGHT
    many ArenaTiles stored in a one dimensional array
    """
    def __init__(self, tiles, board_width, board_height):
        """
        tiles is a 2d list of ArenaTiles
        """
        self.tiles = tiles
        self.board_width = board_width
        self.board_height = board_height
        # Counts the times the arena was shrinked
        self.shrink_counter = 0

    def is_tile_in_arena(self, arena_tile_id):
        """
        Checks whether an tile id is part of the arena
        """
        return arena_tile_id in [tile.id for tile in self.tiles]

    def get_tile_by_id(self, _id):
        """
        Get an arena tile by id
        """
        tmp_list = list(filter(lambda x: x.id==_id, self.tiles))
        if len(tmp_list) > 1:
            raise Exception("Arena tile ids are not unique")
        return tmp_list[0]

    def get_tile_at(self, position):
        """
        Returns the arena tile at the given position
        """
        index = position.x + position.y * self.board_width
        return self.tiles[index]

    def get_unit_at(self, position):
        """
        Get the Unit aka Jumon, Equipment or Trap at position
        Positions outside the arena are always considered free
        """
        try:
            return self.get_tile_at(position).occupied_by()
        except IndexError:
            return None

    def place_unit_at(self, unit, position):
        """
        Places unit at position
        Set unit to None to remove a unit from an arena tile
        """
        if unit is not None:
            """
            If a unit is placed set its position as well
            """
            unit.set_position(position)
        # Set the unit to the position
        self.get_tile_at(position)._occupied_by = unit
