class Player:
    """
    Represents the player of the Akuga game
    """
    def __init__(self, user):
        self.name = user.name
        self.user = user
        self.jumons_to_summon = []
        self.summoned_jumons = []
        self.niveana = []
        # Stores the ids of the arena tiles
        # the player nows the bonus of
        # This way it is possible for one player
        # to know more about the arena than the
        # other player
        self.observed_tiles = []
        self.is_dead = False
        self.timeout_counter = 0

    def set_jumons_to_summon(self, jumons):
        """
        Set the jumons to summon
        """
        self.jumons_to_summon = jumons
        # Set the owner of the jumons
        for j in self.jumons_to_summon:
            j.set_owner(self)

    def add_jumon_to_summon(self, jumon):
        """
        Add a jumon to summon
        """
        jumon.set_owner(self)
        self.jumons_to_summon.append(jumon)

    def owns_tile(self, arena, tile_position):
        """
        Returns if the tile of the arena is already owmed by the player
        Used to check if a move or a summoning is legal for instance
        """
        jumon = arena.get_unit_at(tile_position)
        if jumon is None:
            """
            If the tile is free it cant be owned by the player
            """
            return False
        if jumon in self.summoned_jumons:
            """
            If the jumon at this tile is within the list of summoned jumons
            the player controls a jumon at this tile which means per owns it
            """
            return True
        """
        If the tile is neither empty nor the jumon at this tile is a jumon
        of the current player an other player has to own this field
        """
        return False

    def can_summon(self, jumon):
        """
        Returns if a player can summon a jumon
        aka if the jumon is within the jumons_to_summon list
        """
        return jumon in self.jumons_to_summon

    def controls_jumon(self, jumon):
        """
        Returns wheter a player controls a jumon or not
        aka if the jumon is within the summoned_jumons list
        """
        return jumon in self.summoned_jumons

    def handle_summoning(self, jumon):
        """
        This function is invoked from the state machiene and summons
        a jumon. That means the jumon is moved from
        the jumon_to_summon list to the summone_jumons list
        """
        # Change the jumon from jumons_to_summon to summoned_jumons
        self.summoned_jumons.append(jumon.match_id)
        self.jumons_to_summon.remove(jumon.match_id)

    def handle_jumon_death(self, jumon):
        """
        Handles the death of a jumon, aka removes it from the summoned_jumons
        list an invoke update_player
        """
        self.niveana.append(jumon.match_id)
        self.summoned_jumons.remove(jumon.match_id)

    def increment_timeout_counter(self):
        """
        Increment the timeout counter
        """
        self.timeout_counter += 1

    def get_timeout_counter(self):
        """
        Get the timeout counter
        """
        return self.timeout_counter
