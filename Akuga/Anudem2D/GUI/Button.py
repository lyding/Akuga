'''
This module contains a representation for a button class.
It is very similar to the button layout module but also
contains a texture to render.
'''
from sdl2.events import SDL_MOUSEBUTTONDOWN
from Akuga.Anudem2D.GUI.GUIElement import (
        GUIElement,
        pos_rect_intersect)


class Button(GUIElement):
    '''
    A button is a TexturedSprite observing mouse event,
    therefor it inherits from the TexturedSprite and Observer class.
    '''
    def __init__(self, _id, sprite, callback):
        '''
        The constructor for the Button class

        :param _id: The id of the button
        :param sprite: The sprite of the button
        :param callback: The callback function to invoke
        '''
        super().__init__(_id, sprite, callback)
        self.observer.observe(SDL_MOUSEBUTTONDOWN, self.handle_click)

    def handle_click(self, sdl_event):
        '''
        This function will be used as the callback function for
        click events of the mouse.
        '''
        button_event = sdl_event.button
        pos = (button_event.x, button_event.y)
        if pos_rect_intersect(pos, self.sprite.rectangle[0:4]):
            self.callback(self, button_event.button)


if __name__ == '__main__':
    import sdl2
    import sdl2.ext
    from sdl2 import video
    import glm
    import numpy as np
    from PIL import Image
    from OpenGL.GL import (
        glClear, GL_COLOR_BUFFER_BIT,
        GL_DEPTH_BUFFER_BIT, glBindVertexArray)
    from Akuga.Anudem2D.Util.Shader import load_shader_from_single_file
    from Akuga.Anudem2D.Util.Texture2D import Texture2D
    from Akuga.Anudem2D.Util.EventInterface import\
        convert_sdl_to_anudem_events
    from Akuga.Anudem2D.Drawables.TexturedSprite import\
        TexturedSprite

    sdl2.SDL_Init(sdl2.SDL_INIT_VIDEO)
    window = sdl2.SDL_CreateWindow(
        b"Button Demo",
        sdl2.SDL_WINDOWPOS_UNDEFINED,
        sdl2.SDL_WINDOWPOS_UNDEFINED, 1220, 686,
        sdl2.SDL_WINDOW_OPENGL)
    video.SDL_GL_SetAttribute(video.SDL_GL_CONTEXT_MAJOR_VERSION, 4)
    video.SDL_GL_SetAttribute(video.SDL_GL_CONTEXT_MINOR_VERSION, 5)
    video.SDL_GL_SetAttribute(
        video.SDL_GL_CONTEXT_PROFILE_MASK,
        video.SDL_GL_CONTEXT_PROFILE_CORE)
    context = sdl2.SDL_GL_CreateContext(window)

    # Load a shader programme
    with open('/home/lyding/Akuga/Akuga/Anudem2D/Drawables/shaders/default_sprite.gl') as shader_fd:
        shader = load_shader_from_single_file(shader_fd)

    # Load the image data
    image = Image.open(
        '/home/lyding/Akuga/Akuga/'
        'Anudem2D/GUI/res/card1.png')
    image_data = np.array(list(image.getdata()), dtype=np.uint8)
    texture2d = Texture2D(640, 768, image_data)
    texture2d.init()

    # create the button_sprite
    button_sprite = TexturedSprite(shader, (100, 100, 200, 200, 0), texture2d)

    # Create a button instance
    button1 = Button('card1',
                     button_sprite,
                     lambda self, mode: (
                        print(f'The button {self.id} was'
                              f'pressed with {mode}')))
    button1.init()

    # Set view matrix
    view_matrix = glm.lookAt(
        glm.vec3(0, 0, 0),
        glm.vec3(0, 0, -1),
        glm.vec3(0, 1, 0))
    # projection_matrix = glm.perspective(35.0, 1.0, 0.1, 100.0)
    projection_matrix = glm.ortho(0, 1220, 686, 0)

    shader.use()
    shader.upload_v_matrix(view_matrix)
    shader.upload_p_matrix(projection_matrix)

    running = True
    while running:
        events = sdl2.ext.get_events()
        for event in events:
            if event.type == sdl2.SDL_QUIT:
                running = False
            convert_sdl_to_anudem_events(event)

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        button1.draw()

        glBindVertexArray(0)
        sdl2.SDL_GL_SwapWindow(window)
        sdl2.SDL_Delay(10)

    sdl2.SDL_GL_DeleteContext(context)
    sdl2.SDL_DestroyWindow(window)
    sdl2.SDL_Quit()
