'''
This module contains a class to render text to the screen
'''
from Akuga.Anudem2D.Drawables.TextureAtlas import TextureAtlas


class TextRenderer(TextureAtlas):
    '''
    The TextRenderer extends the TextueAtlas class with methods
    for drawing text, the shader has to be compatible to the
    TextureAtlas and provide a vec3 uniform called 'text_color'
    '''
    def __init__(self, texture2d, info, common, page, glyphs, shader):
        '''
        The constructor of the TextureRenderer.
        Uset the texture atlas to render the position
        '''
        super().__init__(texture2d, {}, shader)
        self.info_metadata = info
        self.common_metadata = common
        self.page_metadata = page
        self.glyphs = glyphs
        self.position = None
        self.text = None
        self.x = None
        self.y = None
        self.scale = None
        self.pixel_width = None
        self.color = None
        # Now go through all glyphes and add theire uvcoord
        # to the uvmap_dict to the glyphs can be used as stamps
        for glyph in glyphs.values():
            self.add_mapping(glyph['id'], glyph['uvmap'])

    def set_text(self, x, y, text, scale, color):
        '''
        Set the text to render
        '''
        self.text = text
        self.stamps = {}
        self.x = x
        self.y = y
        self.scale = scale
        self.pixel_width = 0
        offset_x = self.x
        offset_y = self.y
        for char in self.text:
            if char == '\n':
                offset_y += self.common_metadata['lineHeight'] * self.scale
                offset_x = self.x
                continue
            xpos = offset_x + self.glyphs[char]['xoffset'] * self.scale
            ypos = offset_y + self.glyphs[char]['yoffset'] * self.scale
            self.add_stamp(char, (
                           xpos, ypos, self.glyphs[char]['width'] * self.scale,
                           self.glyphs[char]['height'] * self.scale, 0))
            offset_x += self.glyphs[char]['xadvance'] * scale
            # offset_x cant be used here as this will be reset
            # on every newline character
            self.pixel_width += self.glyphs[char]['xadvance'] * scale
        # We are interested in the width, so substract self.x
        self.pixel_width = self.pixel_width - self.x
        # Set the color of the text
        self.color = color
        self.set_text_color(self.color)


if __name__ == '__main__':
    import sdl2
    from sdl2 import video
    import ctypes
    from PIL import Image
    from Akuga.Anudem2D.Util.Shader import load_shader_from_single_file
    from Akuga.Anudem2D.Util.Texture2D import Texture2D
    from OpenGL.GL import (
        glClear, glClearColor, glEnable, glBlendFunc, GL_COLOR_BUFFER_BIT,
        GL_DEPTH_BUFFER_BIT, glBindVertexArray,
        GL_BLEND, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
    from Akuga.Anudem2D.Util.FNTParser import parse_fnt
    import glm
    import numpy as np

    # Init sdl
    sdl2.SDL_Init(sdl2.SDL_INIT_VIDEO)
    window = sdl2.SDL_CreateWindow(
        b"Drawable Demo",
        sdl2.SDL_WINDOWPOS_UNDEFINED,
        sdl2.SDL_WINDOWPOS_UNDEFINED, 800, 600,
        sdl2.SDL_WINDOW_OPENGL)
    video.SDL_GL_SetAttribute(video.SDL_GL_CONTEXT_MAJOR_VERSION, 4)
    video.SDL_GL_SetAttribute(video.SDL_GL_CONTEXT_MINOR_VERSION, 5)
    video.SDL_GL_SetAttribute(
        video.SDL_GL_CONTEXT_PROFILE_MASK,
        video.SDL_GL_CONTEXT_PROFILE_CORE)
    context = sdl2.SDL_GL_CreateContext(window)

    # Load a shader programme
    with open('/home/lyding/Akuga/Akuga/Anudem2D/'
              'Drawables/shaders/text_renderer.gl') as shader_fd:
        shader = load_shader_from_single_file(shader_fd)

    # Load the image data
    image = Image.open(
        '/home/lyding/Akuga/Akuga/Anudem2D/GUI/res/booter2.png')
    image_data = np.array(list(image.getdata()), dtype=np.uint8)
    texture2d = Texture2D(256, 256, image_data)
    texture2d.init()

    # Load the glyphs from the fnt file
    with open(
            '/home/lyding/Akuga/Akuga/'
            '/Anudem2D/GUI/res/booter2.fnt') as fnt_fd:
        info, common, page, glyphs = parse_fnt(fnt_fd)

    text_renderer = TextRenderer(texture2d, info, common, page, glyphs, shader)
    text_renderer.set_text(100, 100, 'Fleischige\nDigglets', 1, (1, 0.5, 0.25))
    text_renderer.generate_and_init()

    # Set view matrix
    view_matrix = glm.lookAt(
        glm.vec3(0, 0, 0),
        glm.vec3(0, 0, -1),
        glm.vec3(0, 1, 0))
    # projection_matrix = glm.perspective(35.0, 1.0, 0.1, 100.0)
    projection_matrix = glm.ortho(0, 400, 400, 0)
    model_matrix = glm.mat4(1)

    shader.use()
    shader.upload_v_matrix(view_matrix)
    shader.upload_p_matrix(projection_matrix)

    running = True
    event = sdl2.SDL_Event()
    glEnable(GL_BLEND)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
    while running:
        while sdl2.SDL_PollEvent(ctypes.byref(event)) != 0:
            if event.type == sdl2.SDL_QUIT:
                running = False
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glClearColor(1, 1, 0, 0)

        text_renderer.draw()

        glBindVertexArray(0)
        sdl2.SDL_GL_SwapWindow(window)
        sdl2.SDL_Delay(10)
    sdl2.SDL_GL_DeleteContext(context)
    sdl2.SDL_DestroyWindow(window)
    sdl2.SDL_Quit()
