'''
This module contains the MenuStack and Submenu class.
'''


class MenuStack():
    '''
    The MenuStack is a stack used for handling submenus.
    '''

    def __init__(self, submenus, start_submenu):
        '''
        The constructor of the MenuStack class

        :param submenus: A list of all possilble sub_menus
        :param start_submenu: The submenu to start with
                              should be part of submenu
        '''
        self.submenus = submenus
        self.stack = []
        if start_submenu:
            self.push(start_submenu)

    def draw(self):
        '''
        Draw the uppermost menu.
        Draw underlying menus as well if the uppermost menus
        are marked as transparent
        '''
        index = len(self.stack) - 1
        while self.stack[index].transparent and index > 0:
            index -= 1
        for m in self.stack[index:]:
            m.draw()

    def push(self, submenu):
        '''
        Activate the submenu delayed and push it to the stack
        '''
        if len(self.stack) > 0:
            self.stack[-1].deactivate()
        # The delayed activation has to be used here, otherwise
        # the pushed submenu would also handel the mouse event
        # wich lead to this push
        submenu.activate_delayed()
        self.stack.append(submenu)

    def pop(self):
        '''
        Deactivate the uppermost submenu and pop it from the stack
        '''
        self.stack[-1].deactivate()
        self.stack.pop()
        self.stack[-1].activate()


class Submenu():
    '''
    The submenu class represents an element of a menu.
    While the menu class represents the whole menu a part
    of a menu as for example the main menu the game starts with
    would be a submenu.
    A submenu contains a bunch of gui elements responsive to user
    interface elements. Those gui_elements are firing events
    on theire own which have to be observed by the submenu
    again and handeld accordingly. This might be a strange
    approach to submenus as the submenus fires the events and handels
    them again. Callbackfunction might be easier, lets see how
    this way works out.
    '''
    def __init__(
            self, menu_stack, gui_elements,
            passive_elements, transparent=False):
        '''
        The constructor for the submenu

        :param menu: The menu this submenu is a part of
        :param passive_elements: The list of passive
                                 elements the submenu contains
        :param gui_elements: The list of gui_elements the submenu contains
        '''
        self.menu = menu
        self.gui_elements = gui_elements
        self.passive_elements = passive_elements
        self.transparent = transparent

    def add_gui_element(self, gui_element):
        '''
        Add a gui element to the list of gui elements
        '''
        self.gui_elements.append(gui_element)

    def add_passive_element(self, passive_element):
        '''
        Add a passive element to the list of passive
        '''
        self.passive_elements.append(passive_element)

    def draw(self):
        '''
        Go through all the gui elements and draw them
        '''
        # First draw the passive elements as the gui elements
        # are considered more important and should not
        # be overdrawn
        for element in self.passive_elements:
            element.draw()
        for element in self.gui_elements:
            element.draw()

    def activate(self):
        '''
        Go through all gui elements and activate them
        so that they are responsive all following events
        '''
        for element in self.gui_elements:
            element.observer.activate()

    def activate_delayed(self):
        '''
        Go through all gui elements and activate them delayed
        so that they are responsive all following events
        '''
        for element in self.gui_elements:
            element.observer.activate_delayed()

    def deactivate(self):
        '''
        Go through all gui elements and dactivate them
        so that they are unresponsive to events
        '''
        for element in self.gui_elements:
            element.observer.deactivate()


if __name__ == '__main__':
    import sdl2
    import sdl2.ext
    from sdl2 import video
    import glm
    import numpy as np
    from PIL import Image
    from OpenGL.GL import (
        glClear, GL_COLOR_BUFFER_BIT,
        GL_DEPTH_BUFFER_BIT, glBindVertexArray)
    from Akuga.Anudem2D.GUI.ButtonLayout import ButtonLayout
    from Akuga.Anudem2D.Util.Shader import Shader
    from Akuga.Anudem2D.Util.Texture2D import Texture2D
    from Akuga.Anudem2D.Drawables.TexturedSprite import\
        TexturedSprite
    from Akuga.Anudem2D.Util.EventInterface import\
        convert_sdl_to_anudem_events

    sdl2.SDL_Init(sdl2.SDL_INIT_VIDEO)
    window = sdl2.SDL_CreateWindow(
        b"Button Demo",
        sdl2.SDL_WINDOWPOS_UNDEFINED,
        sdl2.SDL_WINDOWPOS_UNDEFINED, 1220, 686,
        sdl2.SDL_WINDOW_OPENGL)
    video.SDL_GL_SetAttribute(video.SDL_GL_CONTEXT_MAJOR_VERSION, 4)
    video.SDL_GL_SetAttribute(video.SDL_GL_CONTEXT_MINOR_VERSION, 5)
    video.SDL_GL_SetAttribute(
        video.SDL_GL_CONTEXT_PROFILE_MASK,
        video.SDL_GL_CONTEXT_PROFILE_CORE)
    context = sdl2.SDL_GL_CreateContext(window)

    # Load a shader programme
    shader = Shader(
        open(
            '/home/lyding/Akuga/Akuga/'
            'Anudem2D/Drawables/shaders/textured_sprite_vs.gl').read(),
        open(
            '/home/lyding/Akuga/Akuga/'
            'Anudem2D/Drawables/shaders/textured_sprite_fs.gl').read())

    class MainMenu1(Submenu):
        def __init__(
                self, menu):
            super().__init__(menu, [], [])
            # Load the image data
            bg_image = Image.open(
                '/home/lyding/Akuga/Akuga/'
                'Anudem2D/GUI/res/background.png')
            bg_image_data = np.array(list(bg_image.getdata()), dtype=np.uint8)
            bg_texture2d = Texture2D(1220, 686, bg_image_data)
            bg_texture2d.init()

            # Create the sprite
            bg_sprite = TexturedSprite(
                shader, (0, 0, 1220, 686, 0), bg_texture2d)
            bg_sprite.init()
            # Create the button layout
            layout = [
                ('main_start', 145, 0, 330 - 154, 70, self.handle_click),
                ('main_sets', 330, 0, 515 - 330, 70, self.handle_click),
                ('main_shop', 515, 0, 700 - 515, 70, self.handle_click),
                ('main_options', 700, 0, 887 - 700, 70, self.handle_click),
                ('main_quit', 887, 0, 1027 - 887, 70, self.handle_click)]
            main_button_layout = ButtonLayout(0, layout)
            self.gui_elements.append(main_button_layout)
            self.passive_elements.append(bg_sprite)

        def handle_click(self, button, mode):
            print('Push MainMenu2')
            self.menu.push(self.menu.MainMenu2)

    class MainMenu2(Submenu):
        def __init__(
                self, menu):
            super().__init__(menu, [], [])
            # Load the image data
            bg_image = Image.open(
                '/home/lyding/Akuga/Akuga/'
                'Anudem2D/GUI/res/background.png')
            bg_image_data = np.array(list(bg_image.getdata()), dtype=np.uint8)
            bg_texture2d = Texture2D(1220, 686, bg_image_data)
            bg_texture2d.init()

            # Create the sprite
            bg_sprite = TexturedSprite(
                shader, (0, 0, 1220, 686, 0), bg_texture2d)
            bg_sprite.init()
            # Create the button layout
            layout = [
                ('main2_start', 145, 0, 330 - 154, 70, self.handle_click),
                ('main2_sets', 330, 0, 515 - 330, 70, self.handle_click),
                ('main2_shop', 515, 0, 700 - 515, 70, self.handle_click),
                ('main2_options', 700, 0, 887 - 700, 70, self.handle_click),
                ('main2_quit', 887, 0, 1027 - 887, 70, self.handle_click)]
            main_button_layout = ButtonLayout(1, layout)
            self.gui_elements.append(main_button_layout)
            self.passive_elements.append(bg_sprite)

        def handle_click(self, button, mode):
            if button[0] == 'main2_quit':
                print('Pop MainMenu2')
                self.menu.pop()

    menu = MenuStack([], None)
    menu.MainMenu1 = MainMenu1(menu)
    menu.MainMenu2 = MainMenu2(menu)
    menu.MainMenu2.deactivate()
    menu.push(menu.MainMenu1)

    # Set view matrix
    view_matrix = glm.lookAt(
        glm.vec3(0, 0, 0),
        glm.vec3(0, 0, -1),
        glm.vec3(0, 1, 0))
    # projection_matrix = glm.perspective(35.0, 1.0, 0.1, 100.0)
    projection_matrix = glm.ortho(0, 1220, 686, 0)

    shader.use()
    shader.upload_v_matrix(view_matrix)
    shader.upload_p_matrix(projection_matrix)

    running = True
    event = sdl2.SDL_Event()
    while running:
        events = sdl2.ext.get_events()
        for event in events:
            if event.type == sdl2.SDL_QUIT:
                running = False
            convert_sdl_to_anudem_events(event)

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        menu.draw()

        glBindVertexArray(0)
        sdl2.SDL_GL_SwapWindow(window)
        sdl2.SDL_Delay(10)

    sdl2.SDL_GL_DeleteContext(context)
    sdl2.SDL_DestroyWindow(window)
    sdl2.SDL_Quit()
