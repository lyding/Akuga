'''
This module contains a representation of a invisible layout
to create invisible buttons or create buttons as an overlay over
a larger image.
'''
from sdl2.events import SDL_MOUSEBUTTONDOWN
from Akuga.Anudem2D.GUI.GUIElement import (
        GUIElement,
        pos_rect_intersect)


class ButtonLayout(GUIElement):
    '''
    The buttonlayout class is an invisible gui element which can be used
    to create buttons from e.g. a background image which contains images
    of buttons.
    '''
    def __init__(self, _id, layout):
        '''
        The constructor for the ButtonLayout class.
        :param _id: The id of the layout
        :param layout: The layout describing the buttons.
                       It is a list of tuples of the form
                       (id, x, y, width, height, callback)
        '''
        super().__init__(_id, None, None)
        self.button_layout = layout
        self.observer.observe(SDL_MOUSEBUTTONDOWN, self.handle_click)

    def init(self):
        '''
        A TextBox does not have components to initialise.
        Therefor do nothing
        '''
        pass

    def full_init(self):
        '''
        A TextBox does not have components to initialise.
        Therefor do nothing
        '''
        pass

    def handle_click(self, sdl_event):
        '''
        This function will be used as the callback function for
        click events of the mouse.
        '''
        button_event = sdl_event.button
        pos = (button_event.x, button_event.y)
        for button in self.button_layout:
            if pos_rect_intersect(pos, button[1:5]):
                button[5](button, button_event.button)

    def draw(self):
        '''
        The draw function is needed to allow the buttonlayout
        to be part of the gui_elements list of a menu.
        But as the buttolayout class is considered to be invisible
        the draw function is empty
        '''
        pass


if __name__ == "__main__":
    import sdl2
    import sdl2.ext
    from sdl2 import video
    import glm
    import numpy as np
    from PIL import Image
    from OpenGL.GL import (
        glClear, GL_COLOR_BUFFER_BIT,
        GL_DEPTH_BUFFER_BIT, glBindVertexArray)
    from Akuga.Anudem2D.Util.Shader import Shader
    from Akuga.Anudem2D.Util.Texture2D import Texture2D
    from Akuga.Anudem2D.Util.EventInterface import\
        convert_sdl_to_anudem_events
    from Akuga.Anudem2D.Drawables.TexturedSprite import\
        TexturedSprite

    sdl2.SDL_Init(sdl2.SDL_INIT_VIDEO)
    window = sdl2.SDL_CreateWindow(
        b"Animated Sprite Demo",
        sdl2.SDL_WINDOWPOS_UNDEFINED,
        sdl2.SDL_WINDOWPOS_UNDEFINED, 1220, 686,
        sdl2.SDL_WINDOW_OPENGL)
    video.SDL_GL_SetAttribute(video.SDL_GL_CONTEXT_MAJOR_VERSION, 4)
    video.SDL_GL_SetAttribute(video.SDL_GL_CONTEXT_MINOR_VERSION, 5)
    video.SDL_GL_SetAttribute(
        video.SDL_GL_CONTEXT_PROFILE_MASK,
        video.SDL_GL_CONTEXT_PROFILE_CORE)
    context = sdl2.SDL_GL_CreateContext(window)

    # Load a shader programme
    shader = Shader(
        open(
            '/home/lyding/Akuga/Akuga/'
            'Anudem2D/Drawables/shaders/textured_sprite_vs.gl').read(),
        open(
            '/home/lyding/Akuga/Akuga/'
            '/Anudem2D/Drawables/shaders/textured_sprite_fs.gl').read())

    # Load the image data
    image = Image.open(
        '/home/lyding/Akuga/Akuga/Anudem2D/GUI/res/background.png')
    image_data = np.array(list(image.getdata()), dtype=np.uint8)
    texture2d = Texture2D(1220, 686, image_data)
    texture2d.init()

    # Create the sprite
    sprite1 = TexturedSprite(shader, (0, 0, 1220, 686, 0), texture2d)
    # Init the sprite
    sprite1.init()

    def cb(self, mode):
        print(f'Button {self[0]} was pressed with {mode}')

    # Create the button layout
    layout = [
        ('start', 145, 0, 330 - 154, 100, cb),
        ('sets', 330, 0, 515 - 330, 100, cb),
        ('shop', 515, 0, 700 - 515, 100, cb),
        ('options', 700, 0, 887 - 700, 100, cb),
        ('quit', 887, 0, 1027 - 887, 100, cb)]
    button_layout = ButtonLayout(0, layout)
    # Only here for test coverage should not be invoked
    # if not for compatibility reasons with other GUIElements
    button_layout.init()

    # Set view matrix
    view_matrix = glm.lookAt(
        glm.vec3(0, 0, 0),
        glm.vec3(0, 0, -1),
        glm.vec3(0, 1, 0))
    # projection_matrix = glm.perspective(35.0, 1.0, 0.1, 100.0)
    projection_matrix = glm.ortho(0, 1220, 686, 0)

    shader.use()
    shader.upload_v_matrix(view_matrix)
    shader.upload_p_matrix(projection_matrix)

    running = True
    while running:
        events = sdl2.ext.get_events()
        for event in events:
            if event.type == sdl2.SDL_QUIT:
                running = False
            convert_sdl_to_anudem_events(event)

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        sprite1.draw()

        glBindVertexArray(0)
        sdl2.SDL_GL_SwapWindow(window)
        sdl2.SDL_Delay(10)

    sdl2.SDL_GL_DeleteContext(context)
    sdl2.SDL_DestroyWindow(window)
    sdl2.SDL_Quit()
