'''
This module contains a representation for an invisible texbox class.
It is the textbox analogon of the button layout
'''
from sdl2 import (
        SDL_MOUSEBUTTONDOWN,
        SDL_KEYDOWN,
        SDL_TEXTINPUT,
        SDL_TEXTEDITING,
        SDLK_BACKSPACE,
        SDLK_RETURN,
        SDLK_RETURN2,
        SDL_StartTextInput,
        SDL_StopTextInput)
from Akuga.Anudem2D.GUI.GUIElement import (
        GUIElement,
        pos_rect_intersect)
from Akuga.Anudem2D.Util.ObserverPattern import Observer


class TextBoxLayout(GUIElement):
    '''
    A TextBoxLayout is an invisible GUI element for text input
    '''
    def __init__(self, _id, rect, text_renderer,
                 label='',
                 mask=None,
                 baseline_factor=0.8,
                 text_offsetx=0,
                 text_offsety=0,
                 text_limit=-1,
                 text_color=(0, 0, 0)):
        '''
        The constructor of the TextBoxLayout

        :param _id: The id of the textbox
        :param rect: The rectangle describing the shape of the textbox layout
        :param text_renderer: The textrenderer to render text
        '''
        super().__init__(_id, None, None)
        self.rect = rect
        self.text_renderer = text_renderer
        self.label = label
        self.mask = mask
        self.baseline_factor = baseline_factor
        self.text_offsetx = text_offsetx
        self.text_offsety = text_offsety
        self.text_limit = text_limit
        self.text_color = text_color
        self.active = False
        self.value = ""
        self.observer.observe(SDL_MOUSEBUTTONDOWN, self.handle_click)
        # It looks better with base then with lineHeight
        self.text_scale = self.rect[3] /\
            self.text_renderer.common_metadata['base']
        self.text_posx = self.text_offsetx + self.rect[0]
        # Set the y position in a way, that the base is at $baseline_factor
        # of the rectangle
        self.text_posy = self.text_offsety + self.rect[1] +\
            self.rect[3] * self.baseline_factor -\
            self.text_renderer.common_metadata['base'] * self.text_scale
        self.text_renderer.set_text(
                self.text_posx,
                self.text_posy,
                self.label,
                self.text_scale,
                self.text_color)
        self.text_renderer.generate()

    def init(self):
        '''
        Init all ocmponents of the TextBox which have to be initialised
        '''
        # The gui element parent class must not be initialised
        # as it is instantiated with sprite set to None
        self.text_renderer.init()

    def activate(self):
        '''
        Activate the textbox
        '''
        # If no value is inserted the label is displayed.
        # Make it disappear
        if len(self.value) == 0:
            self.text_renderer.set_text(
                    self.text_posx,
                    self.text_posy,
                    '',
                    self.text_scale,
                    self.text_color)
            self.text_renderer.generate_and_init()
        # Enable the text input feature of sdl2,
        # so that the textbox can observer text input and text edit events
        SDL_StartTextInput()
        self.observer.observe(SDL_KEYDOWN, self.handle_special_keys)
        self.observer.observe(SDL_TEXTINPUT, self.handle_textinput)
        self.observer.observe(SDL_TEXTEDITING, self.handle_textediting)
        self.active = True

    def deactivate(self):
        '''
        Deactivate the textbox
        '''
        # If the nothing was inserted make the label appear again
        if len(self.value) == 0:
            self.text_renderer.set_text(
                self.text_posx,
                self.text_posy,
                self.label,
                self.text_scale,
                self.text_color)
            self.text_renderer.generate_and_init()

        # Disable the tex input feature of sdl2 as it is not needed anymore
        SDL_StopTextInput()
        # The events have to be ignored, cause an other textbox may
        # be active in the meanwhile and there should not me more than
        # one textbox observing text eventss.
        self.observer.ignore(SDL_KEYDOWN)
        self.observer.ignore(SDL_TEXTINPUT)
        self.observer.ignore(SDL_TEXTEDITING)
        self.active = False

    def handle_click(self, sdl_event):
        '''
        This method is used as the callback function to observe
        SDL_MOUSEBUTTONDOWN events.
        It activates or deactivates the textbox
        '''
        button_event = sdl_event.button
        if pos_rect_intersect((button_event.x, button_event.y),
                              self.rect):
            self.activate()
        elif self.active:
            self.deactivate()

    def handle_special_keys(self, sdl_event):
        '''
        This methode is used as the callback functions to observe
        the keydown events. This is mostly handeld to use
        backspace and enter keypresses
        '''
        key_event = sdl_event.key
        if key_event.keysym.sym == SDLK_BACKSPACE:
            self.value = self.value[:-1]
            if self.mask is None:
                rendered_text = self.value
            else:
                rendered_text = self.mask * len(self.value)
            if len(self.value) > self.text_limit:
                rendered_text = rendered_text[-self.text_limit:]
            self.text_renderer.set_text(
                    self.text_posx,
                    self.text_posy,
                    rendered_text,
                    self.text_scale,
                    self.text_color)
            self.text_renderer.generate_and_init()
        if key_event.keysym.sym == SDLK_RETURN or\
           key_event.keysym.sym == SDLK_RETURN2:
            # Deactivate the textbox on enter as this marks the end of a text
            self.deactivate()

    def handle_textinput(self, sdl_event):
        '''
        This method is used as the callback function to observe
        SDL_TEXTINPUT events.
        '''
        text_event = sdl_event.text
        self.value += text_event.text.decode('utf-8')
        if self.mask is None:
            rendered_text = self.value
        else:
            rendered_text = self.mask * len(self.value)
        if len(self.value) > self.text_limit:
            rendered_text = rendered_text[-self.text_limit:]
        self.text_renderer.set_text(
                self.text_posx,
                self.text_posy,
                rendered_text,
                self.text_scale,
                self.text_color)
        self.text_renderer.generate_and_init()

    def handle_textediting(self, sdl_event):
        '''
        This method is used as the callback function to observe
        SDL_TEXTINPUT events.
        '''
        text_event = sdl_event.text
        start = text_event.start
        end = start + text_event.length
        # Concatenate the new string as python string do not
        # allow item assignment. This code is highly untested and
        # very error prone to off by one errors.
        self.value = self.value[0:start] + text_event.text.decode('utf-8')\
            + self.value[end:]
        if self.mask is None:
            rendered_text = self.value
        else:
            rendered_text = self.mask * len(self.value)
        if len(self.value) > self.text_limit:
            rendered_text = rendered_text[-self.text_limit:]
        self.text_renderer.set_text(
                self.text_posx,
                self.text_posy,
                rendered_text,
                self.text_scale,
                self.text_color)
        self.text_renderer.generate_and_init()

    def draw(self):
        '''
        Draw the textbox
        '''
        self.text_renderer.shader.use()
        self.text_renderer.draw()


if __name__ == '__main__':
    import sdl2
    import sdl2.ext
    from sdl2 import video
    import glm
    import numpy as np
    from PIL import Image
    from OpenGL.GL import (
        glClear, glClearColor, glEnable, glBlendFunc, GL_COLOR_BUFFER_BIT,
        GL_DEPTH_BUFFER_BIT, glBindVertexArray,
        GL_BLEND, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
    from Akuga.Anudem2D.Util.Shader import load_shader_from_single_file
    from Akuga.Anudem2D.Util.Texture2D import Texture2D
    from Akuga.Anudem2D.Util.FNTParser import parse_fnt
    from Akuga.Anudem2D.Util.EventInterface import\
        convert_sdl_to_anudem_events
    from Akuga.Anudem2D.GUI.TextRenderer import TextRenderer

    sdl2.SDL_Init(sdl2.SDL_INIT_VIDEO)
    window = sdl2.SDL_CreateWindow(
        b"Button Demo",
        sdl2.SDL_WINDOWPOS_UNDEFINED,
        sdl2.SDL_WINDOWPOS_UNDEFINED, 1220, 686,
        sdl2.SDL_WINDOW_OPENGL)
    video.SDL_GL_SetAttribute(video.SDL_GL_CONTEXT_MAJOR_VERSION, 4)
    video.SDL_GL_SetAttribute(video.SDL_GL_CONTEXT_MINOR_VERSION, 5)
    video.SDL_GL_SetAttribute(
        video.SDL_GL_CONTEXT_PROFILE_MASK,
        video.SDL_GL_CONTEXT_PROFILE_CORE)
    context = sdl2.SDL_GL_CreateContext(window)

    # Load a shader programme
    with open('/home/lyding/Akuga/Akuga/Anudem2D/'
              'Drawables/shaders/text_renderer.gl') as shader_fd:
        shader2 = load_shader_from_single_file(shader_fd)


    # Load the image data
    image = Image.open(
        '/home/lyding/Akuga/Akuga/Anudem2D/GUI/res/booter2.png')
    image_data = np.array(list(image.getdata()), dtype=np.uint8)
    texture2d2 = Texture2D(256, 256, image_data)
    texture2d2.init()

    # Load the glyphs from the fnt file
    with open(
            '/home/lyding/Akuga/Akuga/'
            'Anudem2D/GUI/res/booter2.fnt') as fnt_fd:
        info, common, page, glyphs = parse_fnt(fnt_fd)
    text_renderer = TextRenderer(
        texture2d2, info, common, page, glyphs, shader2)
    text_renderer.init()

    # Create and init the textbox which will ini the sprite as well
    textbox = TextBoxLayout(0, (0, 0, 100, 25), text_renderer, 'was ein tolles label', '*', 0.7, 0, 0, 4)
    textbox.init()

    # Define a button event observer
    class ButtonEventObserver(Observer):
        def __init__(self):
            super().__init__()
            self.observe(SDL_TEXTINPUT, self.text_input)

        def text_input(self, data):
            print(textbox.value)

    # Create an instance of the button observer to check if everything
    # works as expected
    button_observer = ButtonEventObserver()

    # Set view matrix
    view_matrix = glm.lookAt(
        glm.vec3(0, 0, 0),
        glm.vec3(0, 0, -1),
        glm.vec3(0, 1, 0))
    # projection_matrix = glm.perspective(35.0, 1.0, 0.1, 100.0)
    projection_matrix = glm.ortho(0, 1220, 686, 0)
    model_matrix = glm.mat4(1)

    shader2.use()
    shader2.upload_v_matrix(view_matrix)
    shader2.upload_p_matrix(projection_matrix)

    running = True
    glEnable(GL_BLEND)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
    while running:
        events = sdl2.ext.get_events()
        for event in events:
            if event.type == sdl2.SDL_QUIT:
                running = False
            convert_sdl_to_anudem_events(event)

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glClearColor(1, 1, 0, 0)
        textbox.draw()

        glBindVertexArray(0)
        sdl2.SDL_GL_SwapWindow(window)
        sdl2.SDL_Delay(10)

    sdl2.SDL_GL_DeleteContext(context)
    sdl2.SDL_DestroyWindow(window)
    sdl2.SDL_Quit()
