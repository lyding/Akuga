'''
This module contains the basic gui element class.
Every gui element should inherit from this.
'''
from Akuga.Anudem2D.Util.ObserverPattern import Observer


class GUIElement():
    '''
    The abstract gui element class from which every real
    gui element should inherit
    '''
    def __init__(self, _id, sprite, callback):
        '''
        The constructor of the GUIElement class

        :param _id: The id of this gui element
        :param sprite: The sprite of this gui element
        :param callback: The callback function to invoke
        '''
        self.id = _id
        # GUI elements are deactive by default
        self.observer = Observer()
        self.deactivate()
        self.sprite = sprite
        self.callback = callback

    def init(self):
        '''
        Init all components of the GUIElement which have to be initialised
        '''
        self.sprite.init()

    def full_init(self):
        '''
        Full Init all components of the GUIElement which have to be initialised
        '''
        self.sprite.full_init()

    def activate(self):
        '''
        Make the GUIElement responsive to UI events
        (activated by default)
        '''
        self.observer.activate()

    def deactivate(self):
        '''
        Make the GUIElement unresponsive to UI events
        '''
        self.observer.deactivate()

    def is_active(self):
        '''
        Returns true if the GUIElement is active
        '''
        return self.observer.active

    def draw(self):
        self.sprite.draw()

    def set_callback(self, callback):
        '''
        Set the callback function to invoke
        '''
        self.callback = callback

    def set_sprite(self, sprite):
        '''
        Change the sprite of the gui element
        '''
        self.sprite = sprite


def pos_rect_intersect(pos, rect):
    '''
    Check if pos and rect intersect

    :param pos: An tuple of the form (x, y)
    :param rect: An tuple of the form (x, y, width, height)
    '''
    if pos[0] > rect[0] and pos[0] < rect[0] + rect[2] and\
       pos[1] > rect[1] and pos[1] < rect[1] + rect[3]:
        return True
    return False
