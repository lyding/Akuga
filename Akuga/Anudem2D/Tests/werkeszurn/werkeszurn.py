'''
This module contains a representation for an animated textured sprite.
'''

from Akuga.Anudem2D.Drawables.TexturedSprite import TexturedSprite


class AnimatedTexturedSprite(TexturedSprite):
    '''
    The AnimatedTexturedSprite represents a animated textured sprite
    where each row represents one animation.

    Shader Requirements:
    vertex layout: pos->vec3; uvcoords->vec2
    obj dependend uniforms:
        m_matrix->mat4; // The world matrix
        subsprite_width->float; // The uvcoord width of a subsprite
        subsprite_height->float; // The uvcoord height of a subsprite
        loop->int; // Should the animation be looped
        row->int; // The row aka the animation to draw from
        collums->int; // The number of collumns needed to loop the animation
        time->float; // The current animation time
        duration_per_subsprite->float // The time a subsprite is rendered
        texture0->sampler2d; // The texture of the sprite sheet
    '''
    def __init__(self, shader, rectangle, texture2d, collumns, rows, row,
            duration, time_offset):
        '''
        The constructor of the AnimatedTexturedSprite class

        :param shader:
            A shader instance to draw this object
        :param rectangle:
            A quintuple of the form (x, y, w, h, rot) describing the position,
            size and rotation of a textured sprite
        :praram texture2d:
            An initialised texture2d describing the sprite sheet to draw from
        :param row:
            The number of rows in the sprite sheet
        :param collums:
            The number of collums in the sprite sheet
        :param duration:
            The duration of the animation in milliseconds
        '''
        super().__init__(shader, rectangle, texture2d)
        self.set_collumns(collumns)
        self.set_rows(rows)
        self.set_row(row)
        self.set_duration(duration)
        self.set_time_offset(time_offset)

    def jump_to(self, collum, row):
        '''
        Jump to the specified row and collum
        Requires the shader programme to be in use
        '''
        self.set_row(row)
        self.set_time(collum * self.get_duration_per_subsprite())

    def move(self, collumn, row):
        '''
        Just move left/right up/down on the subsprites
        '''
        self.set_row(self.get_row() + row)
        self.set_time(self.get_time() +
            collumn * self.get_duration_per_subsprite())

    def set_duration(self, duration):
        '''
        Set the duration of the animation and recalculate the
        duration per subsprite
        '''
        self.duration = duration
        self.set_duration_per_subsprite(duration / self.collums)


if __name__ == '__main__':
    import sdl2
    from sdl2 import video
    import ctypes
    import glm
    import numpy as np
    from time import time
    from PIL import Image
    from OpenGL.GL import (
        glClear, GL_COLOR_BUFFER_BIT,
        GL_DEPTH_BUFFER_BIT, glBindVertexArray)
    from Akuga.Anudem2D.Util.Shader import Shader
    from Akuga.Anudem2D.Util.Texture2D import Texture2D

    sdl2.SDL_Init(sdl2.SDL_INIT_VIDEO)
    window = sdl2.SDL_CreateWindow(b"Drawable Demo",
        sdl2.SDL_WINDOWPOS_UNDEFINED,
        sdl2.SDL_WINDOWPOS_UNDEFINED, 800, 600,
        sdl2.SDL_WINDOW_OPENGL)
    video.SDL_GL_SetAttribute(video.SDL_GL_CONTEXT_MAJOR_VERSION, 4)
    video.SDL_GL_SetAttribute(video.SDL_GL_CONTEXT_MINOR_VERSION, 5)
    video.SDL_GL_SetAttribute(video.SDL_GL_CONTEXT_PROFILE_MASK,
        video.SDL_GL_CONTEXT_PROFILE_CORE)
    context = sdl2.SDL_GL_CreateContext(window)

    # Load the static sprite shader programme
    shader1 = Shader(
        open('/home/lyding/Akuga/Akuga/Anudem2D/Tests/werkeszurn/vs.gl').read(),
        open('/home/lyding/Akuga/Akuga/Anudem2D/Tests/werkeszurn/fs.gl').read())

    # Load the animation shader programme
    shader2 = Shader(
        open('/home/lyding/Akuga/Akuga/Anudem2D/Tests/werkeszurn/ats_vs.gl').read(),
        open('/home/lyding/Akuga/Akuga/Anudem2D/Tests/werkeszurn/ats_fs.gl').read())

    # Load the image data
    image = Image.open('/home/lyding/Akuga/Akuga/Anudem2D/Tests/werkeszurn/kralle.png')
    image_data = np.array(list(image.getdata()), dtype=np.uint8)
    texture2d = Texture2D(2048, 256, image_data)
    texture2d.init()

    # Load the werkeszurn image data
    werk_img = Image.open('/home/lyding/Akuga/Akuga/Anudem2D/Tests/werkeszurn/werkeszurn.png')
    werk_img_data = np.array(list(werk_img.getdata()), dtype=np.uint8)
    werk_tex = Texture2D(512, 512, werk_img_data)
    werk_tex.init()

    # Create the werkeszurn image
    werkeszurn = TexturedSprite(shader1, (0, 0, 256, 256, 0), werk_tex)
    werkeszurn.init()

    # Create the animated sprite
    anim = AnimatedTexturedSprite(shader2, (0, 0, 256, 256, 0),
            texture2d, 8, 1, 1, 0.375, 2.0)
    # Init the sprite
    anim.init()

    # Set view matrix
    view_matrix = glm.lookAt(glm.vec3(0, 0, 0), glm.vec3(0, 0, -1), glm.vec3(0, 1, 0))
    # projection_matrix = glm.perspective(35.0, 1.0, 0.1, 100.0)
    projection_matrix = glm.ortho(0, 512, 512, 0)

    t0 = time()
    running = True
    event = sdl2.SDL_Event()
    while running:
        while sdl2.SDL_PollEvent(ctypes.byref(event)) != 0:
            if event.type == sdl2.SDL_QUIT:
                running = False
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        # Update the time
        ellapsed_time = time() - t0
        # Render werkeszurn
        shader1.use()
        shader1.upload_v_matrix(view_matrix)
        shader1.upload_p_matrix(projection_matrix)
        werkeszurn.draw()

        # Render the animation
        shader2.use()
        shader2.upload_time(ellapsed_time)
        shader2.upload_v_matrix(view_matrix)
        shader2.upload_p_matrix(projection_matrix)
        anim.draw()

        glBindVertexArray(0)
        sdl2.SDL_GL_SwapWindow(window)
        sdl2.SDL_Delay(10)

    sdl2.SDL_GL_DeleteContext(context)
    sdl2.SDL_DestroyWindow(window)
    sdl2.SDL_Quit()
