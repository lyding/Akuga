import numpy as np
from Akuga.Anudem2D.Util.Texture2D import Texture2D
from PIL import Image
from sys import modules


def load():
    module = modules[__name__]
    # Load the image data
    for i in range(1, 4):
        image = Image.open(
            '/home/lyding/Akuga/Akuga/'
            f'Anudem2D/Drawables/res/image{i}.png')
        image_data = np.array(list(image.getdata()), dtype=np.uint8)
        texture2d = Texture2D(256, 256, image_data)
        print('From load: ' + str(texture2d.id))
        setattr(module, f'texture{i}', texture2d)
