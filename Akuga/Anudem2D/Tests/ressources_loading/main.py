import sdl2
from sdl2 import video
import ctypes
from OpenGL.GL import (
    glClear, GL_COLOR_BUFFER_BIT,
    GL_DEPTH_BUFFER_BIT, glBindVertexArray)
from Akuga.Anudem2D.Util.Shader import Shader
from Akuga.Anudem2D.Drawables.TexturedSprite import TexturedSprite
import glm
from threading import Thread
import Akuga.Anudem2D.Tests.ressources_loading.ressources as ressources
import Akuga.Anudem2D.Util.LoggedGL as LoggedGL


sdl2.SDL_Init(sdl2.SDL_INIT_VIDEO)
window = sdl2.SDL_CreateWindow(
    b"Drawable Demo",
    sdl2.SDL_WINDOWPOS_UNDEFINED,
    sdl2.SDL_WINDOWPOS_UNDEFINED, 800, 600,
    sdl2.SDL_WINDOW_OPENGL)
video.SDL_GL_SetAttribute(video.SDL_GL_CONTEXT_MAJOR_VERSION, 4)
video.SDL_GL_SetAttribute(video.SDL_GL_CONTEXT_MINOR_VERSION, 5)
video.SDL_GL_SetAttribute(
    video.SDL_GL_CONTEXT_PROFILE_MASK,
    video.SDL_GL_CONTEXT_PROFILE_CORE)
context = sdl2.SDL_GL_CreateContext(window)

# Load a shader programme
shader = Shader(
    open(
        '/home/lyding/Akuga/Akuga/'
        'Anudem2D/Drawables/shaders/textured_sprite_vs.gl').read(),
    open(
        '/home/lyding/Akuga/Akuga/'
        'Anudem2D/Drawables/shaders/textured_sprite_fs.gl').read())


t = Thread(target=ressources.load)
t.start()
t.join()

# ressources.load()

# Create the sprites
sprite1 = TexturedSprite(shader, (100, 100, 100, 100, 0), ressources.texture1)
sprite2 = TexturedSprite(shader, (200, 100, 100, 100, 0), ressources.texture2)
sprite3 = TexturedSprite(shader, (300, 100, 100, 100, 0), ressources.texture3)

# Init the sprite and all of its components
sprite3.full_init()
sprite2.full_init()
sprite1.full_init()

# Set view matrix
view_matrix = glm.lookAt(
    glm.vec3(0, 0, 0),
    glm.vec3(0, 0, -1),
    glm.vec3(0, 1, 0))
projection_matrix = glm.ortho(0, 400, 400, 0)

shader.use()
shader.upload_v_matrix(view_matrix)
shader.upload_p_matrix(projection_matrix)

# Turn of logging
LoggedGL.logging = False

running = True
event = sdl2.SDL_Event()
while running:
    while sdl2.SDL_PollEvent(ctypes.byref(event)) != 0:
        if event.type == sdl2.SDL_QUIT:
            running = False
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

    sprite1.draw()
    sprite2.draw()
    sprite3.draw()

    glBindVertexArray(0)
    sdl2.SDL_GL_SwapWindow(window)
    sdl2.SDL_Delay(10)
sdl2.SDL_GL_DeleteContext(context)
sdl2.SDL_DestroyWindow(window)
sdl2.SDL_Quit()
