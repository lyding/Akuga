'''
This module contains a representation for an animated textured sprite.
'''

from Akuga.Anudem2D.Drawables.TexturedSprite import TexturedSprite


class AnimatedSprite(TexturedSprite):
    '''
    The AnimatedSprite class represents a spritesheet used
    for animations. The layout defines the animations within this
    sprite sheet.

    Shader requirement:
    -------------------
    vertex layout: pos->vec3; uvcoord->vec2
    obj dependend uniforms:
        m_matrix->mat4; // The world matrix
        texture0->texture2d; // The texture of the sprite sheet
        texcoord->vec4; // The texcoord of the current subsprite (frame)
    '''
    def __init__(self, shader, rectangle, texture2d, layout, spf, mode):
        '''
        The constructor of the AnimatedTexturedSprite class

        :param shader:
            A shader instance to draw this object
        :param rectangle:
            A quintuple of the form (x, y, w, h, rot) describing the position,
            size and rotation of a textured sprite
        :praram texture2d:
            An initialised texture2d describing the sprite sheet to draw from
        :param layout:
            A dictionary storing lists of subsprite with the animation name
            as key. The subsprite are represented as a vec4 describing the
            topleft position as well as the width and height.
        :param spf (seconds per frame):
            Specify the seconds two wait between two frames
        :param mode:
            The modus of the animation. This can be
            mode=0: Normal, most basic Animation
            mode=1: Oneshot, hide the sprite after the animation is done
            mode=2: Loop, Just loop the current animation
        '''
        super().__init__(shader, rectangle, texture2d)
        # A dictionary storing a list subsprite with the animation name
        # as key. The texcoord are of the type vec4
        self.layout = layout
        # The seconds per frame
        self.spf = spf
        # Set the mode
        self.mode = mode
        # Set the current animation to be the first in the layout
        self.current_animation = list(layout.keys())[0]
        # Set the subsprite accordingly to the first frame
        self.set_subsprite(self.layout[self.current_animation][0])
        # The last time the frame was updated
        self.time_stamp = 0
        # The index of the frame in the current animation
        self.frame_index = 0
        # Is set to true after the current animation was played
        self.done = False

    def update(self, ellapsed_time):
        '''
        Update the time of the animated sprite and switch to the
        next frame if neccessary
        '''
        if (ellapsed_time - self.time_stamp) > self.spf:
            print(self.frame_index)
            # Update the timestamp
            self.time_stamp = ellapsed_time
            # Switch to the next frame and update the subsprite to render
            if self.frame_index < len(self.layout[self.current_animation]) - 1:
                self.frame_index += 1
                self.set_subsprite(
                    self.layout[self.current_animation][self.frame_index])
            elif self.mode == 2:
                # A looped animation is never considered done
                self.done = False
                # Jump to the start of the current animation
                self.frame_index = 0
                self.set_subsprite(
                    self.layout[self.current_animation],
                    [self.frame_index])
            else:
                self.done = True

    def draw(self):
        '''
        Draw the animated sprite except it is a oneshot animation
        and the animation is done
        '''
        # If the mode is oneshot and the animatio is done
        # do not render anything
        if self.done and self.mode == 1:
            return
        super().draw()

    def set_animation(self, animation_name):
        '''
        Just set the current animation to be the animation
        stored with $animation_name as key.
        And set the subsprite to be the first frame, therefor
        the done flag is reset to be False
        '''
        self.done = False
        self.current_animation = animation_name
        self.set_subsprite(self.layout[self.current_animation][0])

    def set_mode(self, mode):
        '''
        Just set the mode of the animated sprite
        '''
        self.mode = mode


if __name__ == '__main__':
    import sdl2
    from sdl2 import video
    import ctypes
    import glm
    import numpy as np
    from time import time
    from PIL import Image
    from OpenGL.GL import (
        glClear, GL_COLOR_BUFFER_BIT,
        GL_DEPTH_BUFFER_BIT, glBindVertexArray)
    from Akuga.Anudem2D.Util.Shader import Shader
    from Akuga.Anudem2D.Util.Texture2D import Texture2D

    sdl2.SDL_Init(sdl2.SDL_INIT_VIDEO)
    window = sdl2.SDL_CreateWindow(
        b"Animated Sprite Demo",
        sdl2.SDL_WINDOWPOS_UNDEFINED,
        sdl2.SDL_WINDOWPOS_UNDEFINED, 800, 600,
        sdl2.SDL_WINDOW_OPENGL)
    video.SDL_GL_SetAttribute(video.SDL_GL_CONTEXT_MAJOR_VERSION, 4)
    video.SDL_GL_SetAttribute(video.SDL_GL_CONTEXT_MINOR_VERSION, 5)
    video.SDL_GL_SetAttribute(
        video.SDL_GL_CONTEXT_PROFILE_MASK,
        video.SDL_GL_CONTEXT_PROFILE_CORE)
    context = sdl2.SDL_GL_CreateContext(window)

    # Load a shader programme
    shader = Shader(
        open(
            '/home/lyding/Akuga/Akuga/'
            'Anudem2D/Drawables/shaders/ats_vs.gl').read(),
        open(
            '/home/lyding/Akuga/Akuga/'
            'Anudem2D/Drawables/shaders/ats_fs.gl').read())

    # Load the image data
    image = Image.open(
        '/home/lyding/Akuga/Akuga/'
        'Anudem2D/Drawables/res/claw.png')
    image_data = np.array(list(image.getdata()), dtype=np.uint8)
    texture2d = Texture2D(1792, 256, image_data)
    texture2d.init()

    # Create the animated sprite

    layout = {'test': [(1/7 * x, 0, 1/7, 1) for x in range(0, 7)]}

    sprite1 = AnimatedSprite(
            shader, (100, 100, 200, 148, 0),
            texture2d, layout, 0.02, 0)
    # Init the sprite
    sprite1.init()

    # Set view matrix
    view_matrix = glm.lookAt(
        glm.vec3(0, 0, 0),
        glm.vec3(0, 0, -1),
        glm.vec3(0, 1, 0))
    # projection_matrix = glm.perspective(35.0, 1.0, 0.1, 100.0)
    projection_matrix = glm.ortho(0, 400, 400, 0)

    shader.use()
    shader.upload_v_matrix(view_matrix)
    shader.upload_p_matrix(projection_matrix)

    t0 = time()
    ellapsed_time = 0
    running = True
    event = sdl2.SDL_Event()
    while running:
        while sdl2.SDL_PollEvent(ctypes.byref(event)) != 0:
            if event.type == sdl2.SDL_QUIT:
                running = False

        ellapsed_time = time() - t0
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        sprite1.update(ellapsed_time)
        sprite1.draw()

        glBindVertexArray(0)
        sdl2.SDL_GL_SwapWindow(window)
        sdl2.SDL_Delay(10)

    sdl2.SDL_GL_DeleteContext(context)
    sdl2.SDL_DestroyWindow(window)
    sdl2.SDL_Quit()
