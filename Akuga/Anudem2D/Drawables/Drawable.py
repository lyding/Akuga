'''
This module contains the most primitive drawable the Anudem2D
library provides. The Drawable class generates part of its
interface by the object dependend uniforms of the specified shader instance.
'''
from types import MethodType
from OpenGL.GL import (
    glGenBuffers,
    glGenVertexArrays,
    glBindVertexArray,
    glBindBuffer,
    glBufferData,
    glClear,
    glDrawElements,
    GL_STATIC_DRAW,
    GL_ARRAY_BUFFER,
    GL_ELEMENT_ARRAY_BUFFER,
    GL_COLOR_BUFFER_BIT,
    GL_DEPTH_BUFFER_BIT,
    GL_TRIANGLES,
    GL_UNSIGNED_INT)

from Akuga.Anudem2D.Util.LoggedGL import glCall


def getter_clojure(self, uniform):
    '''
    Generate a getter function which returns the uniform attribute value
    '''
    def set(self):
        # Return the uniforms value
        return self.uniforms[uniform[0]][4]
    return set


def setter_clojure(self, uniform):
    '''
    Generate a setter function which overwrites the uniform attribute value
    '''
    def set(self, value):
        # Set the uniforms value
        self.uniforms[uniform[0]][4] = value
    return set


class Drawable:
    '''
    The most primitiv drawable object Anudem2D provides
    '''
    def __init__(self, shader, vertices, indices):
        '''
        The constructor of the Drawable class

        :param shader: The shader instance to draw this object
        :param vertices: The vertices representing the 3d shape of the drawable
        :param indices: The indicex representing the 3d shader of the drawable
        :return: An instance of the Drawable class
        '''
        self.shader = shader
        self.vertices = vertices
        self.indices = indices
        # Generate a vbo and ebo
        self.vbo = glCall(glGenBuffers, (1,))
        self.ebo = glCall(glGenBuffers, (1,))
        self.vao = glCall(glGenVertexArrays, (1,))

        # Generate attributes out of the
        # object depending uniforms of the shader
        self.uniforms = {}
        for uniform in shader.depending_uniforms:
            # Extend the uniform datastructure by one field storing the
            # value and add it to the list of uniforms
            self.uniforms[uniform[0]] = [*uniform, None]
            # Generate a getter function for the uniform and expose
            # it to the user by adding it to the interface
            bound_getter = MethodType(getter_clojure(self, uniform), self)
            setattr(self, 'get_' + uniform[0], bound_getter)
            # Generate a setter function for the uniform and expose
            # it to the user by adding it to the interface
            bound_setter = MethodType(setter_clojure(self, uniform), self)
            setattr(self, 'set_' + uniform[0], bound_setter)

    def init(self):
        '''
        Configure the buffers and set the vertex attribute layout
        '''
        # Configure the vao
        glCall(glBindVertexArray, (self.vao,))
        glCall(glBindBuffer, (GL_ARRAY_BUFFER, self.vbo))
        glCall(glBufferData, (
                GL_ARRAY_BUFFER, len(self.vertices.tobytes()),
                self.vertices, GL_STATIC_DRAW))
        glCall(glBindBuffer, (GL_ELEMENT_ARRAY_BUFFER, self.ebo))
        glCall(glBufferData, (
                GL_ELEMENT_ARRAY_BUFFER, len(self.indices.tobytes()),
                self.indices, GL_STATIC_DRAW))
        # Use the shader to configure the vertex layou
        self.shader.configure_vertex_layout()
        glCall(glBindVertexArray, (0,))

    def full_init(self):
        '''
        The full_init methode inits the Drawable and all components which
        have to be initialised. Cause the Drawable class does not have
        such components the full_init methode is equivalent to the
        init methode.
        Overwrite this if the inherited class has components to
        initialise
        '''
        self.init()

    def update_uniforms(self):
        '''
        Upload all uniforms
        '''
        for uniform in self.uniforms.values():
            uniform[3](uniform[4])

    def draw(self):
        '''
        Draw the Drawable.
        '''
        with self.shader:
            self.update_uniforms()
            glCall(glBindVertexArray, (self.vao,))
            glCall(glDrawElements, (
                GL_TRIANGLES,
                len(self.indices),
                GL_UNSIGNED_INT, None))


if __name__ == '__main__':
    import sdl2
    from sdl2 import video
    import glm
    import numpy as np
    import ctypes
    from Akuga.Anudem2D.Util.Shader import Shader

    sdl2.SDL_Init(sdl2.SDL_INIT_VIDEO)
    window = sdl2.SDL_CreateWindow(
            b"Drawable Demo",
            sdl2.SDL_WINDOWPOS_UNDEFINED,
            sdl2.SDL_WINDOWPOS_UNDEFINED, 800, 600,
            sdl2.SDL_WINDOW_OPENGL)
    video.SDL_GL_SetAttribute(video.SDL_GL_CONTEXT_MAJOR_VERSION, 4)
    video.SDL_GL_SetAttribute(video.SDL_GL_CONTEXT_MINOR_VERSION, 5)
    video.SDL_GL_SetAttribute(
            video.SDL_GL_CONTEXT_PROFILE_MASK,
            video.SDL_GL_CONTEXT_PROFILE_CORE)
    context = sdl2.SDL_GL_CreateContext(window)

    # Load a shader programme
    shader = Shader(
        open(
            '/home/lyding/Akuga/Akuga/'
            'Anudem2D/Drawables/shaders/vs.gl').read(),
        open(
            '/home/lyding/Akuga/Akuga/'
            'Anudem2D/Drawables/shaders/fs.gl').read())

    # Create vertice array
    vertices = np.array([-1, -1, 0, 0, 1,
                         -1, 1, 0, 0, 0,
                         1, 1, 0, 1, 0,
                         1, -1, 0, 1, 1], dtype=np.float32)

    # Create index array
    indices = np.array([0, 1, 2, 0, 2, 3], dtype=np.uint32)

    # Create drawables
    drawable1 = Drawable(shader, vertices, indices)
    drawable2 = Drawable(shader, vertices, indices)

    # Init the drawable
    drawable1.init()
    drawable2.init()

    drawable1.set_m_matrix(glm.translate(glm.mat4x4(), glm.vec3(2, 0, 2)))
    drawable2.set_m_matrix(glm.translate(glm.mat4x4(), glm.vec3(-2, 0, 2)))
    view_matrix = glm.lookAt(
            glm.vec3(0, 0, 0),
            glm.vec3(0, 0, 1),
            glm.vec3(0, 1, 0))
    projection_matrix = glm.perspective(35.0, 1.0, 0.1, 100.0)

    print(drawable1.get_m_matrix())
    print(drawable2.get_m_matrix())

    shader.use()
    shader.upload_v_matrix(view_matrix)
    shader.upload_p_matrix(projection_matrix)

    running = True
    event = sdl2.SDL_Event()
    while running:
        while sdl2.SDL_PollEvent(ctypes.byref(event)) != 0:
            if event.type == sdl2.SDL_QUIT:
                running = False
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        drawable1.draw()
        drawable2.draw()

        glBindVertexArray(0)
        sdl2.SDL_GL_SwapWindow(window)
        sdl2.SDL_Delay(10)

    sdl2.SDL_GL_DeleteContext(context)
    sdl2.SDL_DestroyWindow(window)
    sdl2.SDL_Quit()
