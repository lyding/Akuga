'''
This module contains a representation for a textured sprite.
'''
import glm
from numpy import array, float32, uint32
from Akuga.Anudem2D.Util.Texture2D import Texture2D
from Akuga.Anudem2D.Util.Texture2DUniform import Texture2DUniform
from Akuga.Anudem2D.Drawables.Drawable import Drawable


class TexturedSprite(Drawable):
    '''
    The TextureSprite class represents a Drawable with four
    vertices forming a unit rectangle of the form
    ((0,0,0), (0, 1, 0), (1, 1, 0), (1, 0, 0))
    Each vertex also includes UV coordinates of the form
    ((0, 0), (0, 1), (1, 1), (1, 0))
    Therefor to render this class correctly the Shader
    must not accept anything else than a vec3 for the position
    and a vec2 for the UV coordinates.

    Shader Requirements:
    --------------------
    vertex layout: pos->vec3; uvcoords->vec2
    obj dependend uniforms: m_matrix->mat4; texture0->sampler2D;

    The active texture unit for the sampler uniform texture0 is 0
    '''
    def __init__(self, shader, rectangle, texture2d):
        '''
        The constructor for the Sprite class

        :param shader:
            A shader instance to draw this object
        :param rectangle:
            A quintuple of the form (x, y, w, h, rot) describing the position,
            size and rotation of textured sprite
        :param texture2d:
            An initiliased texture2d describing the texture to draw
        :return: An instance of the Sprite class
        '''
        # Create the vertices array
        vertices = array([
            0, 0, 0, 0, 0,
            0, 1, 0, 0, 1,
            1, 1, 0, 1, 1,
            1, 0, 0, 1, 0], dtype=float32)
        # Create the indices array
        indices = array([0, 1, 2, 0, 2, 3], dtype=uint32)
        # First invoke the parents constructor as this generates the interface
        super().__init__(shader, vertices, indices)
        # Set the rectangle describing the position
        # and dimension of the sprite
        self.set_rectangle(rectangle)
        # Init a Texture2DUniform for the sampler uniform
        self.set_texture0(Texture2DUniform(texture2d, 0))

    def full_init(self):
        '''
        Init the TexturedSprite and all of its components which
        have to be initialised
        '''
        super().init()
        # The texture2d is stored as an attribture of the
        # texture uniform instance created from the Drawable
        # parent class to match the object dependend uniforms
        # of the shader
        self.get_texture0().texture2d.init()

    def set_rectangle(self, rectangle):
        '''
        Just set the rectangle describing the shape of the sprite
        and adjust the model matrix
        '''
        self.rectangle = rectangle
        # Cause of the matrix stack? the matrix operation has to
        # be written in reverse order. So better read this block
        # very unintuitiv from bottom to top
        model = glm.mat4(1)
        model = glm.translate(model, glm.vec3(rectangle[0], rectangle[1], 0))
        model = glm.translate(model, glm.vec3(
            0.5 * rectangle[2],
            0.5 * rectangle[3], 0))
        model = glm.rotate(model, rectangle[4], glm.vec3(0, 0, 1))
        model = glm.translate(model, glm.vec3(
            -0.5 * rectangle[2],
            -0.5 * rectangle[3], 0))
        model = glm.scale(model, glm.vec3(rectangle[2], rectangle[3], 1))
        self.set_m_matrix(model)

    def set_texture(self, texture):
        '''
        Set the texture
        '''
        self.get_texture0().texture2d = texture


if __name__ == '__main__':
    import sdl2
    from sdl2 import video
    import ctypes
    import numpy as np
    from PIL import Image
    from OpenGL.GL import (
        glClear, GL_COLOR_BUFFER_BIT,
        GL_DEPTH_BUFFER_BIT, glBindVertexArray)
    from Akuga.Anudem2D.Util.Shader import Shader

    sdl2.SDL_Init(sdl2.SDL_INIT_VIDEO)
    window = sdl2.SDL_CreateWindow(
        b"Drawable Demo",
        sdl2.SDL_WINDOWPOS_UNDEFINED,
        sdl2.SDL_WINDOWPOS_UNDEFINED, 800, 600,
        sdl2.SDL_WINDOW_OPENGL)
    video.SDL_GL_SetAttribute(video.SDL_GL_CONTEXT_MAJOR_VERSION, 4)
    video.SDL_GL_SetAttribute(video.SDL_GL_CONTEXT_MINOR_VERSION, 5)
    video.SDL_GL_SetAttribute(
        video.SDL_GL_CONTEXT_PROFILE_MASK,
        video.SDL_GL_CONTEXT_PROFILE_CORE)
    context = sdl2.SDL_GL_CreateContext(window)

    # Load a shader programme
    shader = Shader(
        open(
            '/home/lyding/Akuga/Akuga/'
            'Anudem2D/Drawables/shaders/textured_sprite_vs.gl').read(),
        open(
            '/home/lyding/Akuga/Akuga/'
            'Anudem2D/Drawables/shaders/textured_sprite_fs.gl').read())

    # Load the image data
    image = Image.open(
        '/home/lyding/Akuga/Akuga/'
        'Anudem2D/Drawables/res/image1.png')
    image_data = np.array(list(image.getdata()), dtype=np.uint8)
    texture2d = Texture2D(256, 256, image_data)

    # Create the sprites
    sprite1 = TexturedSprite(shader, (100, 100, 100, 100, 0), texture2d)

    # Init the sprite and all of its components
    sprite1.full_init()

    # Set view matrix
    view_matrix = glm.lookAt(
        glm.vec3(0, 0, 0),
        glm.vec3(0, 0, -1),
        glm.vec3(0, 1, 0))
    projection_matrix = glm.ortho(0, 400, 400, 0)

    shader.use()
    shader.upload_v_matrix(view_matrix)
    shader.upload_p_matrix(projection_matrix)

    running = True
    event = sdl2.SDL_Event()
    while running:
        while sdl2.SDL_PollEvent(ctypes.byref(event)) != 0:
            if event.type == sdl2.SDL_QUIT:
                running = False
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        sprite1.draw()

        glBindVertexArray(0)
        sdl2.SDL_GL_SwapWindow(window)
        sdl2.SDL_Delay(10)
    sdl2.SDL_GL_DeleteContext(context)
    sdl2.SDL_DestroyWindow(window)
    sdl2.SDL_Quit()
