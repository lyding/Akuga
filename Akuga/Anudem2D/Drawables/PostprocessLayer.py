'''
This module contains a implementation of a postprocess layer.
'''
from Akuga.Anudem2D.Util.Framebuffers import ColorFramebuffer
from Akuga.Anudem2D.Drawables.TexturedSprite import TexturedSprite


class PostprocessLayer:
    '''
    Represents a postprocess layer, a composition from a ColorFramebuffer
    and a TexturedSprite. The TexturedSprite is the drawable part of
    the PostprocessLayer therefor the shader requirements have to
    be the same as for the TexturedSprite.

    Shader Requirements:
    --------------------
    vertex layout: pos->vec3; uvcoords->vec2
    obj dependend uniforms: m_matrix->mat4; texture0->sampler2D;

    The active texture unit for the sampler uniform texture0 is 0
    The Postprocess Layer is flipped at the X-Axis, so the
    Y-component of the uvcoords should be inverted in the shader
    to redo the flipping.
    '''
    def __init__(self, shader, width, height):
        '''
        The constructor of the PostprocessLayer class

        :param shader: The postprocess shader to draw the postprocess layer
        :param width: The width of the screen
        :param height: The height of the screen
        :return: An instance of the PostprocessLayer class
        '''
        self.framebuffer = ColorFramebuffer(width, height)
        # Create an instance of the TexturedSprite class holding the texture2d
        # of the framebuffer as a reference. This way the texture of the
        # sprite changes when something is rendered on the framebuffer
        self.textured_sprite = TexturedSprite(
            shader, (0, 0, width, height, 0), self.framebuffer.texture2d)

    def init(self):
        '''
        Init the ColorFramebuffer of the PostprocessLayer instance
        '''
        self.framebuffer.init()
        self.textured_sprite.init()

    def full_init(self):
        '''
        Init the PostprocessLayer and all of its components which have to
        be initialised
        '''
        self.framebuffer.init()
        self.textured_sprite.full_init()


if __name__ == '__main__':
    import glm
    import sdl2
    from sdl2 import video
    import ctypes
    import numpy as np
    from PIL import Image
    from OpenGL.GL import (
        glClear, GL_COLOR_BUFFER_BIT,
        GL_DEPTH_BUFFER_BIT, glBindVertexArray)
    from Akuga.Anudem2D.Util.Shader import load_shader_from_single_file
    from Akuga.Anudem2D.Util.Texture2D import Texture2D

    sdl2.SDL_Init(sdl2.SDL_INIT_VIDEO)
    window = sdl2.SDL_CreateWindow(
        b"Drawable Demo",
        sdl2.SDL_WINDOWPOS_UNDEFINED,
        sdl2.SDL_WINDOWPOS_UNDEFINED, 800, 600,
        sdl2.SDL_WINDOW_OPENGL)
    video.SDL_GL_SetAttribute(video.SDL_GL_CONTEXT_MAJOR_VERSION, 4)
    video.SDL_GL_SetAttribute(video.SDL_GL_CONTEXT_MINOR_VERSION, 5)
    video.SDL_GL_SetAttribute(
        video.SDL_GL_CONTEXT_PROFILE_MASK,
        video.SDL_GL_CONTEXT_PROFILE_CORE)
    context = sdl2.SDL_GL_CreateContext(window)

    # Load a shader programme
    with open('/home/lyding/Akuga/Akuga/Anudem2D/Drawables/'
              'shaders/default_sprite.gl') as shader_fd:
        shader = load_shader_from_single_file(shader_fd)

    # Load a shader programme
    with open('/home/lyding/Akuga/Akuga/Anudem2D/Drawables/'
              'shaders/ppl.gl') as shader_fd:
        shader2 = load_shader_from_single_file(shader_fd)

    # Load the image data
    image = Image.open(
        '/home/lyding/Akuga/Akuga/'
        '/Anudem2D/Drawables/res/image1.png')
    image_data = np.array(list(image.getdata()), dtype=np.uint8)
    texture2d = Texture2D(256, 256, image_data)
    texture2d.init()

    # Create the sprites
    sprite1 = TexturedSprite(shader, (0, 0, 100, 100, 0), texture2d)
    sprite2 = TexturedSprite(shader, (300, 0, 100, 100, 0), texture2d)

    # Create and init the postprocess layer
    ppl = PostprocessLayer(shader2, 800, 600)
    ppl.init()

    # Init the sprites
    sprite1.init()
    sprite2.init()

    # Set view matrix
    view_matrix = glm.lookAt(
        glm.vec3(0, 0, 0),
        glm.vec3(0, 0, -1),
        glm.vec3(0, 1, 0))
    # projection_matrix = glm.perspective(35.0, 1.0, 0.1, 100.0)
    projection_matrix = glm.ortho(0, 800, 600, 0)

    shader.use()
    shader.upload_v_matrix(view_matrix)
    shader.upload_p_matrix(projection_matrix)

    shader2.use()
    shader2.upload_v_matrix(view_matrix)
    shader2.upload_p_matrix(projection_matrix)

    running = True
    event = sdl2.SDL_Event()
    time = 0
    while running:
        while sdl2.SDL_PollEvent(ctypes.byref(event)) != 0:
            if event.type == sdl2.SDL_QUIT:
                running = False

        with ppl.framebuffer:
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
            sprite1.draw()
            sprite2.draw()

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        with shader2:
            ppl.textured_sprite.draw()
        # sprite1.draw()
        # sprite2.draw()

        glBindVertexArray(0)
        sdl2.SDL_GL_SwapWindow(window)
        sdl2.SDL_Delay(10)
    sdl2.SDL_GL_DeleteContext(context)
    sdl2.SDL_DestroyWindow(window)
    sdl2.SDL_Quit()
