'''
This module contains a representation of a texture atlas which is a
rather static drawable using one large texture and 'stamps'
sections of this texture to different places in space.
It is a rather static class as changing on stamp
requires the vertices to be recalculated and reuploaded to the gpu.
'''

import glm
import numpy as np
from Akuga.Anudem2D.Drawables.Drawable import Drawable
from Akuga.Anudem2D.Util.Texture2DUniform import Texture2DUniform


class TextureAtlas(Drawable):
    '''
    The representation of a texture atlas which is a
    rather static drawable using one large texture and 'stamps'
    sections of this texture to different places in space.
    It is a rather static class as changing on stamp
    requires the vertices to be recalculated and reuploaded to the gpu.

    Shader Requirements:
    vertex layout: pos->vec3; uvcoords->vec2
    obj dependend uniforms: texture0->sampler2D;

    The active texture unit for the sampler uniform texture0 is 0
    '''
    def __init__(self, texture2d, uvmap_dict, shader):
        '''
        The constructor of the TextureAtlas class

        :param texture2d: The texture containing the large image data
        :param maps: A dict sotring the uvcoord mapping in the form
                     name -> rectangle
        '''
        super().__init__(
            shader,
            np.array([], dtype=np.float32),
            np.array([], dtype=np.uint32))
        # The texture2d storing the large image
        self.texture2d = texture2d
        # A mapping of names to uvcoords to make accessing
        # subimages easier
        self.uvmap_dict = uvmap_dict
        # A dictionary of all stamps to render
        self.stamps = {}
        # Used to generate unused ids by incrementing it
        self.free_id = 0
        # Set the texture2d uniform
        self.set_texture0(Texture2DUniform(texture2d, 0))

    def full_init(self):
        '''
        Init the TextureAtlas and all of its components
        '''
        super().full_init()
        # The texture2d is stored as an attribture of the
        # texture uniform instance created from the Drawable
        # parent class to match the object dependend uniforms
        # of the shader
        self.get_texture0().texture2d.init()

    def add_mapping(self, _id, uvcoord_mapping):
        '''
        Add a uvcoord_mapping to the uvmap_dict
        '''
        if _id not in self.uvmap_dict:
            self.uvmap_dict[_id] = uvcoord_mapping
        else:
            raise KeyError

    def add_stamp(self, uvcoord_mapping, rectangle):
        '''
        Add a stamp to the stamps dictionary storing the uvcoordinate
        mapping and the rectangle

        :param uvcoord_mapping: The name of the uvcoord mapping to use
        :param rectangle: The rectangle describing the position size an
                          rotation of the stampt
        :return: The index under which the stamp is stored in the dictionary
        '''
        _id = self.free_id
        self.stamps[_id] = (uvcoord_mapping, rectangle)
        # Go to the next free id
        self.free_id += 1
        return _id

    def remove_stamp(self, _id):
        '''
        Remove a stamp from the stamps dictionary

        :param _id: The id of the stamp to be removed
        '''
        self.stamps.pop(_id)

    def change_uvcoord_mapping(self, _id, uvcoord_mapping):
        '''
        Change the uvcoord mapping of a stamp

        :param _id: The id of the stamp to alter
        :param uvcoord_mapping: The name of the uvcoord_mapping to use
        '''
        self.stamps[_id] = (uvcoord_mapping, self.stamps[_id][1])

    def change_rectangle(self, _id, rectangle):
        '''
        Change the rectangle of a stamp

        :param _id: The id of the stamp to alter
        :param rectangle: The rectangle to use
        '''
        self.stamps[_id] = (self.stamps[_id][0], rectangle)

    def generate(self):
        '''
        Generate the vertex and index arrays from the list of stamps
        Therefor all stamps has to be gone throug, the
        calculation of the vertex position happens on the cpu
        so this is very slow.
        '''
        vertex_index = 0
        indices_tuple = ()
        vertices_tuple = ()
        self.vertices = np.array([], dtype=np.float32)
        self.indices = np.array([], dtype=np.uint32)
        m = glm.mat4(1)
        for stamp in self.stamps.values():
            # Create a matrix from the rectangle
            # to transform the vertices
            m = glm.mat4(1)
            m = glm.translate(m, glm.vec3(stamp[1][0], stamp[1][1], 0))
            m = glm.translate(
                m, glm.vec3(0.5 * stamp[1][2], 0.5 * stamp[1][3], 0))
            m = glm.rotate(m, stamp[1][4], glm.vec3(0, 0, 1))
            m = glm.translate(
                m, glm.vec3(-0.5 * stamp[1][2], -0.5 * stamp[1][3], 0))
            m = glm.scale(m, glm.vec3(stamp[1][2], stamp[1][3], 1))

            # Generate the vertices from the rectangle and concat
            # each with the corresponding uvcoords
            # The vertices has to be extended to vec4 to be multiplied
            # with the mat4x4 matrix and then the fourth dimension is
            # discarded
            uvcoords = self.uvmap_dict[stamp[0]]
            vertices_tuple += tuple(m * glm.vec4(0, 0, 0, 1))[:3] + uvcoords[0]
            vertices_tuple += tuple(m * glm.vec4(0, 1, 0, 1))[:3] + uvcoords[1]
            vertices_tuple += tuple(m * glm.vec4(1, 1, 0, 1))[:3] + uvcoords[2]
            vertices_tuple += tuple(m * glm.vec4(1, 0, 0, 1))[:3] + uvcoords[3]
            # Append the indices
            indices_tuple += (vertex_index + 0, )
            indices_tuple += (vertex_index + 1, )
            indices_tuple += (vertex_index + 2, )
            indices_tuple += (vertex_index + 0, )
            indices_tuple += (vertex_index + 2, )
            indices_tuple += (vertex_index + 3, )
            vertex_index += 4
        self.vertices = np.append(
            self.vertices,
            vertices_tuple).astype('float32')
        self.indices = np.append(
            self.indices,
            indices_tuple).astype('int32')

    def generate_and_init(self):
        '''
        Generate and init the the texture atlas
        '''
        self.generate()
        self.init()


if __name__ == '__main__':
    import sdl2
    from sdl2 import video
    import ctypes
    from PIL import Image
    from Akuga.Anudem2D.Util.Shader import Shader
    from Akuga.Anudem2D.Util.Texture2D import Texture2D
    from OpenGL.GL import (
        glClear, GL_COLOR_BUFFER_BIT,
        GL_DEPTH_BUFFER_BIT, glBindVertexArray)

    # Init sdl
    sdl2.SDL_Init(sdl2.SDL_INIT_VIDEO)
    window = sdl2.SDL_CreateWindow(
        b"Drawable Demo",
        sdl2.SDL_WINDOWPOS_UNDEFINED,
        sdl2.SDL_WINDOWPOS_UNDEFINED, 800, 600,
        sdl2.SDL_WINDOW_OPENGL)
    video.SDL_GL_SetAttribute(video.SDL_GL_CONTEXT_MAJOR_VERSION, 4)
    video.SDL_GL_SetAttribute(video.SDL_GL_CONTEXT_MINOR_VERSION, 5)
    video.SDL_GL_SetAttribute(
        video.SDL_GL_CONTEXT_PROFILE_MASK,
        video.SDL_GL_CONTEXT_PROFILE_CORE)
    context = sdl2.SDL_GL_CreateContext(window)

    # Load a shader programme
    shader = Shader(
        open(
            '/home/lyding/Akuga/Akuga/'
            'Anudem2D/Drawables/shaders/texture_atlas_vs.gl').read(),
        open(
            '/home/lyding/Akuga/Akuga/'
            'Anudem2D/Drawables/shaders/texture_atlas_fs.gl').read())

    uvcoord_map = {
        'topleft': ((0, 0), (0, 0.5), (0.5, 0.5), (0.5, 0)),
        'topright': ((0.5, 0), (0.5, 0.5), (1, 0.5), (1, 0)),
        'bottomleft': ((0, 0.5), (0, 1), (0.5, 1), (0.5, 0.5)),
        'bottomright': ((0.5, 0.5), (0.5, 1), (1, 1), (1, 0.5))}

    # Load the image data
    image = Image.open(
        '/home/lyding/Akuga/Akuga/'
        '/Anudem2D/Drawables/res/atlas1.png')
    image_data = np.array(list(image.getdata()), dtype=np.uint8)
    texture2d = Texture2D(256, 256, image_data)

    texture_atlas = TextureAtlas(texture2d, uvcoord_map, shader)
    for i in range(0, 10):
        texture_atlas.add_stamp('topleft', (i * 40, 0, 40, 40, 0))
        texture_atlas.add_stamp('topright', (i * 40, 40, 40, 40, 0))
        texture_atlas.add_stamp('bottomleft', (i * 40, 80, 40, 40, 0))
        texture_atlas.add_stamp('bottomright', (i * 40, 120, 40, 40, 0))

    texture_atlas.generate()
    texture_atlas.full_init()

    # Set view matrix
    view_matrix = glm.lookAt(
        glm.vec3(0, 0, 0),
        glm.vec3(0, 0, -1),
        glm.vec3(0, 1, 0))
    # projection_matrix = glm.perspective(35.0, 1.0, 0.1, 100.0)
    projection_matrix = glm.ortho(0, 400, 400, 0)
    model_matrix = glm.mat4(1)

    shader.use()
    shader.upload_v_matrix(view_matrix)
    shader.upload_p_matrix(projection_matrix)

    running = True
    event = sdl2.SDL_Event()
    while running:
        while sdl2.SDL_PollEvent(ctypes.byref(event)) != 0:
            if event.type == sdl2.SDL_QUIT:
                running = False
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        texture_atlas.draw()

        glBindVertexArray(0)
        sdl2.SDL_GL_SwapWindow(window)
        sdl2.SDL_Delay(10)
    sdl2.SDL_GL_DeleteContext(context)
    sdl2.SDL_DestroyWindow(window)
    sdl2.SDL_Quit()
