'''
This module will contain different camera classes
'''
import glm


class Camera:
    '''
    The camera base class
    '''
    def __init__(self, position, yawpitchroll, scale, fov, aspect_ratio,
                 clipping, up=glm.vec3(0, 1, 0)):
        '''
        Constructs a basic camera class

        :param position: The position of the camera (glm.vec3)
        :param rotation: The roation (glm.vec3) in the format yaw, pitch, roll
        :fov: The min and max field of view (float tuple)
        :ascpect_ratio: The aspect ratio (float)
        :clipping: The nearest and the farest distance to include (float tuple)
        '''
        self.position = position
        self.yawpitchroll = yawpitchroll
        self.fov = fov
        self.aspect_ratio = aspect_ratio
        self.clipping = clipping
        # Calculate the camera up vector
        camera_right = glm.normalize(glm.cross(up, self.direction))
        self.up = glm.normalize(glm.cross(self.direction, camera_right))
        # The essentiell matrices
        self.view = None
        self.projection = None

    def recreate_matrices(self):
        '''
        Recreate the view an projection matrices
        '''
        self.projection = glm.perspective(
            self.fov, self.aspect_ratio, self.near, self.far)
        rotation = glm.mat4(1)
        glm.rotate(rotation, -self.rotation.x, glm.vec3(1, 0, 0))
        glm.rotate(rotation, -self.rotation.y, glm.vec3(0, 1, 0))
        glm.rotate(rotation, -self.rotation.z, glm.vec3(0, 0, 1))
        self.view = glm.translate(glm.mat(1), -self.position) * rotation

    def yaw(self, difference):
        '''
        Add the difference to the yaw angle
        '''
        self.yawpitchroll[0] += difference

    def pitch(self, difference):
        '''
        Add the difference to the pitch angle
        '''
        self.yawpitchroll[1] += difference

    def roll(self, difference):
        '''
        Add the difference to the yaw angle
        '''
        self.yawpitchroll[2] += difference

