'''
This module contains a representation of a 2d texture as it will be
used as the sampler2d uniform value.
'''
import numpy as np
from OpenGL.GL import (
    glGenTextures,
    glTexParameteri,
    glBindTexture,
    glTexImage2D,
    glGetTexImage,
    GL_RGBA,
    GL_UNSIGNED_BYTE,
    GL_TEXTURE_2D,
    GL_TEXTURE_WRAP_S,
    GL_TEXTURE_WRAP_T,
    GL_TEXTURE_MIN_FILTER,
    GL_TEXTURE_MAG_FILTER,
    GL_REPEAT,
    GL_NEAREST)

from OpenGL.GL import glGetError
from OpenGL.GLU import gluErrorString

from Akuga.Anudem2D.Util.LoggedGL import glCall


class Texture2D:
    '''
    Represents a 2d texture with the following parameter hardcoded
    WRAP_S: repeat
    WRAP_T: repeat
    MIN/MAG_FILTER: linear
    pixel format: rgba, unsigned byte
    '''
    def __init__(self, width, height, image_data):
        '''
        Constructor for the Texture2D class

        :param width: The width of the image
        :param height: The height of the image
        :param image_data: The data of the image
        :return: An instance of the Texture2D class
        '''
        self.width = width
        self.height = height
        self.image_data = image_data
        self.id = glCall(glGenTextures, (1,))
        error = glGetError()
        print(f'GLERROR {error} :' + str(gluErrorString(error)))
        print('Create a Texture2D: ' + str(self.id))

    def init(self):
        '''
        Configure the texture buffer and
        set the widht, height and image_data of the texture and
        upload it to the gpu

        :param widht: The width of the texture
        :param height: The height of the texture
        :param image_data: The image_data
        '''
        with self:
            glCall(glTexParameteri, (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT))
            glCall(glTexParameteri, (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT))
            glCall(glTexParameteri, (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST))
            glCall(glTexParameteri, (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST))
            self.upload_image_data()

    def bind(self):
        '''
        Bind the texture
        '''
        glCall(glBindTexture, (GL_TEXTURE_2D, self.id))

    def unbind(self):
        '''
        Unbind the texture
        '''
        glCall(glBindTexture, (GL_TEXTURE_2D, 0))

    def upload_image_data(self):
        '''
        Uploads the image data to the gpu,
        requires the texture to be bound
        '''
        glCall(glTexImage2D, (
            GL_TEXTURE_2D, 0, GL_RGBA, self.width, self.height,
            0, GL_RGBA, GL_UNSIGNED_BYTE, self.image_data))

    def download_image_data(self):
        '''
        Download the image data from the gpu
        requires the texture to be bound
        '''
        pixel_buf = glCall(glGetTexImage, (GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE))
        self.image = np.frombuffer(pixel_buf, dtype=np.uint8)

    def __enter__(self):
        '''
        Bind the texture
        '''
        self.bind()

    def __exit__(self, type, value, traceback):
        '''
        Unbind the texture
        '''
        self.unbind()
