'''
This module contains a representation for a sound effect using the
sdlmixer library
'''
from sdl2.sdlmixer import (
    Mix_Pause,
    Mix_Resume,
    Mix_HaltChannel,
    Mix_PlayChannel,
    Mix_VolumeChunk,
    Mix_FadeInChannel,
    Mix_FadeOutChannel)


class Sound:
    '''
    Represent a sound effect using the sdl mixer library
    '''
    def __init__(self, sound_chunk):
        self.sound_chunk = sound_chunk
        self.volume = 1
        self.channel = -1

    def play(self, volume=128, loops=0):
        '''
        Set the volume and play the sound

        :param volume: The volume of the sound [0, 128]
        :param loops: The number of times the sound should be looped
                      -1 for infinite looping, 0 for no looping
        '''
        self.set_volume(volume)
        self.channel = Mix_PlayChannel(-1, self.sound_chunk, loops)

    def fade_in(self, volume, loops, ms):
        '''
        Set the sound volume and play the sound fading in

        :param volume: The volume of the sound [0, 128]
        :param loops: The number of times the sound should be looped
                      -1 for infinite looping, 0 for no looping
        :param ms: The milliseconds to fade in
        '''
        self.set_volume(volume)
        Mix_FadeInChannel(self.channel, self.sound_chunk, loops, ms)

    def fade_out(self, ms):
        '''
        Make sound fade out
        :param ms: The milliseconds to fade out
        '''
        Mix_FadeOutChannel(self.channel, ms)

    def stop(self):
        '''
        Just stop the sound
        '''
        Mix_HaltChannel(self.channel)

    def pause(self):
        '''
        Just pauses the sound
        '''
        Mix_Pause(self.channel)

    def resume(self):
        '''
        Just resume the sound
        '''
        Mix_Resume(self.channel)

    def set_volume(self, volume):
        '''
        Set the volume of the sound
        The volume has to be in the range [0, 128]
        '''
        Mix_VolumeChunk(self.sound_chunk, volume)


if __name__ == '__main__':
    from sdl2 import SDL_Init, SDL_INIT_AUDIO, SDL_Quit, SDL_Delay
    from sdl2.sdlmixer import (
        Mix_OpenAudio,
        Mix_LoadWAV,
        Mix_Playing,
        Mix_CloseAudio,
        MIX_DEFAULT_FORMAT)
    from sdl2.ext import Resources
    from sdl2.ext.compat import byteify

    # Load the res directory as a ressource dir
    ressources = Resources(__file__, 'res')
    # Init the sdl and mixer system
    SDL_Init(SDL_INIT_AUDIO)
    Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 1024)
    # Get the sound file
    sound_file = ressources.get_path('sound1.wav')
    # Get a sample
    sample = Mix_LoadWAV(byteify(sound_file, "utf-8"))
    # Create a sound instance
    sound1 = Sound(sample)
    sound1.play()
    while Mix_Playing(sound1.channel):
        SDL_Delay(100)
    # Close all
    Mix_CloseAudio()
    SDL_Quit(SDL_INIT_AUDIO)
