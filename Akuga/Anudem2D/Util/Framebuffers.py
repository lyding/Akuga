'''
This module contains representatios of framebuffer objects
'''

from OpenGL.GL import (
    glGenFramebuffers,
    glBindFramebuffer,
    glTexParameteri,
    glTexImage2D,
    glFramebufferTexture2D,
    GL_RGBA,
    GL_UNSIGNED_BYTE,
    GL_TEXTURE_2D,
    GL_LINEAR,
    GL_TEXTURE_MIN_FILTER,
    GL_TEXTURE_MAG_FILTER,
    GL_FRAMEBUFFER,
    GL_COLOR_ATTACHMENT0)


from Akuga.Anudem2D.Util.Texture2D import Texture2D
from Akuga.Anudem2D.Util.LoggedGL import glCall


class Framebuffer:
    '''
    Represent a plain framebuffer object without any attachments
    '''
    def __init__(self, width, height):
        '''
        The constructor of the Framebuffer class

        :param width: The width of the framebuffer
        :param height: The height of the framebugger
        :return: An instance of the ColorFramebuffer class
        '''
        self.width = width
        self.height = height
        self.fbo = glCall(glGenFramebuffers, (1))

    def bind(self):
        '''
        Bind the framebuffer
        '''
        glCall(glBindFramebuffer, (GL_FRAMEBUFFER, self.fbo))

    def unbind(self):
        '''
        Unind the framebuffer
        '''
        glCall(glBindFramebuffer, (GL_FRAMEBUFFER, 0))

    def __enter__(self):
        '''
        Just bind the framebuffer
        '''
        self.bind()

    def __exit__(self, type, value, traceback):
        '''
        Just unbind the framebuffer
        '''
        self.unbind()


class ColorFramebuffer(Framebuffer):
    '''
    Represents a framebuffer with color attachment
    '''
    def __init__(self, width, height):
        '''
        The constructor of the ColorFramebuffer class
        :param width: The width of the framebuffer
        :param height: The height of the framebuffer
        '''
        super().__init__(width, height)
        self.texture2d = Texture2D(width, height, None)

    def init(self):
        '''
        Init the framebuffer and the texture2d without uploading
        the image data to the gpu. As no image data is provided.
        '''
        with self:
            with self.texture2d:
                glCall(glTexParameteri, (
                    GL_TEXTURE_2D,
                    GL_TEXTURE_MIN_FILTER,
                    GL_LINEAR))
                glCall(glTexParameteri, (
                    GL_TEXTURE_2D,
                    GL_TEXTURE_MAG_FILTER,
                    GL_LINEAR))
                glCall(glTexImage2D, (
                    GL_TEXTURE_2D, 0, GL_RGBA, self.width, self.height,
                    0, GL_RGBA, GL_UNSIGNED_BYTE, None))

            glCall(glFramebufferTexture2D, (
                GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                GL_TEXTURE_2D, self.texture2d.id, 0))
