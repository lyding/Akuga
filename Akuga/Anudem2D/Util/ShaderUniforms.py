'''
Provides a setter dictionary to select the correct setter function
depending on the datatype of a uniform.

Supporte uniforms are:
    signed int
    unsigned int
    float
    mat4x4
'''

import glm
from OpenGL.GL import (
    glUniform1i,
    glUniform1ui,
    glUniform1f,
    glUniform2f,
    glUniform3f,
    glUniform4f,
    glUniformMatrix4fv,
    GL_FALSE)

from Akuga.Anudem2D.Util.LoggedGL import glCall


def uniform_setter_closure(location, datatype):
    '''
    The closure to parametrize the uniform setter function
    to the location of the uniform
    '''
    def uniform_setter(value):
        UNIFORM_SETTER_DICT[datatype](location, value)
    return uniform_setter


def set_float2_uniform(location, vec2):
    '''
    Set a 2d vector uniform to the currently used shader programme

    :param location: The location of the uniform
    :param vec2: The 2d vector to set (every iterable datatype storing floats
                 is suitable)
    '''
    glCall(glUniform2f, (location, vec2[0], vec2[1]))


def set_float3_uniform(location, vec3):
    '''
    Set a 3d vector uniform to the currently used shader programme

    :param location: The location of the uniform
    :param vec3: The 3d vector to set (every iterable datatype storing floats
                 is suitable)
    '''
    glCall(glUniform3f, (location, vec3[0], vec3[1], vec3[2]))


def set_float4_uniform(location, vec4):
    '''
    Set a 4d vector uniform to the currently used shader programme

    :param location: The location of the uniform
    :param vec4: The 4d vector to set (every iterable datatype storing floats
                 is suitable)
    '''
    glCall(glUniform4f, (location, vec4[0], vec4[1], vec4[2], vec4[3]))


def set_matrix4x4_uniform(location, matrix):
    '''
    Set a 4x4 float matrix uniform to the currently used shader programme

    :param locatiion: The location of the uniform
    :param matrix: The matrix uniform to set
    '''
    glCall(glUniformMatrix4fv, (location, 1, GL_FALSE, glm.value_ptr(matrix)))


def set_sampler2d_uniform(location, texture):
    '''
    In pure opengl a sampler2d uniform is set to an integer representing
    the corresponding texture unit. In Anudem2D a sampler2D is represented
    by an instance of the Texture2D class which stores the texture_unit index,
    the texture buffer as well as width, height and pixel data.
    Setting a sampler2D uniform means to activate the texture unit and
    bind the texture
    '''
    texture.activate()


# The integer representation of the datatypes
# Signed integers
SINT = 0
# Unsigned integers
UINT = 1
# Float
FLOAT = 2
FLOAT2 = 3
FLOAT3 = 4
FLOAT4 = 5
# Mat4x4 matrix
M4x4 = 100
# Sampler
SAMPLER2D = 200


# Create the uniform setter dict
# Each setter function is of the form (location, value)
UNIFORM_SETTER_DICT = {
    SINT: glUniform1i,
    UINT: glUniform1ui,
    FLOAT: glUniform1f,
    FLOAT2: set_float2_uniform,
    FLOAT3: set_float3_uniform,
    FLOAT4: set_float4_uniform,
    M4x4: set_matrix4x4_uniform,
    SAMPLER2D: set_sampler2d_uniform}
