'''
This module implement classes needed by the observer pattern.
'''


class Observer():
    '''
    A representation of an Observer which can observe multiple
    observable objects and can be registered to different event types
    '''
    # A list of all existing observers, used by the fire
    # method of the event
    observers = []
    activation_queue = []

    def __init__(self):
        '''
        The constructor for the observer class
        '''
        # Append this instance to the list of observers
        self.observers.append(self)
        self.observed_events = {}
        self.active = True

    def free(self):
        '''
        Remove the observer from the observers list to remove
        the refrence to the observer. This function has to be
        invoked before the observer can be deleted
        '''
        self.deactivate()
        index = Observer.observers.index(self)
        _ = Observer.observers.pop(index)

    def observe(self, event_type, callback):
        '''
        Insert a record of the form (event_type: callback) to the list
        of observables. This way the callback function will be invoked
        if an event with this type fires
        '''
        self.observed_events[event_type] = callback

    def ignore(self, event_type):
        '''
        Ignore an event which was observerd before
        '''
        del self.observed_events[event_type]

    def activate(self):
        '''
        Activate the observer
        '''
        self.active = True

    def activate_delayed(self):
        '''
        Use this method if you want to activate an observer as a reaction
        to an event and you do not wish the activated observer to
        trigger due to the event which activated it in the first place
        '''
        Observer.activation_queue.append(self)

    def deactivate(self):
        '''
        Deactivate the observer
        '''
        self.active = False


class Event():
    def __init__(self, _type, data):
        '''
        The constructor for the Event class

        :param _type: The type of the event
        :param data: The data dictionary shipped with the event
        '''
        self.type = _type
        self.data = data

    def fire(self):
        '''
        Invoke the callback functions of all observers observing this
        type of event.

        Technically not the observers are checking for the events their
        are observing but the event checks for each observer if this
        observer is observing for an event of this type
        and invoke its callbackfunction
        '''
        for observer in Observer.observers:
            if observer.active and self.type in observer.observed_events:
                # Look up the stored callback function and invoke
                # it with the data dictionary of the event as the
                # argument
                observer.observed_events[self.type](self.data)
        # Now activate all observers which are pending to be activated
        # due to fired event
        for observer in Observer.activation_queue:
            observer.activate()
        Observer.activation_queue.clear()


if __name__ == '__main__':
    observer1 = Observer()
    observer2 = Observer()

    # Make both observers observe Event 0
    observer1.observe(0, lambda x: print('Observer1, Event 0: ' + str(x)))
    observer2.observe(0, lambda x: print('Observer2, Event 0: ' + str(x)))

    # Make both observers observer specific events 1 and 2
    observer1.observe(1, lambda x: print('Observer1, Event 1: ' + str(x)))
    observer2.observe(2, lambda x: print('Observer2, Event 2: ' + str(x)))

    event = Event(0, {})
    event1 = Event(1, {'1': 'Parameter 1'})
    event2 = Event(2, {'2': 'Parameter 2'})
    event.fire()
    event1.fire()
    event2.fire()
    print('Deactivate observer2')
    observer2.deactivate()
    event1.fire()
    event2.fire()
    print('Activate observer2')
    observer2.activate()
    event1.fire()
    event2.fire()
    print('Free observer2')
    observer2.free()
    # Remove the last reference to the observer2
    # so that __del__ is invoked. Write a verbose
    # __del__ method to check it
    observer2 = None
    event.fire()
