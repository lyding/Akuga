'''
This module contains a representation of a 2d texture as it will be
used as the sampler2d uniform value.
'''
from OpenGL.GL import (
    glActiveTexture,
    GL_TEXTURE0)

from Akuga.Anudem2D.Util.LoggedGL import glCall


class Texture2DUniform():
    '''
    The Texture2DUniform class holds a reference to a Texture2D
    (aggregates it) and append the texture unit integer
    as additional information to this.
    '''
    def __init__(self, texture2d, texture_unit):
        '''
        Constructor for the Texture2DUniform class

        :param texture2d: The texture2d to use
        :param texture_unit: The texture_unit on the gpu
        :return: An instance of the Texture2DUniform class
        '''
        self.texture2d = texture2d
        self.texture_unit = GL_TEXTURE0 + texture_unit

    def activate(self):
        '''
        Set the corresponding texture unit to be active
        and bound the texture
        '''
        glCall(glActiveTexture, (self.texture_unit,))
        self.texture2d.bind()

    def inactivate(self):
        '''
        Set the active texture unit to zero again and unbind
        the texture2d
        '''
        glCall(glActiveTexture, (0,))
        self.texture2d.unbind()

    def __enter__(self):
        '''
        Not just bind but activate the texture2duniform
        '''
        self.activate()

    def __exit__(self):
        '''
        Inactivate the texture
        '''
        self.inactivate()
