'''
This module contains the interface between SDL2 and Anudem2D events.
'''
from Akuga.Anudem2D.Util.ObserverPattern import Event


def convert_sdl_to_anudem_events(sdl_event):
    '''
    SDL2 uses an event queue while Anudem2D uses an observer pattern
    to handel events. Therefor all for every sdl_event an anudem2D event
    has to be created, carrying the sdl event as data
    '''
    event = Event(sdl_event.type, sdl_event)
    event.fire()
