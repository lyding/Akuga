'''
This module provides function to analyse the uniforms and the vertex layout
a shader defines. The list of uniforms returned by the
get_uniforms_from_glsl_code function will be used to provide an interface
to all unifomrs a shader defines. The vertex layout returned by the
get_vertex_layout_from_vertex_shader_code function will be used to
configure the gpus vertex layout automatically.
'''
import re
from OpenGL.GL import (
    GL_FLOAT,
    GL_INT)
from Akuga.Anudem2D.Util.ShaderUniforms import (
    uniform_setter_closure, SINT, UINT,
    FLOAT, FLOAT2, FLOAT3, FLOAT4, M4x4, SAMPLER2D)


class MalformedDefinition(BaseException):
    '''
    An exception thrown by the analysator functions if they cant
    parse the definition
    '''
    def __init__(self, message, line_number):
        '''
        Create an istance of MalformedUniformDefinition

        :param message: The error message to pass
        :param line_number: The line_number the error occured at
        :return: MalformedDefinition
        '''
        super().__init__(message)
        self.line_number = line_number

    def __str__(self):
        return super().__str__() + " at " + str(self.line_number)


# This dict is used to translate the uniform datatypes into
# the inner integer representation of the datatype
UNIFORM_TRANSLATION_DICT = {
    'int': SINT,
    'uint': UINT,
    'float': FLOAT,
    'vec2': FLOAT2,
    'vec3': FLOAT3,
    'vec4': FLOAT4,
    'mat4': M4x4,
    'sampler2D': SAMPLER2D}


def get_uniforms_from_glsl_code(glsl_code):
    '''
    Analyse the glsl code for the uniform keyword and try to determine
    the uniforms datatype location and name. Also parse special flags
    appenden it a comment in the same line.
    For now only the ANUDOEM2D_UFF (uniform flavour flag) exists.
    It specifies if the uniform is an uniform dependend of the object
    like a model matrix or independend like view or projection matrix.
    The uff has to be specified!
    It has the following form:
        // ANUDEM2D_UFF: [obj_dependend/obj_independend];

    :params glsl_code: The code of the shader programme
    :return: A 2tupel of lists of uniforms. First the dependend and then
             the independend uniforms are returned.
             Each uniform is of the form (name, location, dtype, setter_func)
    '''
    # The line number is used to display more accurate error messages
    line_number = 1
    # Convert the datatype strings into integers using the datatype_dict
    # Append found uniforms to the uniforms list
    obj_dependend_uniforms = []
    obj_independend_uniforms = []
    # Check the code for the uniform keywoard line by line
    for line in glsl_code.splitlines():
        # Check if the line is a comment
        if re.search(r'^ *//', line) is not None:
            # The line seems to be a comment so just ignore it
            line_number += 1
            continue
        # Check if the line contains a uniform definition
        if re.search(r'.* uniform .*', line) is None:
            # The uniform keywoard was not found (at least not as code)
            # So just increment the line number and skip this line
            line_number += 1
            continue
        # Get the name of the uniform
        m = re.search(r'(\w+);', line)
        if m is None:
            raise MalformedDefinition(
                'Failed to parse the name',
                line_number)
        name = m.group(1)

        # Get the location of the uniform
        m = re.search(r'=(\d+)', line)
        if m is None:
            raise MalformedDefinition(
                'Failed to parse the location',
                line_number)
        location = int(m.group(1))

        # Get the datatype of the uniform
        m = re.search(r'uniform (\w+)', line)
        if m is None:
            raise MalformedDefinition(
                'Failed to parse the datatype',
                line_number)
        datatype = UNIFORM_TRANSLATION_DICT[m.group(1)]

        # Generate the setter function for the uniform datatype
        setter = uniform_setter_closure(location, datatype)

        # Get the anudem2d_uff (uniform flavour flag)
        m = re.search(r'.*// *ANUDEM2D_UFF: (\w+);', line)
        if m is None:
            raise MalformedDefinition(
                'Failed to parse ANUDEM2D_UFF',
                line_number)
        uff = m.group(1)

        # Store the uniform in the obj dependend or independend
        # list depending on the uff
        if uff.upper() == 'OBJ_DEPENDEND':
            obj_dependend_uniforms.append((name, location, datatype, setter))
        elif uff.upper() == 'OBJ_INDEPENDEND':
            obj_independend_uniforms.append((name, location, datatype, setter))
        else:
            raise MalformedDefinition("Illegal UFF", line_number)
        line_number += 1
    # Sort the uniforms by their locations
    obj_dependend_uniforms.sort(key=lambda x: x[0])
    obj_independend_uniforms.sort(key=lambda x: x[0])
    return (obj_dependend_uniforms, obj_independend_uniforms)


def parse_datatype(token):
    '''
    Parses a token into a datatype as a tupel.
    E.g:
        ivec3 -> (3, GL_INT)

    :param token: The string representing the datatype
    :return: A tuple containing the amount and the basic type: (amount, dtype)
    '''
    vec_index = token.find('vec')
    if vec_index < 0:
        # If 'vec' is not found the datatype is a basic datatype
        # but again the datatype can be determined by the first letter
        dtype = token[0]
        amount = 1
    if vec_index == 0:
        # If 'vec' is the very start of the string its a vector of floats
        # and the amount is just appended
        dtype = 'f'
        amount = int(token[3:])
    if vec_index == 1:
        # If 'vec' is prepended by one character this character symbold
        # the basic datatype of the vector
        dtype = token[0]
        amount = int(token[4:])
    return (amount, dtype)


def get_vertex_layout_from_vertex_shader_code(vertex_shader_code):
    '''
    Analyses the vertex shader code for the keywoard 'in' and determines
    the location and the datatype of the vertex attribute.
    The vertex layout is a tuple containing a list of vertex attributes
    and the line stride of the vertex layout.
    The vertex attribute has the form (location, offset, amount, dtype)

    The following code:
        layout (location=0) in vec3 in_position;
        layout (location=1) in vec2 in_uvcoords;
    Would be represented as:
        ([(0, 0, 3, GL_FLOAT), (1, 12, 2, GL_FLOAT)], 20)

    :param vertex_shader_code: The vertex shader code
    :return: The vertex laout
    '''
    # The line number is used to display more accurate error messages
    line_number = 1
    # Append found uniforms to the uniforms list
    vertex_attributes = []
    # Map the datatype abreviation to the GL constant and the size in bytes
    datatype_dict = {
        'f': (GL_FLOAT, 4),
        'i': (GL_INT, 4)}
    # Check the code for the uniform keywoard line by line
    for line in vertex_shader_code.splitlines():
        # Check if the line is a comment
        if re.search(r'^ *//', line) is not None:
            # The line seems to be a comment so just ignore it
            line_number += 1
            continue
        if re.search(' in ', line) is None:
            # If the keywoard 'in' was not found just skip this line
            line_number += 1
            continue
        # Get the name of the vertex attribute
        m = re.search(r'(\w+);', line)
        if m is None:
            raise MalformedDefinition(
                'Failed to parse the name',
                line_number)
        name = m.group(1)
        # Get the location of the vertex attribute
        m = re.search(r'=(\d+)', line)
        if m is None:
            raise MalformedDefinition(
                'Failed to parse the location',
                line_number)
        location = int(m.group(1))
        # Get the datatype of the uniform
        m = re.search(r'in (\w+)', line)
        if m is None:
            raise MalformedDefinition(
                'Failed to parse the datatype',
                line_number)
        datatype = parse_datatype(m.group(1))
        vertex_attributes.append((name, location, *datatype))
        line_number += 1
    # Now all vertex attributes are in an unsorted list, to make the
    # calculation of the offset more easily the list of vertex attributes
    # have to be sorted by their locations
    vertex_attributes.sort(key=lambda x: x[0])
    offset = 0
    vertex_layout = []
    for a in vertex_attributes:
        # The vertex layout has the form
        # (name, location, offset, amount, dtype)
        vertex_layout.append(
            (a[0], a[1], offset, a[2], datatype_dict[a[3]][0]))
        # Calculate the offset of the new vertex attribute here
        # the layout is still (name, location, amount, dtype)
        offset += a[2] * datatype_dict[a[3]][1]
    # The offset after the last attribute was added is equal to
    # the line stride
    return vertex_layout, offset


if __name__ == '__main__':
    code = open(
        '/home/lyding/Desktop/opengl/python_tests/'
        'triangle3d_textured/vertex_shader.gl').read()
    print(get_uniforms_from_glsl_code(code))
    print(get_vertex_layout_from_vertex_shader_code(code))
