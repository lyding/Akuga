'''
This module contains an animation chain, which
defacto represent a time line in which animations can be
inserted and connected to SoundEffects.
All animations in the animation chain share a same global time,
the animation chain is an observer class and therefor can be
controlled by events
'''

from Akuga.Anudem2D.Util.ObserverPattern import Observer


class AnimationChain(Observer):
    '''
    Represents an animation chain in which events can be inserted
    and connected to sound effects. All animation in the animation chain
    share the same global time, the AnimatioChain can be controlled
    by events, as it inherits from the Observer class
    '''
    def __init__(self, effects):
        '''
        The constructor for the AnimatioChain

        :param effects: A dictionary of the form name: (animation, sound)
                        storing all effects in the AnimationChain
        '''
        self.effects = effects

    def add_effect(self, name, animated_sprite, sound_effect):
        '''
        Add an effect to the effect dictionary

        :param name: The name of the animation
        :param animated_sprite: The animated sprite to add
        :param sound_effect: The sound effect connected to the animation,
                             this may be None
        '''
        self.effects[name] = (animated_sprite, sound_effect)

    def del_effect(self, name):
        '''
        Remove an effect by its name

        :param name: The name of the effect to remove
        '''
        self.effect.pop(name)

    def pause(self):
        '''
        Pause the AnimationChain
        '''
        self.pause = True

    def resume(self):
        '''
        Resume the AnimationChain
        '''
        self.pause = False

    def rewind(self):
        '''
        Rewind the AnimationChain
        '''
        self.time = 0

    def update(self, time):
        pass
