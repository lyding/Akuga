'''
This module contains function related to parsing windows
fnt bitmap fonts.
'''
import shlex  # contains quoted string preserving split


def parse_token(token, tokens_dict, converter=int):
    '''
    Parse a token that means a string of the form "name=value"
    And add it to the tokens_dict
    By default the value is converted to int, but this can be changed
    by converter.

    :param token: The token to parse
    :param tokens_dic: The dictionary of parsed tokens
    :param converter: The converter function to use to convert the value
    '''
    name, value = token.split('=')
    try:
        tokens_dict[name] = converter(value)
    except ValueError:
        tokens_dict[name] = value


def parse_fnt(fnt_fd):
    '''
    Parse the fnt file by reading it line by line,
    every token which is not part of a 'char' line which describes
    a glyph is considered to be metadata.
    '''
    info_tokens = {}
    common_tokens = {}
    page_tokens = {}
    glyphs = {}
    for line in fnt_fd.readlines():
        tokens = shlex.split(line)
        if tokens[0] == 'info':
            for token in tokens[1:]:
                parse_token(token, info_tokens)
        if tokens[0] == 'common':
            for token in tokens[1:]:
                parse_token(token, common_tokens)
        if tokens[0] == 'page':
            for token in tokens[1:]:
                parse_token(token, page_tokens)
        if tokens[0] == 'char':
            glyph_tokens = {}
            for token in tokens[1:]:
                parse_token(token, glyph_tokens)
            # Convert the integer id into a string id
            # so the glyphs are stored under the their string
            # representation as key
            glyph_tokens['id'] = chr(glyph_tokens['id'])
            glyphs[glyph_tokens['id']] = glyph_tokens
    # Every glyph should also contain its position and dimensions
    # in uvcoordinates so it can be used as a stamp for the
    # TextureAtlas class
    for key, value in glyphs.items():
        x = value['x'] / common_tokens['scaleW']
        # Here 1 - is used to flip the y coordinate as the uvcoord
        # have the origin in the bottom left corner while the pixel coords
        # have the origin in the top left corner!
        y = value['y'] / common_tokens['scaleH']
        width = value['width'] / common_tokens['scaleW']
        height = value['height'] / common_tokens['scaleH']
        glyphs[key]['uvmap'] = ((x, y), (x, y + height),
                                (x + width, y + height), (x + width, y))
    return info_tokens, common_tokens, page_tokens, glyphs


if __name__ == "__main__":
    fd = open(
        '/home/lyding/Akuga/Akuga/'
        'Anudem2D/GUI/res/booter.fnt')
    info, common, page, glyphs = parse_fnt(fd)
    print(glyphs['A'])
