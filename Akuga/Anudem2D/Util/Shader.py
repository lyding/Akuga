'''
In the Anudem2D game library a shader represents more than just the
programme running on the gpu. It provides an interface to all uniforms
the shader programme provides and specifies the vertex layout it needs.
Both, the uniforms and vertex layout are parsed from the glsl code.
'''
from ctypes import byref
from ctypes import c_void_p as voidp

from OpenGL.GL import (
    shaders,
    glGenBuffers,
    glGenVertexArrays,
    glBindBuffer,
    glBindVertexArray,
    glBufferData,
    glVertexAttribPointer,
    glEnableVertexAttribArray,
    glUseProgram,
    glClear,
    glDrawElements,
    GL_VERTEX_SHADER,
    GL_FRAGMENT_SHADER,
    GL_ARRAY_BUFFER,
    GL_ELEMENT_ARRAY_BUFFER,
    GL_UNSIGNED_INT,
    GL_FALSE,
    GL_STATIC_DRAW,
    GL_COLOR_BUFFER_BIT,
    GL_DEPTH_BUFFER_BIT,
    GL_TRIANGLES)


from Akuga.Anudem2D.Util.GLSLAnalyser import (
    get_uniforms_from_glsl_code,
    get_vertex_layout_from_vertex_shader_code)

from Akuga.Anudem2D.Util.LoggedGL import glCall


def load_shader_from_single_file(shader_fd):
    '''
    If the both the shader and vertex shader are specified in one
    file the code has to follow two rules

    1. There has to be a line #define $SHADER_STAGE_SWITCH
       The variable $SHADER_STAGE_SWITCH will be replaced with either
       VERTEX_SHADER or FRAGMENT_SHADER

    2. Both code chunk have to be embraced with #ifdef VERTEX_SHADER..#endif
       or #ifdef FRAGMENT_SHADER..#endif
       This way only the vertex or fragment code is loaded depending
       on te SHADER_STAGE_SWITCH variable

    The code will be prepended with the correct define
    for the vertex and fragment shader code automatically
    '''

    shader_code = shader_fd.read()
    return Shader(
        shader_code.replace('$SHADER_STAGE_SWITCH', 'VERTEX_SHADER'),
        shader_code.replace('$SHADER_STAGE_SWITCH', 'FRAGMENT_SHADER'))


class Shader:
    '''
    The Anudem2D representation of a shader. It generates an interface
    to the uniforms and the vertex layout directly from the glsl code
    '''
    # This static variable will store the active shader on the gpu,
    # this way the shader programme has to be updated only if its
    # different from the active shader
    active_shader = None

    def __init__(self, vertex_shader_code, fragment_shader_code):
        '''
        Constructor for the Shader class

        :param vertex_shader_code: The code of the vertex shader
        :param fragment_shader_code: The code of the fragmen shader
        :return: An instance of the Shader class
        '''
        # Get the static and dynamic uniforms
        self.depending_uniforms, self.independing_uniforms =\
            get_uniforms_from_glsl_code(
                vertex_shader_code +
                fragment_shader_code)
        # Get the vertex layout
        self.vertex_layout, self.vertex_layout_stride =\
            get_vertex_layout_from_vertex_shader_code(vertex_shader_code)
        # Compile and link the shader programme
        vertex_shader = shaders.compileShader(
            vertex_shader_code,
            GL_VERTEX_SHADER)
        fragment_shader = shaders.compileShader(
            fragment_shader_code,
            GL_FRAGMENT_SHADER)
        self.programme = shaders.compileProgram(vertex_shader, fragment_shader)

        for vertex_attribute in self.vertex_layout:
            # Go throug all vertex attributes and expose a getter
            # function to get the vertex attributes offset
            setattr(self, 'get_' + vertex_attribute[0] + '_offset',
                    (lambda self, va: lambda: va[2])(self, vertex_attribute))

        for uniform in self.independing_uniforms:
            # Go through all independend uniforms and expose its setter
            # function to the user by adding it to the shaders interface
            setattr(self, 'upload_' + uniform[0], uniform[3])

    def configure_vertex_layout(self):
        '''
        Configure the gpu to use the parsed vertex layout
        '''
        # Just invoke glVertexAttribPointer on all attributes
        # and enable them
        for vertex_attribute in self.vertex_layout:
            glCall(glVertexAttribPointer, (
                vertex_attribute[1],  # Position
                vertex_attribute[3],  # Amount
                vertex_attribute[4],  # Datatype
                GL_FALSE,
                self.vertex_layout_stride,   # Stride
                voidp(vertex_attribute[2])))  # Offset
            glCall(glEnableVertexAttribArray, (vertex_attribute[1],))

    def use(self):
        '''
        Use the shader
        '''
        if self is not Shader.active_shader:
            glCall(glUseProgram, (self.programme,))
            Shader.active_shader = self

    def __enter__(self):
        self.use()
        return self

    def __exit__(self, type, value, traceback):
        pass


if __name__ == '__main__':
    import glm
    import sdl2
    from sdl2 import video
    import numpy as np

    # Init sdl2
    sdl2.SDL_Init(sdl2.SDL_INIT_VIDEO)
    window = sdl2.SDL_CreateWindow(
        b'Drawable Demo',
        sdl2.SDL_WINDOWPOS_UNDEFINED,
        sdl2.SDL_WINDOWPOS_UNDEFINED, 800, 600,
        sdl2.SDL_WINDOW_OPENGL)
    video.SDL_GL_SetAttribute(video.SDL_GL_CONTEXT_MAJOR_VERSION, 4)
    video.SDL_GL_SetAttribute(video.SDL_GL_CONTEXT_MINOR_VERSION, 5)
    video.SDL_GL_SetAttribute(
        video.SDL_GL_CONTEXT_PROFILE_MASK,
        video.SDL_GL_CONTEXT_PROFILE_CORE)
    context = sdl2.SDL_GL_CreateContext(window)

    # Create vertice array
    vertices = np.array([-1, -1, 0,
                         -1, 1, 0,
                         1, 1, 0,
                         1, -1, 0], dtype=np.float32)

    # Create index array
    indices = np.array([0, 1, 2, 0, 2, 3], dtype=np.uint32)

    # A buffer is required for both, the vertices (vbo) and the indices (ebo)
    vbo = glGenBuffers(1)
    ebo = glGenBuffers(1)

    # vc = open(
    #     '/home/lyding/Akuga/Akuga/'
    #     '/Anudem2D/Util/shader_vs.gl').read()
    # fc = open(
    #     '/home/lyding/Akuga/Akuga/'
    #     'Anudem2D/Util/shader_fs.gl').read()
    # my_shader = Shader(vc, fc)

    with open('/home/lyding/Akuga/Akuga'
              '/Anudem2D/Util/shader_single_file.gl') as shader_fd:
        my_shader = load_shader_from_single_file(shader_fd)

    # Create a vao which whill bound the vbo, the ebo and the vertex attributes
    vao = glGenVertexArrays(1)
    glBindVertexArray(vao)

    glBindBuffer(GL_ARRAY_BUFFER, vbo)
    glBufferData(
        GL_ARRAY_BUFFER, len(vertices.tobytes()), vertices, GL_STATIC_DRAW)

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo)
    glBufferData(
        GL_ELEMENT_ARRAY_BUFFER, len(indices.tobytes()),
        indices, GL_STATIC_DRAW)

    my_shader.configure_vertex_layout()

    # Unbind the vao again
    glBindVertexArray(0)

    # Use the shader programme
    my_shader.use()

    # Set the matrices
    model_matrix = glm.translate(glm.mat4x4(), glm.vec3(0, 0, 2))
    view_matrix = glm.lookAt(
        glm.vec3(0, 0, 0),
        glm.vec3(0, 0, 1),
        glm.vec3(0, 1, 0))
    proj_matrix = glm.perspective(35.0, 1.0, 0.1, 100.0)
    my_shader.upload_w_matrix(model_matrix)
    my_shader.upload_v_matrix(view_matrix)
    my_shader.upload_p_matrix(proj_matrix)

    running = True
    event = sdl2.SDL_Event()
    while running:
        while sdl2.SDL_PollEvent(byref(event)) != 0:
            if event.type == sdl2.SDL_QUIT:
                running = False

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glBindVertexArray(vao)
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, None)
        glBindVertexArray(0)
        sdl2.SDL_GL_SwapWindow(window)
        sdl2.SDL_Delay(10)

    # Close the opengl context and close the window
    sdl2.SDL_GL_DeleteContext(context)
    sdl2.SDL_DestroyWindow(window)
    sdl2.SDL_Quit()
