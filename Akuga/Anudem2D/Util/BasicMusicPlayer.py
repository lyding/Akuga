'''
This module contains a player for music controlled by events
'''
from sdl2.sdlmixer import (
    Mix_PlayMusic,
    Mix_PauseMusic,
    Mix_FadeInMusic,
    Mix_FadeOutMusic,
    Mix_HaltMusic,
    Mix_VolumeMusic,
    Mix_ResumeMusic,
    Mix_RewindMusic,
    Mix_HookMusicFinished)

from Akuga.Anudem2D.Util.ObserverPattern import Observer
from Akuga.Anudem2D.Util.EventDefinitions import (
    BMP_PLAY,
    BMP_FADE_IN,
    BMP_FADE_OUT,
    BMP_PAUSE,
    BMP_STOP,
    BMP_SET_VOLUME,
    BMP_RESUME,
    BMP_REWIND,
    BMP_SET_FINISHED_CALLBACK)


class MusicPlayer(Observer):
    '''
    A representation of a music player which can be used to play different
    musics controlled by events
    '''

    def __init__(self, music_chunks={}):
        '''
        The contstructor for the MusicPlayer class

        :param music_chunks: A dictionary of the form
                             {music_name: music_chunk} storing all
                             playable music chunks
        '''
        super().__init__()
        # A dictionary of the form {music_name: music_chunk} storing
        # all playable music chunks
        self.music_chunks = music_chunks
        self.observe(BMP_PLAY, self.play_music)
        self.observe(BMP_FADE_IN, self.fade_in_music)
        self.observe(BMP_FADE_OUT, self.fade_out_music)
        self.observe(BMP_PAUSE, self.pause_music)
        self.observe(BMP_STOP, self.stop_music)
        self.observe(BMP_SET_VOLUME, self.set_volume)
        self.observe(BMP_RESUME, self.resume_music)
        self.observe(BMP_REWIND, self.rewind_music)
        self.observe(
            BMP_SET_FINISHED_CALLBACK,
            self.set_music_finished_callback)

    def add_music(self, music_name, music_chunk):
        '''
        Create a new register in music_chunks dictionary

        :param music_name: The name of the music to add
        :param music_chunk: The chunk of the music to add
        '''
        self.music_chunks[music_name] = music_chunk

    def del_music(self, music_name):
        '''
        Remove a music from the music chunks dictionary

        :param music_name: The name of the music to remove
        '''
        self.music_chunks.pop(music_name)

    def play_music(self, data):
        '''
        Just play a music, this will be used as a callback function to
        observer PLAY_MUSIC events

        :param data: A dictionary containing the data to be passed for
                     this callback function. e.g:
                     {'name': music_name, 'loops': loops}
        '''
        Mix_PlayMusic(self.music_chunks[data['name']], data['loops'])

    def fade_in_music(self, data):
        '''
        Start to play and fade in music, this will be used as a
        callback function to observer FADE_IN_MUSIC events

        :param data: A dictionary containing the data to be passed forr
                     this callback function. e.g:
                     {'name': music_name, 'loops': loops, 'ms': fade_in_ms}
        '''
        Mix_FadeInMusic(
            self.music_chunks[data['name']],
            data['loops'], data['ms'])

    def fade_out_music(self, data):
        '''
        Stop to play and fade out music, this will be used as a
        callback function to observer FADE_OUT_MUSIC events

        :param data: A dictionary containing the data to be passed forr
                     this callback function. e.g:
                     {ms': fade_in_ms}
        '''
        Mix_FadeOutMusic(data['ms'])

    def pause_music(self, data):
        '''
        Just pause the currently playing music, this will be used as
        a callback function to listen for PAUSE_MUSIC events

        :param data: Not used
        '''
        Mix_PauseMusic()

    def stop_music(self, data):
        '''
        Just stop the currently playing music, this will be used as
        a callback function to listen for STOP_MUSIC events

        :param data: Not used
        '''
        Mix_HaltMusic()

    def set_volume(self, data):
        '''
        Set the volume of the music, this will be used as
        a callback function to listen for SET_VOLUME events

        :param data: A dictionary containing the data to be passed for
                     this callback function. e.g:
                     {'volume': volume}
                     Volume has to be in the range [0, 1.0]
        '''
        Mix_VolumeMusic(data['volume'] * 128)

    def resume_music(self, data):
        '''
        Resume the current music, this will be used as
        a callback function to listen for RESUME_MUSIC events

        :param data: Not used
        '''
        Mix_ResumeMusic()

    def rewind_music(self, data):
        '''
        Rewind the current music, this will be used as
        a callback function to listen for RESUME_MUSIC events

        :param data: Not used
        '''
        Mix_RewindMusic()

    def set_music_finished_callback(self, data):
        '''
        Set the callback function invoked when the music stops,
        this will be used as a callback function to listen for
        SET_MUSIC_FINISHED_CALLBACK events.
        The callbackfunction has to be a ctype function returning
        and accepting nothing

        :param datra: A dictionary containin the data to be passed
                      for this callback function. e.g:
                      {'callback': callback_function}
        '''
        Mix_HookMusicFinished(data['callback'])


if __name__ == '__main__':
    from ctypes import (CFUNCTYPE)
    from sdl2 import SDL_Init, SDL_INIT_AUDIO, SDL_Quit, SDL_Delay
    from sdl2.sdlmixer import (
        Mix_OpenAudio,
        Mix_LoadMUS,
        Mix_PlayingMusic,
        Mix_CloseAudio,
        MIX_DEFAULT_FORMAT)
    from sdl2.ext import Resources
    from sdl2.ext.compat import byteify
    from Akuga.Anudem2D.Util.ObserverPattern import Event

    # Load the res directory as a ressource dir
    ressources = Resources(__file__, 'res')
    # Init the sdl and mixer system
    SDL_Init(SDL_INIT_AUDIO)
    Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 1024)
    # Get the sound file
    sound_file = ressources.get_path('11.mp3')
    # Create a music player
    player = MusicPlayer()
    # Get a sample
    sample = Mix_LoadMUS(byteify(sound_file, "utf-8"))
    # Add the sound to the sound player
    player.add_music('music1', sample)

    # Create a callback function
    clb_functype = CFUNCTYPE(None)

    def callback():
        print('callback!')
    # Create a c compatible callback function
    c_callback = clb_functype(callback)

    # Create and fire a PLAY_SOUND event
    event = Event(BMP_PLAY, {'name': 'music1', 'loops': 0})
    event.fire()

    while Mix_PlayingMusic():
        SDL_Delay(100)

    Mix_CloseAudio()
    SDL_Quit(SDL_INIT_AUDIO)
