'''
This module contains an advanced music player controlled by events
'''
from random import randint
from ctypes import CFUNCTYPE
from sdl2.sdlmixer import (
    Mix_PauseMusic,
    Mix_FadeInMusic,
    Mix_FadeOutMusic,
    Mix_HaltMusic,
    Mix_VolumeMusic,
    Mix_ResumeMusic,
    Mix_RewindMusic,
    Mix_HookMusicFinished)

from Akuga.Anudem2D.Util.ObserverPattern import Observer
from Akuga.Anudem2D.Util.EventDefinitions import (
    AMP_SET_ACTIVE_PLAYLIST,
    AMP_FADE_TO_PLAYLIST,
    AMP_SET_ACTIVE_SONG,
    AMP_SET_FADEIN,
    AMP_SET_FADEOUT,
    AMP_PAUSE,
    AMP_RESUME,
    AMP_REWIND,
    AMP_STOP,
    AMP_SET_VOLUME,
    AMP_NEXT,
    AMP_SET_MODE)


# THIS IS NOT A METHOD
def sdlmixer_music_hook():
    '''
    This is the music hook function passed to sdl_mixer
    as the ctype callback function invoked whenever a song
    is finished. As passing a method using a clojure resulted
    in segfaults this function just fires the next event
    so the next song starts to play
    '''
    event = Event(AMP_NEXT, {})
    event.fire()


class AdvancedMusicPlayer(Observer):
    '''
    A representation of an advanced music player which can be
    used to play and manipulate a playlist event-driven.
    '''
    NORMAL = 0
    SHUFFLE = 1
    CALLBACK_TYPE = CFUNCTYPE(None)

    def __init__(self, fadein, fadeout, volume=1.0, music_chunks={}):
        '''
        The construtor for the advanced music player

        :params fadein: The seconds to fade in to a song
        :params fadeout: The seconds to fade out from a song
        :params volume: The volume of the player [0, 1.0]
        :params music_chunks: A dictionary of the form {music_name: chunk}
        '''
        super().__init__()
        self.fadein = fadein
        self.fadeout = fadeout
        self.volume = volume
        self.music_chunks = music_chunks
        # A dictionary of the form {'playlist_name': [music1, music2...]}
        self.playlists = {}
        # The active playlist
        self.active_playlist = None
        # The index of the active song -1 for initialisation
        # cause load_next_song is also set to True this will be
        # incremented to 0 which is valid or choosen randomly but valid
        self.active_song_index = -1
        # The mode how the next song should be selected
        self.mode = self.NORMAL
        # Specifies if a new song should be started
        self.load_next_song = True
        # Specifies if the player is currently fading to an other playlist
        self.switching_playlist = False
        # Specified if the player is currently fading to an other
        # user specified song
        self.switching_song = False
        # The playlist to switch to
        self.next_playlist = None
        # Just the time when the last loaded song started
        # This is used with the length of the song and the
        # fadeout to determine when the Mix_FadeOutMusic
        # function should be invoked
        self.song_start_timestamp = 0
        # This flag will be used to ignore calls to music_finished_callback
        # if the music was halted by the user as it should change the song
        self.stopped = False

        # Set the volume
        Mix_VolumeMusic(int(self.volume * 128))
        # Set the music finished callback function
        Mix_HookMusicFinished(self.CALLBACK_TYPE(sdlmixer_music_hook))
        # Listen for AMP (advanced music player) events
        self.observe(AMP_SET_ACTIVE_PLAYLIST, self.set_active_playlist)
        self.observe(AMP_FADE_TO_PLAYLIST, self.fade_to_playlist)
        self.observe(AMP_SET_ACTIVE_SONG, self.set_active_song)
        self.observe(AMP_SET_FADEIN, self.set_fadein)
        self.observe(AMP_SET_FADEOUT, self.set_fadeout)
        self.observe(AMP_PAUSE, self.pause)
        self.observe(AMP_RESUME, self.resume)
        self.observe(AMP_REWIND, self.rewind)
        self.observe(AMP_STOP, self.stop)
        self.observe(AMP_SET_VOLUME, self.set_volume)
        self.observe(AMP_NEXT, self.next)
        self.observe(AMP_SET_MODE, self.set_mode)

    def add_music(self, music_name, music_chunk, music_len):
        '''
        Add music to the dictionary of playable music

        :param music_name: The name of the music to play
        :param music_chunk: The chunk of the music
        :param music_len: The length of the music in seconds
        '''
        self.music_chunks[music_name] = (music_chunk, music_len)

    def del_music(self, music_name):
        '''
        Remove music from the dictionary of playable music

        :param music_name: The name of the music to remove
        '''
        self.music_chunks.pop(music_name)

    def add_playlist(self, playlist_name, playlist):
        '''
        Add a playlist to the advanced music player

        :param playlist_name: The name of the playlist
        :param playlist: A list containing the names of the
                         songs in the playlist
        '''
        self.playlists[playlist_name] = playlist

    def del_playlist(self, playlist_name):
        '''
        Just remove a playlist

        :param playlist_name: The name of the playlist to remove
        '''
        self.playlist.pop(playlist_name)

    def add_song_to_playlist(self, playlist_name, music_name):
        '''
        Add a song to a playlist, the music has to be added to
        the advanced music palyer first

        :param playlist_name: The name of the playlist
        :param music_name: The name of the music to add to the playlist
        '''
        self.playlists[playlist_name].append(music_name)

    def del_song_from_playlist(self, playlist_name, music_name):
        '''
        Remove a song from a playlist

        :param playlist_name: The name of the playlist
        :param music_name: The name of the music to remove from the playlist
        '''
        index = self.playlists[playlist_name].index(music_name)
        self.playlists[playlist_name].pop(index)

    def set_active_playlist(self, data):
        '''
        Switch the active playlist,
        this will be used as the callback function to listen
        for SET_ACTIVE_PLAYLIST events

        :param data: A dictionary storing the arguments,
                     it has to contain a field 'playlist_name'
                     holding the name of the playlist to switch to
        '''
        # Set the switching playlist flag to reset the song index
        # in the update function
        self.switching_playlist = True
        # Set this flag to load the new song
        self.load_next_song = True
        playlist_name = data['playlist_name']
        self.next_playlist = self.playlists[playlist_name]

    def fade_to_playlist(self, data):
        '''
        Fade out and in to a new playlist without

        :param data: A dictionary storing the arguments,
                     it has to contain a field 'playlist_name'
                     holding the name of the playlist to switch to
        '''
        # Set the switching playlist flag to reset the song index
        # in the update function
        self.switching_playlist = True
        playlist_name = data['playlist_name']
        self.next_playlist = self.playlists[playlist_name]
        Mix_FadeOutMusic(int(self.fadeout * 1000))

    def set_active_song(self, data):
        '''
        Switch the active song,
        this will be used as the callback function to listen
        for SET_ACTIVE_SONG events

        :param data: A dictionary storing the arguments,
                     it has to contain a field 'song_name'
                     holding the name of the song to switch to
        '''
        # Set the switching song flag to set the song index
        # in the update function
        self.switching_song = True
        self.next_index = self.active_playlist.index(data['song_name'])
        Mix_FadeOutMusic(int(self.fadeout * 1000))

    def set_fadein(self, data):
        '''
        Set the seocnds to fade in before every new song,
        this will be used as a callback functoin to listen
        for SET_FADEIN events.

        :param data: A dictionary storing the arguments,
                     it has to contain a field 'fadein'
                     specifying the seconds to fade in
        '''
        self.fadein = int(data['fadein'] * 1000)

    def set_fadeout(self, data):
        '''
        Set the seconds to fade out before every new song
        This will be used as a callback function to listen
        for SET_FADEOUT_MS events

        :param data: A dictionaring storing the arguments,
                     it has to contain a field 'fadeout'
                     specifying the seconds to fade out
        '''
        self.fadeout = int(data['fadeout'] * 1000)

    def pause(self, data):
        '''
        Pause the music.
        This will be used as a callback function to listen
        for PAUSE_MUSIC events

        :param data: Not used
        '''
        self.stopped = True
        Mix_PauseMusic()

    def resume(self, data):
        '''
        Resume the music.
        This will be used as a callback function to listen
        for RESUME_MUSIC events

        :param data: Not used
        '''
        self.stopped = False
        Mix_ResumeMusic()

    def rewind(self, data):
        '''
        Rewind the music.
        This will be used as a callback function to listen
        for REWIND_MUSIC events

        :param data: Not used
        '''
        self.stopped = False
        Mix_RewindMusic()

    def stop(self, data):
        '''
        Stop the music and set the stopped flag to true,
        this will be used to eventually ignore calls to
        music_finished_callback.
        This will be used as a callback function to listen
        for STOP_MUSIC events

        :param data: Not used
        '''
        self.stopped = True
        Mix_HaltMusic()

    def next(self, data):
        '''
        This will be invoked whenever a song stop.
        It just set the load_next_song flag which
        will be used in the update function to start
        the new song. How the new song is selected
        is determined by the mode flag.
        This will be used as a callback function to listen
        for AMP_NEXT events

        :param data: Not used
        '''
        if self.stopped is False:
            self.load_next_song = True

    def set_volume(self, data):
        '''
        Set the volume of the music, it has to be in the range [0, 1.0]
        This will be used as a callback function to listen for
        SET_VOLUME events

        :param data: A dictionary storing the arguments,
                     it has to contain a field 'volume'
                     specifying the volume

        '''
        self.volume = int(data['volume'] * 128)
        Mix_VolumeMusic(self.volume)

    def set_mode(self, data):
        '''
        Set the mode of the player which defines how
        the next song is determined.
        This will be used as a callback function to listen
        for AMP_SET_MODE events.

        :param data: A dictionary storing the arguments,
                     it has to contain a field 'mode'
                     specifying the mode. Currently
                     self.NORMAL and self.SHUFFLE are defined
        '''
        self.mode = data['mode']

    def update(self, ellapsed_time):
        '''
        The update function which will handle the
        fade in and fade out between two songs
        '''
        # Do nothing if the media player is paused
        if not self.stopped:
            if self.load_next_song:
                if self.switching_playlist:
                    # If the playlist is switched
                    # set the index to -1, it will be incremented to 0 which is
                    # valid or will be choosen randomly but valid
                    self.active_song_index = -1
                    self.switching_playlist = False
                    self.active_playlist = self.next_playlist
                    self.next_playlist = None
                if self.mode == self.NORMAL:
                    # Play the playlist in order
                    self.active_song_index += 1
                    # Make sure the index does not exceed the range
                    # of the active playlist
                    self.active_song_index %= len(self.active_playlist)
                if self.mode == self.SHUFFLE:
                    # Randomly select songs from the playlist
                    self.active_song_index = randint(
                        0, len(self.active_playlist) - 1)
                if self.switching_song:
                    # If the user want the player to switch to a specific song
                    # overwrite the selected song and set it to next_song
                    # which stores the selected song
                    self.active_song_index = self.next_index
                    self.next_index = None
                    self.switching_song = False
                # Update the timestamp for the new song
                self.song_start_timestamp = ellapsed_time
                # Wait again until the new song stops before
                # a new song is selected
                self.load_next_song = False
                # Now fade in to the new selected song
                Mix_FadeInMusic(
                    self.music_chunks[
                        self.active_playlist[self.active_song_index]][0],
                    0, int(self.fadein * 1000))

            # Get the seconds since the current song is playing
            seconds_since_song = ellapsed_time - self.song_start_timestamp
            if seconds_since_song >= self.music_chunks[self.active_playlist[
                    self.active_song_index]][1] - self.fadeout:
                # If the current song plays longer than its duration minus
                # the seconds to fade out start to fade out
                Mix_FadeOutMusic(int(self.fadeout * 1000))


if __name__ == '__main__':
    from time import time
    from sdl2 import SDL_Init, SDL_INIT_AUDIO, SDL_Delay
    from sdl2.sdlmixer import (
        Mix_OpenAudio,
        Mix_LoadMUS,
        Mix_CloseAudio,
        MIX_DEFAULT_FORMAT)
    from sdl2.ext import Resources
    from sdl2.ext.compat import byteify
    from Akuga.Anudem2D.Util.ObserverPattern import Event

    # Init the sdl and mixer system
    SDL_Init(SDL_INIT_AUDIO)
    Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 1024)

    # Load the res directory as a ressource dir
    ressources = Resources(__file__, 'res/')
    # Get the paths
    path11 = ressources.get_path('11.mp3')
    path12 = ressources.get_path('12.mp3')
    path21 = ressources.get_path('21.mp3')
    path22 = ressources.get_path('22.mp3')
    # Load the songs
    song11 = Mix_LoadMUS(byteify(path11, 'utf-8'))
    song12 = Mix_LoadMUS(byteify(path12, 'utf-8'))
    song21 = Mix_LoadMUS(byteify(path21, 'utf-8'))
    song22 = Mix_LoadMUS(byteify(path22, 'utf-8'))

    # Create an advanced music player
    player = AdvancedMusicPlayer(1.0, 1.0)

    # Add the songs to the advance music player
    player.add_music('11', song11, 3 * 60 + 51)
    player.add_music('12', song12, 3 * 60 + 56)
    player.add_music('21', song21, 6)
    player.add_music('22', song22, 4)

    player.add_playlist('ftu', ['11', '12'])
    player.add_playlist('test', ['21', '22'])

    event = Event(AMP_SET_ACTIVE_PLAYLIST, {'playlist_name': 'ftu'})
    event.fire()
    # event = Event(AMP_SET_MODE, {'mode': AdvancedMusicPlayer.SHUFFLE})
    # event.fire()

    t0 = time()
    ellapsed_time = 0
    oneshot = False
    while True:
        ellapsed_time = time() - t0
        player.update(ellapsed_time)
        SDL_Delay(50)
        if ellapsed_time > 20 and not oneshot:
            oneshot = True
            event = Event(AMP_SET_ACTIVE_SONG, {'song_name': '12'})
            event.fire()
    Mix_CloseAudio()
