'''
This module contains just integer definitions of event types
'''

# Sound and music related events
# TODO: Change the name of the sound events
from sdl2.events import SDL_USEREVENT


def define_event(_id):
    '''
    The define event is used to add an event definition and makes sure
    that the id is above the SDL_USEREVENT so that the
    Anudem2D events never overlap with sdl events.
    This is needed as Anudem2D uses sdl2 events internally with
    the observer pattern. In case Anudem2D events and sdl events
    overlap incredible magical bugs and is to be expected.
    '''
    return SDL_USEREVENT + _id


BMP_PLAY_SOUND = define_event(100)
BMP_STOP_SOUND = define_event(101)
BMP_SET_MASTER_VOLUME = define_event(102)

BMP_PLAY = define_event(103)
BMP_FADE_IN = define_event(104)
BMP_PAUSE = define_event(105)
BMP_STOP = define_event(106)
BMP_SET_VOLUME = define_event(107)
BMP_RESUME = define_event(108)
BMP_REWIND = define_event(109)
BMP_SET_POSITION = define_event(110)
BMP_FADE_OUT = define_event(111)
BMP_SET_FINISHED_CALLBACK = define_event(112)

AMP_SET_ACTIVE_PLAYLIST = define_event(150)
AMP_FADE_TO_PLAYLIST = define_event(151)
AMP_SET_ACTIVE_SONG = define_event(152)
AMP_SET_FADEIN = define_event(153)
AMP_SET_FADEOUT = define_event(154)
AMP_PAUSE = define_event(155)
AMP_RESUME = define_event(156)
AMP_REWIND = define_event(157)
AMP_STOP = define_event(158)
AMP_SET_VOLUME = define_event(159)
AMP_NEXT = define_event(160)
AMP_SET_MODE = define_event(161)

SEP_PLAY = define_event(197)
SEP_STOP = define_event(198)
SEP_SET_VOLUME = define_event(199)

BUTTON_LEFT_CLICK = define_event(250)
BUTTON_RIGHT_CLICK = define_event(251)
BUTTON_MIDDLE_CLICK = define_event(252)
BUTTON_X1_CLICK = define_event(253)
BUTTON_X2_CLICK = define_event(254)
