'''
This module contains a player for sound effects controlled by events
'''
from Akuga.Anudem2D.Util.ObserverPattern import Observer
from Akuga.Anudem2D.Util.EventDefinitions import (
    SEP_PLAY,
    SEP_STOP,
    SEP_SET_VOLUME)


class SoundEffectPlayer(Observer):
    '''
    A representation of a sound effect player which can be used
    to play different sound effect controlled by events
    '''
    def __init__(self, master_volume=1.0):
        '''
        The constructor for the SoundEffectPlayer class.

        :param master_volume: The master volume for all sounds [0, 1.0]
        '''
        super().__init__()
        self.master_volume = master_volume
        self.sound_dict = {}
        # Register for the play_sound, stop_sound and
        # set_master_volume event
        self.observe(SEP_PLAY, self.play_sound)
        self.observe(SEP_STOP, self.stop_sound)
        self.observe(SEP_SET_VOLUME, self.set_master_volume)

    def add_sound(self, name, sound):
        '''
        Add a sound to the internal sound dictionary
        '''
        self.sound_dict[name] = sound

    def del_sound(self, name):
        '''
        Delete a sound from the internal sound dictionary
        '''
        self.sound_dict.pop(name)

    def play_sound(self, data):
        '''
        Play a sound from the internal sound dictionary,
        this is used as the callback function to observe
        PLAY_SOUND events. The data parameter, a dictionary,
        has to contain a field 'name' for the name of the sound
        to play and a field 'volume' for the volume of this
        particular sound effect and a field params, a list,
        specifying the parameters for the play function.

        e.g:
            data = {'name': summoning_sound, 'volume': 128, 'loops': 0}
        '''
        self.sound_dict[data['name']].play(
            int(data['volume'] * self.master_volume), data['loops'])

    def stop_sound(self, data):
        '''
        Stop a sound of the internal sound dictionary.
        This function is used as the callback function to observe
        STOP_SOUND events. The data parameter, a dictionary,
        has to contain a field 'name' specifying the name of
        the sound to stop
        '''
        self.sound_dict[data['name']].stop()

    def set_master_volume(self, data):
        '''
        Set the master volume. Data, a dictionary, has to contain
        a field 'master_volume' specifying the master volume to set
        '''
        self.master_volume = data['master_volume']


if __name__ == '__main__':
    from sdl2 import SDL_Init, SDL_INIT_AUDIO, SDL_Quit, SDL_Delay
    from sdl2.sdlmixer import (Mix_OpenAudio, Mix_LoadWAV,
                               Mix_CloseAudio, MIX_DEFAULT_FORMAT)
    from sdl2.ext import Resources
    from sdl2.ext.compat import byteify
    from Akuga.Anudem2D.Util.Sound import Sound
    from Akuga.Anudem2D.Util.ObserverPattern import Event

    # Load the res directory as a ressource dir
    ressources = Resources(__file__, 'res')
    # Init the sdl and mixer system
    SDL_Init(SDL_INIT_AUDIO)
    Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 1024)
    # Get the sound file
    sound_file = ressources.get_path('sound1.wav')
    # Create a sound player
    player = SoundEffectPlayer(1.0)
    # Get a sample
    sample = Mix_LoadWAV(byteify(sound_file, "utf-8"))
    # Add the sound to the sound player
    player.add_sound('sound1', Sound(sample))

    # Create and fire a PLAY_SOUND event
    event = Event(SEP_PLAY, {'name': 'sound1', 'volume': 128, 'loops': 0})
    event.fire()
    SDL_Delay(1000)
    event.fire()
    SDL_Delay(1000)

    Mix_CloseAudio()
    SDL_Quit(SDL_INIT_AUDIO)
