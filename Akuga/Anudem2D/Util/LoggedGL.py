from OpenGL.GL import (
        glClear,
        glClearColor,
        glBindBuffer,
        glBindTexture,
        glBindVertexArray,
        glBindFramebuffer,
        glGenBuffers,
        glGenVertexArrays,
        glGenFramebuffers,
        glGenTextures,
        glBufferData,
        glDrawElements,
        glEnable,
        glVertexAttribPointer,
        glEnableVertexAttribArray,
        glBlendFunc,
        glTexImage2D,
        glGetTexImage,
        glFramebufferTexture2D,
        glUseProgram,
        glUniform1i,
        glUniform1ui,
        glUniform1f,
        glUniform2f,
        glUniform3f,
        glUniform4f,
        glUniformMatrix4fv,
        glTexParameteri,
        glActiveTexture)


logging = True


def glCall(func, args):
    if logging:
        print('GL_Call: ' + func.__name__ + ' ' + str(args))
    return func(*args)
