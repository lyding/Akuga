import threading
from json import loads
from Akuga.JumonSet import serialize_set


def user_from_database_response(response, communicator, client_address):
    """
    Create and return a user instance created from
    a databse response
    """
    # Convert the coma delimited strings into jumon sets
    collection = loads(response[4])
    set1 = loads(response[5])
    set2 = loads(response[6])
    set3 = loads(response[7])
    return User(response[0], response[1], response[2], response[3],
                collection, set1, set2, set3, response[8],
                response[9], response[10],
                communicator, client_address)


class User:
    """
    Represents a registered user aka one of the poor souls doomed to
    play Akuga
    name: name of the player
    pass_hash: the md5 hash of its password
    credits: The credits a user owns
    set[0,1,2]: The three jumon sets a user once created
    communicator : the StreamSocketCommunicator the user communicates with
    client_address: The address of the communicator
    """
    def __init__(self, name, pass_hash, email_hash,
                 credits, collection, set0, set1, set2,
                 fame, exp, level, communicator, client_address):
        self.name = name
        self.pass_hash = pass_hash
        self.email_hash = email_hash
        self.credits = credits
        self.fame = fame
        self.collection = collection
        self.sets = [set0, set1, set2]
        self.communicator = communicator
        self.exp = exp
        self.level = level
        self.percentage_to_next_level = 0.7
        self.client_address = client_address
        # The active set (will always set before the user is enqueued)
        self.active_set = 0
        # Flag which store if the user is in the game_scene
        # It is used by the match server to wait for all users to arrive
        self.__in_game_scene = False
        # Check if the match is initiated but the client is not yet
        # in the game scene
        self.__initiate_match = False
        # Is the user is actually enqueued? It will be used to only allow
        # certain actions as long as the user is in the queue
        self.__enqueued = False
        # Make the user class threadsafe even if it should never be altered
        # by more than one thread
        self.lock = threading.Lock()

    def set_in_game_scene(self, flag):
        """
        Set the in_game_scene flag. Thread-safe
        """
        with self.lock:
            self.__in_game_scene = flag 

    def get_in_game_scene(self):
        """
        Get the in_game_scene flag. Thread-safe
        """
        with self.lock:
            return self.__in_game_scene

    def set_initiate_match(self, flag):
        """
        Set the initiate_match flag. Thread-safe
        """
        with self.lock:
            self.__initiate_match = flag 

    def get_initiate_match(self):
        """
        Set the initiate_match flag. Thread-safe
        """
        with self.lock:
            return self.__initiate_match

    def set_enqueued(self, flag):
        """
        Set the enqueued flag. Thread-safe
        """
        with self.lock:
            self.__enqueued = flag 

    def get_enqueued(self):
        """
        Get the enqueued flag. Thread-safe
        """
        with self.lock:
            return self.__enqueued

    def get_collection_serialized(self):
        """
        Return all elements of the collection list as a ',' seperated string.
        This way it can be send to the database server
        """
        return serialize_set(self.collection)

    def get_set_serialized(self, index):
        """
        Return all elements of the set as a ',' seperated string.
        This way it can be send to the database server
        """
        return serialize_set(self.sets[index])

    def get_stats(self):
        """
		Return all stats which are neccesarry for the game client to run
		These are: credits, fame, percentage_to_next_level,
				   jumon_collection, jumon_set1, jumon_set2, jumon_set3
		"""
		# The name is not part of the sets cause the name is already
		# set in the client. It was used to log in
        stats = {}
        stats['credits'] = int(self.credits)
        stats['fame'] = int(self.fame)
        stats['exp'] = int(self.exp)
        stats['percentage_to_next_level'] = self.percentage_to_next_level
        stats['jumon_collection'] = self.collection
        stats['jumon_set1'] = self.sets[0]
        stats['jumon_set2'] = self.sets[1]
        stats['jumon_set3'] = self.sets[2]
        return stats

    def __str__(self):
        """
        Returns the user as a string
        """
        return "User: " + self.name + " " + self.pass_hash + " " +\
            self.email_hash + " " + str(self.credits) + " " +\
            str(self.collection) + " " +\
            str(self.sets) + " " + str(self.client_address) + " " + \
            str(self.in_play)
