"""
This module contains the implementation of a Queue,
which is a used to enqueue users for a match.
An own implementation is needed cause the python
queue does not allow arbitrary access which is
needed if a player wants to withdraw from the queue
"""
import threading

class Queue:
    '''
    '''
    def __init__(self):
        self.__data = []
        self.__lock = threading.Lock()

    def put(self, item):
        '''
        Enqueue an item

        :param item: The item to enqueue
        '''
        with self.__lock:
            self.__data.append(item)

    def get(self, number=1):
        '''
        Get item(s) from beginning of the queue

        :param number: The number of items to get
        :returns: A list of items

        :note: This function is unsafe, check if
               the queue contains enough elements
               before using get.
        '''
        with self.__lock:
            items = self.__data[0:number]
            del self.__data[0: number]
            return items

    def contains(self, number):
        '''
        Check wether the queue contains at least
        a certain amount of items

        :param number: The number of items to check for
        :returns: True or False
        '''
        with self.__lock:
            return len(self.__data) >= number

    def remove(self, item):
        '''
        Remove an item from the queue

        :param item: The item to remove
        :returns: None
        '''
        with self.__lock:
            self.__data.remove(item)

    def __len__(self):
        '''
        Return the length of the queue
        '''
        with self.__lock:
            return len(self.__data)


if __name__ == "__main__":
    q = Queue()
    for i in range(10):
        q.put(i)
    print('Expecting 10, got ', len(q))
    print(q.get())
    print('Expecting 9, got ', len(q))
    print(q.get(2))
    print('Expecting 7, got ', len(q))
    print('Expecting False, got ', q.contains(20))
    q.remove(9)
    print('Expecting 6, got ', len(q))
