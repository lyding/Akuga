'''
This module contains every function to send emails.
Mailjet is used as the email api
'''

from os import environ
from mailjet_rest import Client
from Akuga.GameServer.GlobalDefinitions import EMAIL_ADDRESS


MAILJET = None
MAILJET_PUBLIC_KEY = None
MAILJET_PRIVATE_KEY = None


def init():
    # global MAILJET
    # global MAILJET_PUBLIC_KEY
    # global MAILJET_PRIVATE_KEY
    # MAILJET_PUBLIC_KEY = environ['MJ_APIKEY_PUBLIC']
    # MAILJET_PRIVATE_KEY = environ['MJ_APIKEY_PRIVATE']
    # MAILJET = Client(auth=(MAILJET_PUBLIC_KEY, MAILJET_PRIVATE_KEY),
    #                  version='v3.1')
    pass


def send_reset_token_to_client(user, client_mail_address, token):
    '''
    Send a randomly generated reset password token to the client.
    It will use the email template as designed by our chief of
    design in the mailjet email template editor
    '''
    # data = {
    #     'Messages': [{
    #         'From': {
    #             'Email': EMAIL_ADDRESS,
    #             'Name': 'Akuga the mythic maschiene'},
    #         'To': [{
    #             'Email': client_mail_address,
    #             'Name': user}],
    #         'Subject': 'Your reset token is here',
    #         'TemplateID': 1647269,
    #         'TemplateLanguage': True,
    #         'Variables': {'reset_token': token}
    #         }
    #     ]
    # }
    # result = MAILJET.send.create(data=data)
    # return result.status_code
    pass


if __name__ == '__main__':
    init()
    print(send_reset_token_to_client(
        'test_user',
        'lukas.brumm@gmx.de',
        '1234'))
