# Network related definitions
SERVER_ADDRESS = ('127.0.0.1', 10315)
USER_DBS_ADDRESS = ('127.0.0.1', 10098)
MAX_ACTIVE_CONNECTIONS = 1024
EMAIL_ADDRESS = "Akuga@protonmail.com"

# User related definitions
START_CREDITS = 444
START_FAME = 4
START_EXP = 0

# MISC
RESET_TOKEN_LIFESPAN = 3600
ANUDEM_NEWS_PATH = 'anudem_news.txt'
