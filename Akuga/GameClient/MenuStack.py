'''
This module contains the implementation of a stack based menu system
'''


class Menu():
    '''
    A menu is basically a collection of gui elements which can be
    updated and rendered
    '''
    def __init__(self, name, menu_stack, opaque=False):
        # Make itself known to the menu stack so this menu can be
        # refered by its name
        self.name = name
        self.menu_stack = menu_stack
        self.opaque = opaque
        # Was the menu initialised?
        self.is_init = False
        self.gui_elements = []
        self.static_elements = []
        self.is_active = False
        menu_stack.register(name, self)

    def init(self):
        '''
        Init all components which have to be initialised
        '''
        for element in self.gui_elements:
            element.full_init()
        for element in self.static_elements:
            element.full_init()
        self.init = True

    def draw(self):
        '''
        Draw the menu
        '''
        for element in self.static_elements:
            element.draw()
        for element in self.gui_elements:
            element.draw()

    def update(self, ellapsed_time):
        '''
        Update the menu
        '''
        for element in self.gui_elements:
            element.update(ellapsed_time)

    def activate(self):
        '''
        Make the menu responsive to UI events
        '''
        for element in self.gui_elements:
            element.activate()
        self.is_active = True

    def deactivate(self):
        '''
        Make the menu unresponsive to UI events
        '''
        for element in self.gui_elements:
            element.deactivate()
        self.is_active = False

    def add_gui_element(self, name, gui_element):
        '''
        Add a new gui element to the menu
        '''
        # If the menu is active active all new gui elements automatically
        if self.is_active:
            gui_element.activate()
        setattr(self, name, gui_element)
        self.gui_elements.append(gui_element)

    def add_static_element(self, name, gui_element):
        '''
        Add a new static element to the menu
        '''
        setattr(self, name, gui_element)
        self.static_elements.append(gui_element)


class MenuStack():
    '''
    The menu stack is somewhat like the engine behind a complete menu,
    the menu stack is rendered from top to down until the first non
    opaque menu is rendered
    '''
    def __init__(self):
        self.menu_name_dict = {}
        self.menus = []
        pass

    def register(self, name, menu):
        '''
        Add a menu to the menu_name_dict
        '''
        self.menu_name_dict[name] = menu

    def push(self, name):
        '''
        Pushed a registered menu on top of the stack,
        also invoke its init method if it was not initialised yet.
        '''
        menu = self.menu_name_dict[name]
        if not menu.is_init:
            menu.init()
        # Make new topmost menu responsive to ui events
        menu.activate()
        self.menus.append(self.menu_name_dict[name])

    def pop(self):
        '''
        Pop the last menu from the stack
        '''
        # make menu to pop unresponsive to ui elements
        if len(self.menus) > 0:
            self.menus[-1].deactivate()
        self.menus.pop()

    def switch(self, menu_name):
        '''
        Replaces the uppermost menu with a different menu
        '''
        self.pop()
        self.push(menu_name)

    def draw(self):
        '''
        Render the menu stack from top to bottom until the first non
        opaque menu is rendered
        '''
        for menu in reversed(self.menus):
            menu.draw()
            if not menu.opaque:
                break

    def update(self, ellapsed_time):
        '''
        Only the last menu is updated, opaque or not
        '''
        self.menus[-1].update(ellapsed_time)
