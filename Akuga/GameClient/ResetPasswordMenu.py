'''
This module contains the implementation of the reset password menu
'''

from pathlib import Path
from sdl2 import SDL_BUTTON_LEFT
from Akuga.GameClient.MenuStack import Menu
from Akuga.Anudem2D.Drawables.TexturedSprite import TexturedSprite
from Akuga.Anudem2D.GUI.Button import Button
from Akuga.Anudem2D.GUI.ButtonLayout import ButtonLayout
from Akuga.Anudem2D.GUI.TextBox import TextBox
from Akuga.GameClient.Protocoll import reset_password, send_pwd_reset_token
from Akuga.GameClient.Network import StreamSocketCommunicator
from Akuga.GameClient.GlobalDefinitions import SCREEN_RECT
import Akuga.GameClient.Ressources as Ressources


class ResetPasswordMenu(Menu):
    '''
    The reset password menu
    '''

    def __init__(self, menu_stack, user, communicator):
        super().__init__('Reset_Password_Menu', menu_stack)
        self.user = user
        self.communicator = communicator
        self.add_static_element('background', TexturedSprite(
                Ressources.DEFAULT_SPRITE_SHADER,
                SCREEN_RECT,
                Ressources.BACKGROUND1))
        self.add_static_element('title', TexturedSprite(
                Ressources.DEFAULT_SPRITE_SHADER,
                (312, 77, 1296, 277, 0),
                Ressources.YOU_FORGOT))
        self.add_static_element('subtitle', TexturedSprite(
                Ressources.DEFAULT_SPRITE_SHADER,
                (579, 347, 767, 106, 0),
                Ressources.YOUR_PASSWORD))

        username_textbox_sprite = TexturedSprite(
                Ressources.DEFAULT_SPRITE_SHADER,
                (574, 483, 771, 58, 0),
                Ressources.TEXTBOX)

        email_textbox_sprite = TexturedSprite(
                Ressources.DEFAULT_SPRITE_SHADER,
                (574, 558, 771, 58, 0),
                Ressources.TEXTBOX)

        password_textbox_sprite = TexturedSprite(
                Ressources.DEFAULT_SPRITE_SHADER,
                (574, 633, 771, 58, 0),
                Ressources.TEXTBOX)

        token_textbox_sprite = TexturedSprite(
                Ressources.DEFAULT_SPRITE_SHADER,
                (574, 708, 771, 58, 0),
                Ressources.TEXTBOX)

        self.add_gui_element('username_textbox', TextBox(
            0, username_textbox_sprite, Ressources.GET_TEXTRENDERER(),
            'username', None, 0.75, 10, 0, 32, (1, 1, 1)))
        self.add_gui_element('email_textbox', TextBox(
            1, email_textbox_sprite, Ressources.GET_TEXTRENDERER(),
            'email', None, 0.75, 10, 0, 32, (1, 1, 1)))
        self.add_gui_element('password_textbox', TextBox(
            2, password_textbox_sprite, Ressources.GET_TEXTRENDERER(),
            'new password', '?', 0.75, 10, 0, 32, (1, 1, 1)))
        self.add_gui_element('token_textbox', TextBox(
            3, token_textbox_sprite, Ressources.GET_TEXTRENDERER(),
            'reset token', None, 0.75, 10, 0, 32, (1, 1, 1)))

        reset_button_sprite = TexturedSprite(
                Ressources.DEFAULT_SPRITE_SHADER,
                (811, 783, 306, 113, 0),
                Ressources.RESET_PASSWORD_BUTTON)

        self.add_gui_element('reset_button', Button(
                4, reset_button_sprite,
                self.reset_button_callback))

        quit_button_sprite = TexturedSprite(
                Ressources.DEFAULT_SPRITE_SHADER,
                (1804, 63, 53, 53, 0),
                Ressources.QUIT_BUTTON)

        self.add_gui_element('quit_button', Button(
                5, quit_button_sprite,
                self.quit_button_callback))

        self.add_static_element(
                'send_token_text',
                Ressources.GET_TEXTRENDERER())
        self.send_token_text.set_text(
                1353, 561, 'Send Reset Token', 0.5, (1, 1, 1))
        self.send_token_text.generate()
        self.send_token_button = ButtonLayout(
                6, [(60, 1353, 571, 200, 25, self.send_token_button_callback)])

    def send_token_button_callback(self, button_template, mouse_mode):
        if mouse_mode == SDL_BUTTON_LEFT:
            send_pwd_reset_token(
                self.communicator,
                self.username_textbox.value,
                self.email_textbox.value)

    def reset_button_callback(self, button_template, mouse_mode):
        if mouse_mode == SDL_BUTTON_LEFT:
            response = reset_password(
                self.communicator, self.username_textbox.value,
                self.password_textbox.value, self.token_textbox.value)
            if response[0] == 0:
                self.menu_stack.switch('Login_Menu')

    def quit_button_callback(self, button_template, mouse_mode):
        self.menu_stack.switch('Login_Menu')


if __name__ == '__main__':
    import sdl2
    import sdl2.ext
    from sdl2 import video
    import socket
    import glm
    from OpenGL.GL import (
        glClear, GL_COLOR_BUFFER_BIT,
        GL_DEPTH_BUFFER_BIT, glBindVertexArray, GL_BLEND, glEnable,
        GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, glBlendFunc, glClearColor)
    from Akuga.Anudem2D.Util.EventInterface import\
        convert_sdl_to_anudem_events
    from Akuga.GameClient.User import User
    from Akuga.GameClient.MenuStack import MenuStack
    from Akuga.GameClient.GlobalDefinitions import (SERVER_ADDRESS, NBYTES)

    sdl2.SDL_Init(sdl2.SDL_INIT_VIDEO)
    window = sdl2.SDL_CreateWindow(
        b"Drawable Demo",
        sdl2.SDL_WINDOWPOS_UNDEFINED,
        sdl2.SDL_WINDOWPOS_UNDEFINED, 1920, 1080,
        sdl2.SDL_WINDOW_OPENGL)
    video.SDL_GL_SetAttribute(video.SDL_GL_CONTEXT_MAJOR_VERSION, 4)
    video.SDL_GL_SetAttribute(video.SDL_GL_CONTEXT_MINOR_VERSION, 5)
    video.SDL_GL_SetAttribute(
        video.SDL_GL_CONTEXT_PROFILE_MASK,
        video.SDL_GL_CONTEXT_PROFILE_CORE)
    context = sdl2.SDL_GL_CreateContext(window)

    socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    socket.connect(SERVER_ADDRESS)
    communicator = StreamSocketCommunicator(socket, NBYTES)

    user = User()

    base_path = Path.cwd() / 'Akuga' / 'GameClient' / 'res'
    with open(base_path / 'ressources.list') as ressources_fd:
        Ressources.load(base_path, ressources_fd)

    menu_stack = MenuStack()
    reset_password_menu = ResetPasswordMenu(
        menu_stack, user, communicator)

    menu_stack.push('Reset_Password_Menu')

    # Set view matrix
    view_matrix = glm.lookAt(
        glm.vec3(0, 0, 0),
        glm.vec3(0, 0, -1),
        glm.vec3(0, 1, 0))
    projection_matrix = glm.ortho(0, 1920, 1080, 0)

    # Upload the view and projection matrix for every shader
    Ressources.TEXT_RENDERER_SHADER.use()
    Ressources.TEXT_RENDERER_SHADER.upload_v_matrix(view_matrix)
    Ressources.TEXT_RENDERER_SHADER.upload_p_matrix(projection_matrix)

    Ressources.DEFAULT_SPRITE_SHADER.use()
    Ressources.DEFAULT_SPRITE_SHADER.upload_v_matrix(view_matrix)
    Ressources.DEFAULT_SPRITE_SHADER.upload_p_matrix(projection_matrix)

    glEnable(GL_BLEND)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

    event = sdl2.SDL_Event()
    running = True
    while running:
        events = sdl2.ext.get_events()
        for event in events:
            if event.type == sdl2.SDL_QUIT:
                running = False
            convert_sdl_to_anudem_events(event)

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glClearColor(1, 0, 1, 0)

        # login_menu.draw()
        menu_stack.draw()

        glBindVertexArray(0)
        sdl2.SDL_GL_SwapWindow(window)
        sdl2.SDL_Delay(10)
    sdl2.SDL_GL_DeleteContext(context)
    sdl2.SDL_DestroyWindow(window)
    sdl2.SDL_Quit()
    communicator.close()
