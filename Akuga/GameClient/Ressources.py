'''
This module loads and provides every ressource file which is needed by
the game
'''
from numpy import array, uint8
from pathlib import Path
from sys import modules
from json import loads as json_loads
from PIL import Image
from Akuga.Anudem2D.Util.Shader import load_shader_from_single_file
from Akuga.Anudem2D.Util.Texture2D import Texture2D
from Akuga.Anudem2D.Util.FNTParser import parse_fnt
from Akuga.Anudem2D.GUI.TextRenderer import TextRenderer


def load_texture(asset_description, basepath):
    '''
    Load a texture2d from a path with specified width and height of the image

    :param asset_description: A dictionary describing the assets,
                              it has to contain at least the items path,
                              width and height
    :return: Returns a Texture2D
    '''
    path = basepath / asset_description['path']
    width = asset_description['width']
    height = asset_description['height']

    image = Image.open(path)
    image_data = array(list(image.getdata()), dtype=uint8)
    texture = Texture2D(width, height, image_data)
    texture.init()
    return texture


def load_shader(shader_desciption, basepath):
    '''
    Load a shader from a path, the shader has to be coded in a single file!

    :param path: A dictionary describing the shader, it has to contain
                 nothing but the path of the shader
    :return: Returns a shader object
    '''
    path = basepath / shader_desciption['path']
    shader = None
    with open(path) as shader_fd:
        shader = load_shader_from_single_file(shader_fd)
    return shader


def load_textrenderer(textrenderer_description, basepath):
    '''
    Load a font from a path and generate a generator function to
    create arbitrarily many text renderers

    :param textrenderer_description: A dictionary describing the textrenderer
                                     it has to contain at least the path to
                                     the fnt file ('fnt_path') the path
                                     to the image file ('img_path'),
                                     the width and height of the image
                                     ('img_width', 'img_height') as well as
                                     the name of the shader to use to render
                                     the text ('shader_name')
                                     The shader has to be already initialised,
                                     therefor the order of the ressources
                                     matter. First declare the shader, than
                                     the textrenderer
    '''
    fnt_path = basepath / textrenderer_description['fnt_path']
    img_path = basepath / textrenderer_description['img_path']
    img_width = textrenderer_description['img_width']
    img_height = textrenderer_description['img_height']
    shader_name = textrenderer_description['shader_name']
    shader = globals()[shader_name]
    font_image = Image.open(img_path)
    font_data = array(list(font_image.getdata()), dtype=uint8)
    font_texture = Texture2D(img_width, img_height, font_data)
    font_texture.init()

    with open(fnt_path) as fnt_fd:
        info, common, page, glyphs = parse_fnt(fnt_fd)

    def get_textrenderer():
        return TextRenderer(
            font_texture, info, common, page, glyphs, shader)
    return get_textrenderer


def load(base_path, ressource_description_fd):
    '''
    Load all ressource files (will become slooooow)
    '''

    # This dictionary will be used to trigger the right loader function
    # depending on the type field of the asset
    type_loader_dict = {
        'png': load_texture,
        'shader': load_shader,
        'textrenderer': load_textrenderer}

    # Get THIS module, it will be used to add extra variables on the fly
    module = modules[__name__]
    # Every entry has to contain at least the type field which describes
    # the type of the ressource and the name which should be used in the
    # python code for this object as well as all additional fields
    # required by the matching loader function
    #
    # e.g {"name": "test", "type": "png", "path": "assets/test.png",
    #      "width": 512, "height": 512}
    for line in ressource_description_fd:
        ressource_description = json_loads(line)
        ressource = type_loader_dict[ressource_description['type']](
                ressource_description, base_path)
        # Now use the specified name to add this ressource directly
        # to this module
        setattr(module, ressource_description['name'], ressource)


def load_from_string(base_path, ressource_description):
    '''
    Load all ressource files (will become slooooow)
    '''
    # This dictionary will be used to trigger the right loader function
    # depending on the type field of the asset
    type_loader_dict = {
        'png': load_texture,
        'shader': load_shader,
        'textrenderer': load_textrenderer}

    lines = ressource_description.split('\n')

    # Get THIS module, it will be used to add extra variables on the fly
    module = modules[__name__]
    # Every entry has to contain at least the type field which describes
    # the type of the ressource and the name which should be used in the
    # python code for this object as well as all additional fields
    # required by the matching loader function
    #
    # e.g {"name": "test", "type": "png", "path": "assets/test.png",
    #      "width": 512, "height": 512}
    for line in lines:
        ressource_description = json_loads(line)
        ressource = type_loader_dict[ressource_description['type']](
                ressource_description, base_path)
        # Now use the specified name to add this ressource directly
        # to this module
        setattr(module, ressource_description['name'], ressource)


if __name__ == '__main__':
    import sdl2
    from sdl2 import video

    sdl2.SDL_Init(sdl2.SDL_INIT_VIDEO)
    window = sdl2.SDL_CreateWindow(
        b"Drawable Demo",
        sdl2.SDL_WINDOWPOS_UNDEFINED,
        sdl2.SDL_WINDOWPOS_UNDEFINED, 800, 600,
        sdl2.SDL_WINDOW_OPENGL)
    video.SDL_GL_SetAttribute(video.SDL_GL_CONTEXT_MAJOR_VERSION, 4)
    video.SDL_GL_SetAttribute(video.SDL_GL_CONTEXT_MINOR_VERSION, 5)
    video.SDL_GL_SetAttribute(
        video.SDL_GL_CONTEXT_PROFILE_MASK,
        video.SDL_GL_CONTEXT_PROFILE_CORE)
    context = sdl2.SDL_GL_CreateContext(window)

    base_path = Path.cwd() / 'Akuga' / 'GameClient' / 'res'

    with open(base_path / 'ressources.list') as ressources_fd:
        load(base_path, ressources_fd)

    print(DEFAULT_SPRITE_SHADER)
    print(AKUGA_TITLE)
    print(LOGIN_BUTTON)
    print(GET_TEXTRENDERER)
    print(GET_TEXTRENDERER())

    sdl2.SDL_GL_DeleteContext(context)
    sdl2.SDL_DestroyWindow(window)
    sdl2.SDL_Quit()
