"""
This module contains all functions related to network communication
"""

from json import (loads, dumps)


class SocketClosed(Exception):
    '''
    The socket module does not raise an error, if the foreign site
    closes the socket unexpectatly, the SocketClosed error will be raised
    to signal this situation
    '''
    pass


class StreamSocketCommunicator:
    def __init__(self, connection, nbytes):
        '''
        connection: The stream socket to work on
        nbytes: Max bytes to read once at a time
        '''
        self.connection = connection
        self.nbytes = nbytes
        self.cached_string = ''

    def receive_line(self):
        '''
        Receives a complete line from a stream socket.
        Therefor the stream of bytes have to be cached and scaned for
        terminator signs. The newline character in this case
        '''
        index = self.cached_string.find('\n')
        while index < 0:
            # As long as there is no complete line cached receive bytes
            # from the wire. Cause the socket is set to nonblocking
            # the BlockingIOError can occure, handel it be returning
            # the empty string
            try:
                new_data = self.connection.recv(self.nbytes).decode('utf-8')
            except BlockingIOError:
                return ""
            if not new_data:
                # If the connection was closed by the foreign host
                # raise a socket error
                raise SocketClosed()
            self.cached_string += new_data
            # Again look for the newline character
            index = self.cached_string.find('\n')
        # Now a complete line is cached so return it and remove it from
        # the cache
        complete_line = self.cached_string[:index]
        self.cached_string = self.cached_string[index + 1:]
        return complete_line

    def recv_packet(self):
        """
        Receive a packet, aka a complete line, from the wire and use json
        to deserialize it. Only lists are accepted as valid datatypes
        """
        json_line = self.receive_line()
        if json_line == "":
            return []
        packet = loads(json_line)
        return packet

    def send_packet(self, tokens):
        """
        Send a python list by serialising it using json
        """
        json_dump = dumps(tokens) + '\n'
        json_dump = json_dump.encode('utf-8')
        self.connection.send(json_dump)

    def close(self):
        """
        Closes the socket the communicator uses and cleares the chache
        """
        self.connection.close()
        self.cached_string = ''

    def refresh(self, new_communicator):
        """
        Overwrite the connection the communicator uses
        but leaves the cache untouched. This will be
        used if a user reconnects while playing
        """
        self.connection = new_communicator.connection
