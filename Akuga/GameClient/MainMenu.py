'''
This module contains the main menu
'''
from pathlib import Path
from sdl2 import SDL_BUTTON_LEFT
from json import loads as json_loads
from Akuga.GameClient.MenuStack import Menu
from Akuga.Anudem2D.Drawables.TexturedSprite import TexturedSprite
from Akuga.Anudem2D.GUI.ButtonLayout import ButtonLayout
from Akuga.GameClient.GlobalDefinitions import SCREEN_RECT
import Akuga.GameClient.Ressources as Ressources
from json.decoder import JSONDecodeError as JSONDecodeError


class MainMenu(Menu):
    '''
    The main menu
    '''
    def __init__(self, menu_stack, user, quit_button_callback):
        super().__init__('Main_Menu', menu_stack)
        self.user = user
        self.add_static_element('background', TexturedSprite(
                Ressources.DEFAULT_SPRITE_SHADER,
                SCREEN_RECT,
                Ressources.BACKGROUND2))
        self.add_static_element('paper', TexturedSprite(
                Ressources.DEFAULT_SPRITE_SHADER,
                (225, 183, 1459, 897, 0),
                Ressources.PAPER1))
        # self.name_text_renderer = Ressources.GET_TEXTRENDERER()
        # self.cash_text_renderer = Ressources.GET_TEXTRENDERER()
        self.news_drawables = []

        self.add_gui_element('button_layout', ButtonLayout(
                1,
                [
                    (11, 232, 0, 292, 119, self.start_button_callback),
                    (12, 524, 0, 292, 119, self.sets_button_callback),
                    (13, 816, 0, 292, 119, self.shop_button_callback),
                    (14, 1108, 0, 292, 119, self.options_button_callback),
                    (15, 1400, 0, 292, 119, quit_button_callback)
                ]))

    def draw(self):
        '''
        Draw the menu
        '''
        super().draw()
        for drawable in self.news_drawables:
            drawable.draw()

    def start_button_callback(self, button_template, mouse_mode):
        self.menu_stack.switch('Start_Menu')

    def sets_button_callback(self, button_template, mouse_mode):
        self.menu_stack.switch('Sets_Menu')

    def shop_button_callback(self, button_template, mouse_mode):
        self.menu_stack.switch('Shop_Menu')

    def options_button_callback(self, button_template, mouse_mode):
        self.menu_stack.switch('Options_Menu')

    def generate_news(self, news_from_anudem):
        '''
        Generate the news from anudem field from the servers
        response of the GET_NEWS command.
        Every line describes either a text or an image to be rendered,
        both described as a json dictionary

        :param news_from_anudem: A list of lines as send by the server
        '''
        for line in news_from_anudem:
            try:
                template = json_loads(line)
            except JSONDecodeError:
                print('Error parsing ' + line)
            if template['type'] == 'text':
                position = template['pos']
                text = template['text']
                scale = template['scale']
                color = template['color']
                text_renderer = Ressources.GET_TEXTRENDERER()
                text_renderer.set_text(
                    position[0],
                    position[1],
                    text,
                    scale,
                    color)
                text_renderer.generate_and_init()
                self.news_drawables.append(text_renderer)
            if template['type'] == 'image':
                rect = template['rect']
                img = template['img']
                sprite = TexturedSprite(
                    Ressources.DEFAULT_SPRITE_SHADER,
                    rect,
                    Ressources.__dict__[img])
                sprite.full_init()
                self.news_drawables.append(sprite)


if __name__ == '__main__':
    import sdl2
    import sdl2.ext
    from sdl2 import video
    import glm
    from OpenGL.GL import (
        glClear, GL_COLOR_BUFFER_BIT,
        GL_DEPTH_BUFFER_BIT, glBindVertexArray, GL_BLEND, glEnable,
        GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, glBlendFunc, glClearColor)
    from Akuga.Anudem2D.Util.EventInterface import\
        convert_sdl_to_anudem_events
    from Akuga.GameClient.User import User
    from Akuga.GameClient.MenuStack import MenuStack

    running = True

    def quit_button_callback(button_template, mouse_button):
        global running
        if mouse_button == SDL_BUTTON_LEFT:
            running = False

    sdl2.SDL_Init(sdl2.SDL_INIT_VIDEO)
    window = sdl2.SDL_CreateWindow(
        b"Main Menu Demo",
        sdl2.SDL_WINDOWPOS_UNDEFINED,
        sdl2.SDL_WINDOWPOS_UNDEFINED, 1920, 1080,
        sdl2.SDL_WINDOW_OPENGL)
    video.SDL_GL_SetAttribute(video.SDL_GL_CONTEXT_MAJOR_VERSION, 4)
    video.SDL_GL_SetAttribute(video.SDL_GL_CONTEXT_MINOR_VERSION, 5)
    video.SDL_GL_SetAttribute(
        video.SDL_GL_CONTEXT_PROFILE_MASK,
        video.SDL_GL_CONTEXT_PROFILE_CORE)
    context = sdl2.SDL_GL_CreateContext(window)

    user = User()

    base_path = Path.cwd() / 'Akuga' / 'GameClient' / 'res'
    with open(base_path / 'ressources.list') as ressources_fd:
        Ressources.load(base_path, ressources_fd)

    menu_stack = MenuStack()
    main_menu = MainMenu(
        menu_stack, user, quit_button_callback)

    main_menu.generate_news([
        '{"type": "text", "pos": [300, 300], "text": "Anudem News Test", "scale": 1, "color": [1, 1, 1]}',
        '{"type": "image", "rect": [300, 400, 600, 300, 0], "img": "AKUGA_TITLE"}'
    ])

    menu_stack.push('Main_Menu')

    # Set view matrix
    view_matrix = glm.lookAt(
        glm.vec3(0, 0, 0),
        glm.vec3(0, 0, -1),
        glm.vec3(0, 1, 0))
    projection_matrix = glm.ortho(0, 1920, 1080, 0)

    # Upload the view and projection matrix for every shader
    Ressources.TEXT_RENDERER_SHADER.use()
    Ressources.TEXT_RENDERER_SHADER.upload_v_matrix(view_matrix)
    Ressources.TEXT_RENDERER_SHADER.upload_p_matrix(projection_matrix)

    Ressources.DEFAULT_SPRITE_SHADER.use()
    Ressources.DEFAULT_SPRITE_SHADER.upload_v_matrix(view_matrix)
    Ressources.DEFAULT_SPRITE_SHADER.upload_p_matrix(projection_matrix)

    glEnable(GL_BLEND)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

    event = sdl2.SDL_Event()
    while running:
        events = sdl2.ext.get_events()
        for event in events:
            if event.type == sdl2.SDL_QUIT:
                running = False
            convert_sdl_to_anudem_events(event)

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glClearColor(1, 0, 1, 0)

        # login_menu.draw()
        menu_stack.draw()

        glBindVertexArray(0)
        sdl2.SDL_GL_SwapWindow(window)
        sdl2.SDL_Delay(10)
    sdl2.SDL_GL_DeleteContext(context)
    sdl2.SDL_DestroyWindow(window)
    sdl2.SDL_Quit()
