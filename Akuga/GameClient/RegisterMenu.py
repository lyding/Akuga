'''
This module contains the implementation of the login menu
'''
from pathlib import Path
from sdl2 import SDL_BUTTON_LEFT
from Akuga.GameClient.MenuStack import Menu
from Akuga.Anudem2D.Drawables.TexturedSprite import TexturedSprite
from Akuga.Anudem2D.GUI.TextBox import TextBox
from Akuga.Anudem2D.GUI.Button import Button
from Akuga.GameClient.Protocoll import register
from Akuga.GameClient.Network import StreamSocketCommunicator
from Akuga.GameClient.GlobalDefinitions import SCREEN_RECT
import Akuga.GameClient.Ressources as Ressources


class RegisterMenu(Menu):
    '''
    The register menu
    '''

    def __init__(self, menu_stack, communicator):
        super().__init__('Register_Menu', menu_stack)
        self.communicator = communicator
        self.add_static_element('background', TexturedSprite(
                Ressources.DEFAULT_SPRITE_SHADER,
                SCREEN_RECT,
                Ressources.BACKGROUND1))

        self.add_static_element('title', TexturedSprite(
                Ressources.DEFAULT_SPRITE_SHADER,
                (242, 225, 1437, 311, 0),
                Ressources.AKUGA_TITLE))

        email_textbox_sprite = TexturedSprite(
                Ressources.DEFAULT_SPRITE_SHADER,
                (574, 604, 771, 58, 0),
                Ressources.TEXTBOX)

        username_textbox_sprite = TexturedSprite(
                Ressources.DEFAULT_SPRITE_SHADER,
                (574, 679, 771, 58, 0),
                Ressources.TEXTBOX)

        password_textbox_sprite = TexturedSprite(
                Ressources.DEFAULT_SPRITE_SHADER,
                (574, 754, 771, 58, 0),
                Ressources.TEXTBOX)

        self.add_gui_element('email_textbox', TextBox(
            0, email_textbox_sprite, Ressources.GET_TEXTRENDERER(),
            'email', None, 0.75, 10, 0, 32, (1, 1, 1)))
        self.add_gui_element('username_textbox', TextBox(
            1, username_textbox_sprite, Ressources.GET_TEXTRENDERER(),
            'username', None, 0.75, 10, 0, 32, (1, 1, 1)))
        self.add_gui_element('password_textbox', TextBox(
            2, password_textbox_sprite, Ressources.GET_TEXTRENDERER(),
            'password', '?', 0.75, 10, 0, 32, (1, 1, 1)))

        register_button_sprite = TexturedSprite(
                Ressources.DEFAULT_SPRITE_SHADER,
                (808, 829, 306, 113, 0),
                Ressources.REGISTER_BUTTON)

        self.add_gui_element('register_button', Button(
                3, register_button_sprite,
                self.register_button_callback))

        quit_button_sprite = TexturedSprite(
                Ressources.DEFAULT_SPRITE_SHADER,
                (1804, 63, 53, 53, 0),
                Ressources.QUIT_BUTTON)

        self.add_gui_element('quit_button', Button(
                3, quit_button_sprite,
                self.quit_button_callback))

    def register_button_callback(self, button_template, mouse_button):
        if mouse_button == SDL_BUTTON_LEFT:
            # response = login(
            #     self.communicator,
            #     self.username_textbox.value,
            #     hash(self.password_textbox.value))
            response = register(
                self.communicator,
                self.username_textbox.value,
                self.password_textbox.value,
                self.email_textbox.value)
            if response[0] == -1:
                print('Error while registering the user:')
                print(response[1])
            else:
                # The user is registeres succesfully so switch to the login
                # menu and hope that the user wont recognize that this
                # game is clumsy
                menu_stack.switch('Login_Menu')

    def quit_button_callback(self, button_template, mouse_button):
        menu_stack.switch('Login_Menu')


if __name__ == '__main__':
    import sdl2
    import sdl2.ext
    from sdl2 import video
    import socket
    import glm
    from OpenGL.GL import (
        glClear, GL_COLOR_BUFFER_BIT,
        GL_DEPTH_BUFFER_BIT, glBindVertexArray, GL_BLEND, glEnable,
        GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, glBlendFunc, glClearColor)
    from Akuga.Anudem2D.Util.EventInterface import\
        convert_sdl_to_anudem_events
    from Akuga.GameClient.User import User
    from Akuga.GameClient.MenuStack import MenuStack
    from Akuga.GameClient.GlobalDefinitions import (SERVER_ADDRESS, NBYTES)

    sdl2.SDL_Init(sdl2.SDL_INIT_VIDEO)
    window = sdl2.SDL_CreateWindow(
        b"Drawable Demo",
        sdl2.SDL_WINDOWPOS_UNDEFINED,
        sdl2.SDL_WINDOWPOS_UNDEFINED, 1920, 1080,
        sdl2.SDL_WINDOW_OPENGL)
    video.SDL_GL_SetAttribute(video.SDL_GL_CONTEXT_MAJOR_VERSION, 4)
    video.SDL_GL_SetAttribute(video.SDL_GL_CONTEXT_MINOR_VERSION, 5)
    video.SDL_GL_SetAttribute(
        video.SDL_GL_CONTEXT_PROFILE_MASK,
        video.SDL_GL_CONTEXT_PROFILE_CORE)
    context = sdl2.SDL_GL_CreateContext(window)

    socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    socket.connect(SERVER_ADDRESS)
    communicator = StreamSocketCommunicator(socket, NBYTES)

    user = User()

    base_path = Path.cwd() / 'Akuga' / 'GameClient' / 'res'
    with open(base_path / 'ressources.list') as ressources_fd:
        Ressources.load(base_path, ressources_fd)

    menu_stack = MenuStack()
    register_menu = RegisterMenu(menu_stack, communicator)

    menu_stack.push('Register_Menu')

    # Set view matrix
    view_matrix = glm.lookAt(
        glm.vec3(0, 0, 0),
        glm.vec3(0, 0, -1),
        glm.vec3(0, 1, 0))
    projection_matrix = glm.ortho(0, 1920, 1080, 0)

    # Upload the view and projection matrix for every shader
    Ressources.TEXT_RENDERER_SHADER.use()
    Ressources.TEXT_RENDERER_SHADER.upload_v_matrix(view_matrix)
    Ressources.TEXT_RENDERER_SHADER.upload_p_matrix(projection_matrix)

    Ressources.DEFAULT_SPRITE_SHADER.use()
    Ressources.DEFAULT_SPRITE_SHADER.upload_v_matrix(view_matrix)
    Ressources.DEFAULT_SPRITE_SHADER.upload_p_matrix(projection_matrix)

    running = True
    glEnable(GL_BLEND)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

    event = sdl2.SDL_Event()
    while running:
        events = sdl2.ext.get_events()
        for event in events:
            if event.type == sdl2.SDL_QUIT:
                running = False
            convert_sdl_to_anudem_events(event)

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glClearColor(1, 0, 1, 0)

        # login_menu.draw()
        menu_stack.draw()

        glBindVertexArray(0)
        sdl2.SDL_GL_SwapWindow(window)
        sdl2.SDL_Delay(10)
    sdl2.SDL_GL_DeleteContext(context)
    sdl2.SDL_DestroyWindow(window)
    sdl2.SDL_Quit()
    communicator.close()
