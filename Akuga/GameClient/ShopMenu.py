'''
This module contains the implementation of the shop menu
'''
from pathlib import Path
from sdl2 import SDL_BUTTON_LEFT
from Akuga.GameClient.MenuStack import Menu
from Akuga.Anudem2D.Drawables.TexturedSprite import TexturedSprite
from Akuga.Anudem2D.GUI.Button import Button
from Akuga.Anudem2D.GUI.ButtonLayout import ButtonLayout
from Akuga.GameClient.Protocoll import buy_jumon
from Akuga.GameClient.Network import StreamSocketCommunicator
from Akuga.GameClient.GlobalDefinitions import SCREEN_RECT
from Akuga.GameClient.Jumons import Jumon
import Akuga.GameClient.Ressources as Ressources


class ShopMenu(Menu):
    '''
    The shop menu
    '''
    def __init__(self, menu_stack, user, communicator, jumon_list, base_path):
        super().__init__('Shop_Menu', menu_stack)
        self.user = user
        self.communicator = communicator
        self.base_path = base_path
        self.jumon_list = jumon_list
        self.page = 0
        self.number_of_buttons = 0

        # The color filters
        self.dea_selected = True
        self.evo_selected = True
        self.mun_selected = True
        self.gal_selected = True

        # All the assets
        self.add_static_element('background_sprite', TexturedSprite(
                Ressources.DEFAULT_SPRITE_SHADER,
                SCREEN_RECT,
                Ressources.BACKGROUND2))

        self.add_static_element('deko_sprite', TexturedSprite(
                Ressources.DEFAULT_SPRITE_SHADER,
                (0, 256, 225, 694, 0),
                Ressources.DEKO))

        self.add_static_element('selection_bg_sprite', TexturedSprite(
                Ressources.DEFAULT_SPRITE_SHADER,
                (1703, 275, 137, 328, 0),
                Ressources.SELECTION_BG))

        self.add_static_element('book_sprite', TexturedSprite(
                Ressources.DEFAULT_SPRITE_SHADER,
                (238, 184, 1458, 873, 0),
                Ressources.BOOK))

        self.dea_sprite = TexturedSprite(
                Ressources.DEFAULT_SPRITE_SHADER,
                (1729, 284, 80, 80, 0),
                Ressources.DEA)

        self.dea_glow_sprite = TexturedSprite(
                Ressources.DEFAULT_SPRITE_SHADER,
                (1729, 284, 80, 80, 0),
                Ressources.DEA_GLOW)

        self.evo_sprite = TexturedSprite(
                Ressources.DEFAULT_SPRITE_SHADER,
                (1729, 356, 80, 80, 0),
                Ressources.EVO)

        self.evo_glow_sprite = TexturedSprite(
                Ressources.DEFAULT_SPRITE_SHADER,
                (1729, 356, 80, 80, 0),
                Ressources.EVO_GLOW)

        self.mun_sprite = TexturedSprite(
                Ressources.DEFAULT_SPRITE_SHADER,
                (1729, 428, 80, 80, 0),
                Ressources.MUN)

        self.mun_glow_sprite = TexturedSprite(
                Ressources.DEFAULT_SPRITE_SHADER,
                (1729, 428, 80, 80, 0),
                Ressources.MUN_GLOW)

        self.gal_sprite = TexturedSprite(
                Ressources.DEFAULT_SPRITE_SHADER,
                (1729, 497, 80, 80, 0),
                Ressources.GAL)

        self.gal_glow_sprite = TexturedSprite(
                Ressources.DEFAULT_SPRITE_SHADER,
                (1729, 497, 80, 80, 0),
                Ressources.GAL_GLOW)

        # Use the glowing symbols as the sprites to create the
        # selection buttons as this will be the default.
        # when a button is clicked its selection flag will be toggled
        # and the sprite of the button will be changed
        self.add_gui_element('dea_button', Button(
                8, self.dea_glow_sprite, self.dea_button_callback))
        self.add_gui_element('evo_button', Button(
                9, self.evo_glow_sprite, self.evo_button_callback))
        self.add_gui_element('mun_button', Button(
                10, self.mun_glow_sprite, self.mun_button_callback))
        self.add_gui_element('gal_button', Button(
                11, self.gal_glow_sprite, self.gal_button_callback))

        # The menu switching button array
        self.add_gui_element('head_buttons', ButtonLayout(
                12,
                [
                    (11, 232, 0, 292, 119, self.start_button_callback),
                    (12, 524, 0, 292, 119, self.sets_button_callback),
                    (13, 1108, 0, 292, 119, self.options_button_callback),
                    (14, 1400, 0, 292, 119, quit_button_callback),
                    (15, 507, 218, 145, 50, self.left_arrow_button_callback),
                    (16, 1269, 218, 145, 50, self.right_arrow_button_callback)
                ]))

    def init(self):
        '''
        Init all components
        '''
        super().init()

        # Prefilter all existing jumons and also sort them alphabetically
        self.dea_jumons = list(filter(
            lambda x: x.color == 'red',
            self.jumon_list))
        self.dea_jumons.sort(key=lambda x: x.name)
        self.evo_jumons = list(filter(
            lambda x: x.color == 'blue',
            self.jumon_list))
        self.evo_jumons.sort(key=lambda x: x.name)
        self.mun_jumons = list(filter(
            lambda x: x.color == 'green',
            self.jumon_list))
        self.mun_jumons.sort(key=lambda x: x.name)
        self.gal_jumons = list(filter(
            lambda x: x.color == 'black',
            self.jumon_list))
        self.gal_jumons.sort(key=lambda x: x.name)

        # Now concatenate all the jumon lists into one larger
        # sorted list of jumns. The order of the jumons in this list
        # is the order in which the jumons appear in the menu
        # The default is that all filters are off
        # so no need to check it here
        self.sorted_and_filtered_jumon_list = self.dea_jumons +\
            self.evo_jumons + self.mun_jumons + self.gal_jumons

        # The IDs will be used to calculate the index
        # of the jumon they are refering to
        self.jumon_buttons = []
        self.jumon_button_sprites = []
        button_positions = [
            (282, 287), (589, 287), (282, 650), (589, 650),
            (1046, 287), (1352, 287), (1046, 650), (1352, 650)]

        self.number_of_buttons = 8
        for i in range(0, self.number_of_buttons):
            # Load the jumon texture
            jumon = self.get_jumon_from_index(0, i)
            self.load_jumon_ressources(jumon)
            # Use the loaded ressources to generate the sprite
            # and the button, everything will be initialised
            # in the init method
            self.jumon_button_sprites.append(TexturedSprite(
                Ressources.DEFAULT_SPRITE_SHADER,
                (*(button_positions[i]), 286, 344, 0),
                Ressources.__dict__[jumon.name]))
            self.jumon_buttons.append(Button(
                i, self.jumon_button_sprites[i],
                self.jumon_button_callback))

        for button in self.jumon_buttons:
            button.full_init()

        self.dea_sprite.full_init()
        self.evo_sprite.full_init()
        self.mun_sprite.full_init()
        self.gal_sprite.full_init()

    def draw(self):
        super().draw()
        for i in range(0, self.number_of_buttons):
            self.jumon_buttons[i].draw()

    def update_jumon_button_layout(self):
        '''
        Updates the jumon button layout,
        has to be invoked whenever the jumon button layout should change
        '''
        self.number_of_buttons = min(
            8, len(self.sorted_and_filtered_jumon_list) - self.page * 8)
        # Load the jumon ressources and set the texutes
        for i in range(self.number_of_buttons):
            jumon = self.get_jumon_from_index(self.page, i)
            self.load_jumon_ressources(jumon)
            self.jumon_button_sprites[i].set_texture(
                Ressources.__dict__[jumon.name])
            self.jumon_buttons[i].observer.activate()
        # Deactivate the unneeded buttons
        for i in range(self.number_of_buttons, 8):
            self.jumon_buttons[i].observer.deactivate()

    def load_jumon_ressources(self, jumon):
        '''
        Load the texture of a jumon
        '''
        if jumon.name not in Ressources.__dict__:
            Ressources.load_from_string(
                self.base_path,
                f'{{"name": "{jumon.name}", "type": "png", '
                f'"path": "assets/jumons/{jumon.name}.png", '
                f'"width": 286, "height": 343}}')

    def change_jumon_button_sprites(self):
        '''
        Change the sprites of the jumon buttons,
        for instance if a filter or the page changes
        The textures have to be loaded and initialised
        '''
        for i in range(0, min(8, len(self.sorted_and_filtered_jumon_list))):
            # Load the jumon texture
            jumon = self.get_jumon_from_index(0, i)
            self.jumon_button_sprites[i].set_texture(
                Ressources.__dict__[jumon.name])

    def get_jumon_from_index(self, page_index, pos_index):
        '''
        Get the jumon (from the sorted_jumon_list) by the page number
        and the position of the 'book'
        '''
        # page_index * 8 as there are 8 jumons displayed on each 'page'
        index = page_index * 8 + pos_index
        return self.sorted_and_filtered_jumon_list[index]

    def recreate_sorted_and_filtered_jumon_list(self):
        '''
        (Re)creates the sorted_and_filtered_jumon_list
        according to the active filters
        '''
        self.sorted_and_filtered_jumon_list = []
        if self.dea_selected:
            self.sorted_and_filtered_jumon_list += self.dea_jumons
        if self.evo_selected:
            self.sorted_and_filtered_jumon_list += self.evo_jumons
        if self.mun_selected:
            self.sorted_and_filtered_jumon_list += self.mun_jumons
        if self.gal_selected:
            self.sorted_and_filtered_jumon_list += self.gal_jumons

    def jumon_button_callback(self, button_template, mouse_button):
        print(f'Button {button_template} was clicked')

    def dea_button_callback(self, button_template, mouse_button):
        '''
        Toggle the dea_selected variable and change the sprite of the button
        '''
        if mouse_button != SDL_BUTTON_LEFT:
            return
        self.dea_selected = not self.dea_selected
        if self.dea_selected:
            self.dea_button.sprite = self.dea_glow_sprite
        else:
            self.dea_button.sprite = self.dea_sprite
        self.recreate_sorted_and_filtered_jumon_list()
        self.page = min(
            self.page,
            len(self.sorted_and_filtered_jumon_list) // 8)
        self.update_jumon_button_layout()

    def evo_button_callback(self, button_template, mouse_button):
        '''
        Toggle the evo_selected variable and change the sprite of the button
        '''
        if mouse_button != SDL_BUTTON_LEFT:
            return
        self.evo_selected = not self.evo_selected
        if self.evo_selected:
            self.evo_button.sprite = self.evo_glow_sprite
        else:
            self.evo_button.sprite = self.evo_sprite
        self.recreate_sorted_and_filtered_jumon_list()
        self.page = min(
            self.page,
            len(self.sorted_and_filtered_jumon_list) // 8)
        self.update_jumon_button_layout()

    def mun_button_callback(self, button_template, mouse_button):
        '''
        Toggle the mun_selected variable and change the sprite of the button
        '''
        if mouse_button != SDL_BUTTON_LEFT:
            return
        self.mun_selected = not self.mun_selected
        if self.mun_selected:
            self.mun_button.sprite = self.mun_glow_sprite
        else:
            self.mun_button.sprite = self.mun_sprite
        self.recreate_sorted_and_filtered_jumon_list()
        self.page = min(
            self.page,
            len(self.sorted_and_filtered_jumon_list) // 8)
        self.update_jumon_button_layout()

    def gal_button_callback(self, button_template, mouse_button):
        '''
        Toggle the gal_selected variable and change the sprite of the button
        '''
        if mouse_button != SDL_BUTTON_LEFT:
            return
        self.gal_selected = not self.gal_selected
        if self.gal_selected:
            self.gal_button.sprite = self.gal_glow_sprite
        else:
            self.gal_button.sprite = self.gal_sprite
        self.recreate_sorted_and_filtered_jumon_list()
        self.page = min(
            self.page,
            len(self.sorted_and_filtered_jumon_list) // 8)
        self.update_jumon_button_layout()

    def start_button_callback(self, button_template, mouse_mode):
        self.menu_stack.switch('Start_Menu')

    def sets_button_callback(self, button_template, mouse_mode):
        self.menu_stack.switch('Sets_Menu')

    def shop_button_callback(self, button_template, mouse_mode):
        self.menu_stack.switch('Shop_Menu')

    def options_button_callback(self, button_template, mouse_mode):
        self.menu_stack.switch('Options_Menu')

    def left_arrow_button_callback(self, button_template, mouse_mode):
        if self.page > 0:
            self.page -= 1
        self.update_jumon_button_layout()

    def right_arrow_button_callback(self, button_template, mouse_mode):
        if self.page < len(self.sorted_and_filtered_jumon_list) // 8:
            self.page += 1
        self.update_jumon_button_layout()


if __name__ == '__main__':
    import sdl2
    import sdl2.ext
    from sdl2 import video
    import glm
    import socket
    from OpenGL.GL import (
        glClear, GL_COLOR_BUFFER_BIT,
        GL_DEPTH_BUFFER_BIT, glBindVertexArray, GL_BLEND, glEnable,
        GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, glBlendFunc, glClearColor)
    from Akuga.Anudem2D.Util.EventInterface import\
        convert_sdl_to_anudem_events
    from Akuga.GameClient.User import User
    from Akuga.GameClient.MenuStack import MenuStack
    from Akuga.GameClient.LoginMenu import LoginMenu
    from Akuga.GameClient.MainMenu import MainMenu
    from Akuga.GameClient.GlobalDefinitions import (SERVER_ADDRESS, NBYTES)

    running = True

    def quit_button_callback(button_template, mouse_button):
        global running
        if mouse_button == SDL_BUTTON_LEFT:
            running = False

    sdl2.SDL_Init(sdl2.SDL_INIT_VIDEO)
    window = sdl2.SDL_CreateWindow(
        b"Main Menu Demo",
        sdl2.SDL_WINDOWPOS_UNDEFINED,
        sdl2.SDL_WINDOWPOS_UNDEFINED, 1920, 1080,
        sdl2.SDL_WINDOW_OPENGL)
    video.SDL_GL_SetAttribute(video.SDL_GL_CONTEXT_MAJOR_VERSION, 4)
    video.SDL_GL_SetAttribute(video.SDL_GL_CONTEXT_MINOR_VERSION, 5)
    video.SDL_GL_SetAttribute(
        video.SDL_GL_CONTEXT_PROFILE_MASK,
        video.SDL_GL_CONTEXT_PROFILE_CORE)
    context = sdl2.SDL_GL_CreateContext(window)

    socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    socket.connect(SERVER_ADDRESS)
    communicator = StreamSocketCommunicator(socket, NBYTES)

    user = User()

    base_path = Path.cwd() / 'Akuga' / 'GameClient' / 'res'
    with open(base_path / 'ressources.list') as ressources_fd:
        Ressources.load(base_path, ressources_fd)

    jumon_list = []
    menu_stack = MenuStack()
    login_menu = LoginMenu(menu_stack, user,
                           communicator, quit_button_callback, jumon_list)
    main_menu = MainMenu(menu_stack, user, quit_button_callback)
    shop_menu = ShopMenu(menu_stack, user, None, jumon_list, base_path)
    '''
    login_menu.deactivate()
    main_menu.deactivate()
    shop_menu.deactivate()
    '''
    menu_stack.push('Login_Menu')

    # Set view matrix
    view_matrix = glm.lookAt(
        glm.vec3(0, 0, 0),
        glm.vec3(0, 0, -1),
        glm.vec3(0, 1, 0))
    projection_matrix = glm.ortho(0, 1920, 1080, 0)

    # Upload the view and projection matrix for every shader
    Ressources.TEXT_RENDERER_SHADER.use()
    Ressources.TEXT_RENDERER_SHADER.upload_v_matrix(view_matrix)
    Ressources.TEXT_RENDERER_SHADER.upload_p_matrix(projection_matrix)

    Ressources.DEFAULT_SPRITE_SHADER.use()
    Ressources.DEFAULT_SPRITE_SHADER.upload_v_matrix(view_matrix)
    Ressources.DEFAULT_SPRITE_SHADER.upload_p_matrix(projection_matrix)

    glEnable(GL_BLEND)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

    event = sdl2.SDL_Event()
    while running:
        events = sdl2.ext.get_events()
        for event in events:
            if event.type == sdl2.SDL_QUIT:
                running = False
            convert_sdl_to_anudem_events(event)

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glClearColor(1, 0, 1, 0)

        # login_menu.draw()
        menu_stack.draw()

        glBindVertexArray(0)
        sdl2.SDL_GL_SwapWindow(window)
        sdl2.SDL_Delay(10)
    sdl2.SDL_GL_DeleteContext(context)
    sdl2.SDL_DestroyWindow(window)
    sdl2.SDL_Quit()
