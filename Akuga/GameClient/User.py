'''
This module contains the user class which represents a user
after a succesfull login
'''


class User:
    '''
    The userclass is nothing more than a nice way to store all
    informations about the user in a nice adressable way
    '''
    def __init__(self):
        self.name = None
        self.credits = None
        self.collection = None
        self.set0 = None
        self.set1 = None
        self.set2 = None
