'''
This module contains the definition of a jumon as well as a function
to init a dictionary of all jumons from a list of jumon data tuples
'''


class Jumon:
    def __init__(self, name, color, attack, defense,
                 movement, special, price, basic, flavor):
        self.name = name
        self.color = color
        self.attack = attack
        self.defense = defense
        self.movement = movement
        self.special = special
        self.price = price
        self.basic = basic
        self.flavor = flavor


def parse_jumons(jumons_data, jumon_list):
    '''
    Create a jumons list from a list of jumon data tuples
    Each tuple should have the form
    (name, color, attack, defense, movement, special, price, basic, flavor)

    All changes are made inplace.
    Thats important as the jumon_list will be a semi global variable
    shared with other jumons
    '''
    jumon_list.clear()
    for jumon_tuple in jumons_data:
        jumon_list.append(Jumon(*jumon_tuple))
