'''
This module contains the implementation of the login menu
'''
from pathlib import Path
from sdl2 import SDL_BUTTON_LEFT
from Akuga.GameClient.MenuStack import Menu
from Akuga.Anudem2D.Drawables.TexturedSprite import TexturedSprite
from Akuga.Anudem2D.GUI.Button import Button
from Akuga.Anudem2D.GUI.TextBox import TextBox
from Akuga.Anudem2D.GUI.ButtonLayout import ButtonLayout
from Akuga.GameClient.Protocoll import (login, query_user, get_jumons)
from Akuga.GameClient.Network import StreamSocketCommunicator
from Akuga.GameClient.GlobalDefinitions import SCREEN_RECT
import Akuga.GameClient.Ressources as Ressources
import Akuga.GameClient.Jumons as Jumons


class LoginMenu(Menu):
    '''
    The login menu
    '''

    def __init__(self, menu_stack, user, communicator,
                 quit_button_callback, jumon_dict):
        super().__init__('Login_Menu', menu_stack)
        self.user = user
        self.communicator = communicator
        self.jumon_dict = jumon_dict
        self.add_static_element('background_sprite', TexturedSprite(
                Ressources.DEFAULT_SPRITE_SHADER,
                SCREEN_RECT,
                Ressources.BACKGROUND1))
        self.add_static_element('title', TexturedSprite(
                Ressources.DEFAULT_SPRITE_SHADER,
                (242, 225, 1437, 311, 0),
                Ressources.AKUGA_TITLE))
        self.add_static_element('subtitle', TexturedSprite(
                Ressources.DEFAULT_SPRITE_SHADER,
                (469, 529, 982, 108, 0),
                Ressources.AKUGA_SUBTITLE))

        username_textbox_sprite = TexturedSprite(
                Ressources.DEFAULT_SPRITE_SHADER,
                (574, 679, 771, 58, 0),
                Ressources.TEXTBOX)

        password_textbox_sprite = TexturedSprite(
                Ressources.DEFAULT_SPRITE_SHADER,
                (574, 754, 771, 58, 0),
                Ressources.TEXTBOX)

        self.add_gui_element('username_textbox', TextBox(
            0, username_textbox_sprite, Ressources.GET_TEXTRENDERER(),
            'username', None, 0.75, 10, 0, 32, (1, 1, 1)))
        self.add_gui_element('password_textbox', TextBox(
            1, password_textbox_sprite, Ressources.GET_TEXTRENDERER(),
            'password', '?', 0.75, 10, 0, 32, (1, 1, 1)))

        login_button_sprite = TexturedSprite(
                Ressources.DEFAULT_SPRITE_SHADER,
                (808, 829, 306, 113, 0),
                Ressources.LOGIN_BUTTON)

        self.add_gui_element('login_button', Button(
                2, login_button_sprite,
                self.login_button_callback))

        quit_button_sprite = TexturedSprite(
                Ressources.DEFAULT_SPRITE_SHADER,
                (1804, 63, 53, 53, 0),
                Ressources.QUIT_BUTTON)

        self.add_gui_element('quit_button', Button(
                3, quit_button_sprite,
                quit_button_callback))

        self.add_static_element(
            'i_forgot_it_text', Ressources.GET_TEXTRENDERER())
        self.i_forgot_it_text.set_text(1346, 760, 'I forgot it', .5, (1, 1, 1))
        self.i_forgot_it_text.generate()

        self.add_static_element(
            'register_text', Ressources.GET_TEXTRENDERER())
        self.register_text.set_text(892, 953, 'Register', .5, (1, 1, 1))
        self.register_text.generate()

        self.button_layout = ButtonLayout(
            4,
            [
                (40, 879, 959, 163, 33, self.register_button_callback),
                (41, 1356, 767, 151, 24, self.pwd_reset_button_callback)
            ])

    def login_button_callback(self, button_template, mouse_button):
        if mouse_button != SDL_BUTTON_LEFT:
            return
        # TODO: Use the hash everywhere
        # response = login(
        #     self.communicator,
        #     self.username_textbox.value,
        #     hash(self.password_textbox.value))

        response = login(
            self.communicator,
            self.username_textbox.value,
            self.password_textbox.value)

        if response[0] == -1:
            print('Error while logging in:')
            print(response[1])
            return

        response = query_user(self.communicator, self.user)
        if response[0] == -1:
            print('Error while querying user:')
            print(response[1])
            return

        response = get_jumons(self.communicator)
        if response[0] == -1:
            print('Error while querying the jumons')
            print(response[1])
            return
        # Create a dictionary of ALL existing jumons in the Akuga
        # Game. It will be neede mostly in the Shop and Set Menu
        Jumons.parse_jumons(response[1][0], self.jumon_dict)
        # Change the menu
        print(self.user.credits)
        print(self.user.collection)
        self.menu_stack.switch('Main_Menu')

    def register_button_callback(self, button_template, mouse_button):
        self.menu_stack.switch('Register_Menu')

    def quit_button_callback(self, button_template, mouse_button):
        pass

    def pwd_reset_button_callback(self, button_template, mouse_button):
        self.menu_stack.switch('Reset_Password_Menu')


if __name__ == '__main__':
    import sdl2
    import sdl2.ext
    from sdl2 import video
    import socket
    import glm
    from OpenGL.GL import (
        glClear, GL_COLOR_BUFFER_BIT,
        GL_DEPTH_BUFFER_BIT, glBindVertexArray, GL_BLEND, glEnable,
        GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, glBlendFunc, glClearColor)
    from Akuga.Anudem2D.Util.EventInterface import\
        convert_sdl_to_anudem_events
    from Akuga.GameClient.User import User
    from Akuga.GameClient.MenuStack import MenuStack
    from Akuga.GameClient.GlobalDefinitions import (SERVER_ADDRESS, NBYTES)

    running = True

    def quit_button_callback(button_template, mouse_button):
        global running
        if mouse_button == SDL_BUTTON_LEFT:
            running = False

    sdl2.SDL_Init(sdl2.SDL_INIT_VIDEO)
    window = sdl2.SDL_CreateWindow(
        b"Drawable Demo",
        sdl2.SDL_WINDOWPOS_UNDEFINED,
        sdl2.SDL_WINDOWPOS_UNDEFINED, 1920, 1080,
        sdl2.SDL_WINDOW_OPENGL)
    video.SDL_GL_SetAttribute(video.SDL_GL_CONTEXT_MAJOR_VERSION, 4)
    video.SDL_GL_SetAttribute(video.SDL_GL_CONTEXT_MINOR_VERSION, 5)
    video.SDL_GL_SetAttribute(
        video.SDL_GL_CONTEXT_PROFILE_MASK,
        video.SDL_GL_CONTEXT_PROFILE_CORE)
    context = sdl2.SDL_GL_CreateContext(window)

    socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    socket.connect(SERVER_ADDRESS)
    communicator = StreamSocketCommunicator(socket, NBYTES)

    user = User()

    base_path = Path.cwd() / 'Akuga' / 'GameClient' / 'res'
    with open(base_path / 'ressources.list') as ressources_fd:
        Ressources.load(base_path, ressources_fd)

    menu_stack = MenuStack()
    jumon_dict = {}
    login_menu = LoginMenu(
        menu_stack, user, communicator, quit_button_callback, jumon_dict)

    menu_stack.push('Login_Menu')

    # Set view matrix
    view_matrix = glm.lookAt(
        glm.vec3(0, 0, 0),
        glm.vec3(0, 0, -1),
        glm.vec3(0, 1, 0))
    projection_matrix = glm.ortho(0, 1920, 1080, 0)

    # Upload the view and projection matrix for every shader
    Ressources.TEXT_RENDERER_SHADER.use()
    Ressources.TEXT_RENDERER_SHADER.upload_v_matrix(view_matrix)
    Ressources.TEXT_RENDERER_SHADER.upload_p_matrix(projection_matrix)

    Ressources.DEFAULT_SPRITE_SHADER.use()
    Ressources.DEFAULT_SPRITE_SHADER.upload_v_matrix(view_matrix)
    Ressources.DEFAULT_SPRITE_SHADER.upload_p_matrix(projection_matrix)

    glEnable(GL_BLEND)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

    event = sdl2.SDL_Event()
    while running:
        events = sdl2.ext.get_events()
        for event in events:
            if event.type == sdl2.SDL_QUIT:
                running = False
            convert_sdl_to_anudem_events(event)

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glClearColor(1, 0, 1, 0)

        # login_menu.draw()
        menu_stack.draw()

        glBindVertexArray(0)
        sdl2.SDL_GL_SwapWindow(window)
        sdl2.SDL_Delay(10)
    sdl2.SDL_GL_DeleteContext(context)
    sdl2.SDL_DestroyWindow(window)
    sdl2.SDL_Quit()
    communicator.close()
