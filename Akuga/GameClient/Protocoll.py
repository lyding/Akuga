'''
This module contains a function for every procedure which can be called
on the server side
'''

# Game Server Commands


def register(communicator, username, pass_hash, email_hash):
    '''
    Register a new user for the Akuga Game
    '''
    communicator.send_packet([
        'REGISTER_USER', username, pass_hash, email_hash])
    response = communicator.recv_packet()
    if response[0] == 'ERROR':
        return (-1, response[1:])
    if response[0] == 'SUCCESS':
        return (0, '')


def login(communicator, username, pass_hash):
    '''
    Login to the akuga game server
    '''
    communicator.send_packet(['LOG_IN', username, pass_hash])
    response = communicator.recv_packet()
    if response[0] == 'ERROR':
        return (-1, response[1:])
    if response[0] == 'SUCCESS':
        return (0, '')


def send_pwd_reset_token(communicator, username, email):
    '''
    Queryies a password reset token to the specified email iff
    the email matches the email which was used to register this user
    '''
    communicator.send_packet(['SEND_PWD_RESET_TOKEN', username, email])
    response = communicator.recv_packet()
    if response[0] == 'ERROR':
        return (-1, response[1:])
    if response[0] == 'SUCCESS':
        return (0, '')


def reset_password(communicator, username, pass_hash, reset_token):
    '''
    Resets the password to pass_hash if the reset token is valid
    '''
    communicator.send_packet(['RESET_PASSWORD', username,
                              pass_hash, reset_token])
    response = communicator.recv_packet()
    if response[0] == 'ERROR':
        return (-1, response[1:])
    if response[0] == 'SUCCESS':
        return (0, '')


def enqueue_for_match(communicator, game_mode):
    '''
    Enqueue for a match in the specified game mode,
    only valid after a succesfull login
    '''
    communicator.send_packet(['ENQUEUE_FOR_MATCH', game_mode])
    response = communicator.recv_packet()
    if response[0] == 'ERROR':
        return (-1, response[1:])
    if response[0] == 'SUCCESS':
        return (0, '')


def get_jumons(communicator):
    '''
    Query the database for all jumons,
    only valid after a succesfull login
    '''
    communicator.send_packet(['GET_JUMONS'])
    response = communicator.recv_packet()
    if response[0] == 'ERROR':
        return (-1, response[1:])
    if response[0] == 'SUCCESS':
        return (0, response[1:])


def get_jumon_collection(communicator):
    '''
    Query the database for the jumon collection
    only valid after a succesfull login
    '''
    communicator.send_packet(['GET_JUMON_COLLECTION'])
    response = communicator.recv_packet()
    if response[0] == 'ERROR':
        return (-1, response[1:])
    if response[0] == 'SUCCESS':
        return (0, response[1])


def get_jumon_set(communicator, set_number):
    '''
    Query the server for the specified set of the user,
    only valid after a succesfull login
    '''
    communicator.send_packet(['GET_JUMON_SET', set_number])
    response = communicator.recv_packet()
    if response[0] == 'ERROR':
        return (-1, response[1:])
    if response[0] == 'SUCCESS':
        return (0, response[1])


def set_jumon_set(communicator, set_number, jumon_set):
    '''
    Update a specified jumon set, a jumon set is a dictionary
    of the form jumon_name: amount.
    Only valid after a succesfull login
    '''
    communicator.send_packet(['SET_JUMON_SET', set_number, jumon_set])
    response = communicator.recv_packet()
    if response[0] == 'ERROR':
        return (-1, response[1:])
    if response[0] == 'SUCCESS':
        return (0, response[1])


def buy_jumon(communicator, jumon_name):
    '''
    Buy a jumon,
    only valid after a succesfull login
    '''
    communicator.send_packet(['BUY_JUMON', jumon_name])
    response = communicator.recv_packet()
    if response[0] == 'ERROR':
        return (-1, response[1:])
    if response[0] == 'SUCCESS':
        return (0, response[1])


def get_credits(communicator):
    '''
    Get the credits a user has
    only valid after a succesfull login
    '''
    communicator.send_packet(['GET_CREDITS'])
    response = communicator.recv_packet()
    # No error is possible with this command
    return (0, int(response[1]))


def get_news(communicator):
    '''
    Get the news from anudem text and split it into
    a list of lines for easier processing
    '''
    communicator.send_packet(['GET_NEWS'])
    response = communicator.recv_packet()
    # No error is possible with this command
    return (0, response[1].split('\r'))


# Match related commands

def pick_jumon(communicator, jumon_id):
    '''
    Pick the specified jumon
    only valid during a match
    '''
    communicator.send_packet(['PICK_JUMON', jumon_id])


def summon_jumon(communicator, jumon_id, x_pos, y_pos):
    '''
    Summon the specified jumon,
    only valid during a match
    '''
    communicator.send_packet(['SUMMON_JUMON', jumon_id, x_pos, y_pos])


def move_jumon(communicaor, jumon_id, x_pos, y_pos):
    '''
    Move the specified jumon,
    only valid during a match
    '''
    communicaor.send_packet(['MOVE_JUMON', jumon_id, x_pos, y_pos])


def special_move_jumon(communicator, jumon_id, targets):
    '''
    Special move specified jumon
    only valid duing a match
    '''
    communicator.send_packet(['SPECIAL_MOVE_JUMON', jumon_id] + targets)


# Non atomic command

def query_user(communicator, user):
    '''
    Query alls user characteristics
    '''
    response = get_credits(communicator)
    if response[0] == -1:
        return response
    credits = response[1]

    response = get_jumon_collection(communicator)
    if response[0] == -1:
        return response
    collection = response[1]
    response = get_jumon_set(communicator, 0)
    if response[0] == -1:
        return response
    set0 = response[1]

    response = get_jumon_set(communicator, 1)
    if response[0] == -1:
        return response
    set1 = response[1]

    response = get_jumon_set(communicator, 2)
    if response[0] == -1:
        return response
    set2 = response[1]

    # Update the values at the very end to avoid partial updates
    user.credits = credits
    user.collection = collection
    user.set0 = set0
    user.set1 = set1
    user.set2 = set2
    return (0, '')
