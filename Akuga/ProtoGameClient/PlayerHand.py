'''
This module contains a represnetation of the players hand
aka all jumons a player picked
'''

import Akuga.ProtoGameClient.Assets as Assets
import Akuga.ProtoGameClient.MeepleDict as MeepleDict
import Akuga.ProtoGameClient.CommandFSM as CommandFSM
from Akuga.Anudem2D.Drawables.TexturedSprite import TexturedSprite
from Akuga.Anudem2D.GUI.Button import Button
from Akuga.EventDefinitions import Event
from Akuga.Anudem2D.Util.ObserverPattern import Observer
from sdl2 import SDL_BUTTON_LEFT


def handle_click(button, mode):
    '''
    Send a jumon_to_summon_tile command to the commands fsm which will use
    it to initiiate the correct commands to send to the server
    '''
    if mode == SDL_BUTTON_LEFT:
        CommandFSM.get_queue().put(Event(
            'jumon_to_summon_tile',
            jumon_id=button.id))


class PlayerHand():
    def __init__(self, pos, button_dimensions, jumons=[]):
        self.pos = pos
        self.button_dimensions = button_dimensions
        self.jumons = jumons
        self.jumon_buttons = []
        self.active = True

    def activate(self):
        for button in self.jumon_buttons:
            button.observer.activate()
        self.active = True

    def deactivate(self):
        for button in self.jumon_buttons:
            button.observer.deactivate()
        self.active = False

    def generate(self):
        '''
        Generate the button representation
        '''
        # Clear the old representation entirely
        for button in self.jumon_buttons:
            button.observer.free()
        self.jumon_buttons.clear()
        index = 0
        offset = (
            self.pos[0],
            self.pos[1] - len(self.jumons) * self.button_dimensions[0] // 2)
        for jumon_id in self.jumons:
            rect = (
                offset[0],
                offset[1] + index * self.button_dimensions[1],
                self.button_dimensions[0],
                self.button_dimensions[1],
                0)
            texture = Assets.get_texture(MeepleDict.get_jumon(jumon_id)[0])
            sprite = TexturedSprite(Assets.get_shader(), rect, texture)
            button = Button(jumon_id, sprite, handle_click)
            button.init()
            self.jumon_buttons.append(button)
            index += 1
        print(len(Observer.observers))

    def draw(self):
        '''
        Draw all buttons
        '''
        for button in self.jumon_buttons:
            button.draw()


if __name__ == '__main__':
    import sdl2
    import sdl2.ext
    from sdl2 import video
    import glm
    from OpenGL.GL import (
        glClear, GL_COLOR_BUFFER_BIT,
        GL_DEPTH_BUFFER_BIT, glBindVertexArray)
    from Akuga.Anudem2D.Util.EventInterface import\
        convert_sdl_to_anudem_events

    sdl2.SDL_Init(sdl2.SDL_INIT_VIDEO)
    window = sdl2.SDL_CreateWindow(
        b"Button Demo",
        sdl2.SDL_WINDOWPOS_UNDEFINED,
        sdl2.SDL_WINDOWPOS_UNDEFINED, 1220, 686,
        sdl2.SDL_WINDOW_OPENGL)
    video.SDL_GL_SetAttribute(video.SDL_GL_CONTEXT_MAJOR_VERSION, 4)
    video.SDL_GL_SetAttribute(video.SDL_GL_CONTEXT_MINOR_VERSION, 5)
    video.SDL_GL_SetAttribute(
        video.SDL_GL_CONTEXT_PROFILE_MASK,
        video.SDL_GL_CONTEXT_PROFILE_CORE)
    context = sdl2.SDL_GL_CreateContext(window)

    # Set view matrix
    view_matrix = glm.lookAt(
        glm.vec3(0, 0, 0),
        glm.vec3(0, 0, -1),
        glm.vec3(0, 1, 0))
    # projection_matrix = glm.perspective(35.0, 1.0, 0.1, 100.0)
    projection_matrix = glm.ortho(0, 1220, 686, 0)

    Assets.init_shader('default.vs', 'default.fs')
    Assets.get_shader().use()
    Assets.get_shader().upload_v_matrix(view_matrix)
    Assets.get_shader().upload_p_matrix(projection_matrix)

    Assets.add_texture('Blauta')
    Assets.add_texture('Plodher')

    MeepleDict.add_jumon(0, ('Blauta',))
    MeepleDict.add_jumon(1, ('Plodher',))

    playerhand = PlayerHand((300, 300), (50, 50), [0, 1])
    # Even if the generate methode is invoked twice the buttons
    # wont trigger twice (not like in the buggy code I set on fire)
    # as the observers seems to be removed succesfully
    playerhand.generate()
    playerhand.generate()

    running = True
    while running:
        events = sdl2.ext.get_events()
        for event in events:
            if event.type == sdl2.SDL_QUIT:
                running = False
            convert_sdl_to_anudem_events(event)

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        playerhand.draw()

        glBindVertexArray(0)
        sdl2.SDL_GL_SwapWindow(window)
        sdl2.SDL_Delay(10)

    sdl2.SDL_GL_DeleteContext(context)
    sdl2.SDL_DestroyWindow(window)
    sdl2.SDL_Quit()
