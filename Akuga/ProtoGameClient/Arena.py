'''
This module contains a representation of the arena
'''
import Akuga.ProtoGameClient.Assets as Assets
import Akuga.ProtoGameClient.MeepleDict as MeepleDict
import Akuga.ProtoGameClient.CommandFSM as CommandFSM
from Akuga.Anudem2D.Drawables.TexturedSprite import TexturedSprite
from Akuga.Anudem2D.GUI.Button import Button
from Akuga.Anudem2D.Util.ObserverPattern import Observer
from Akuga.EventDefinitions import Event
from sdl2 import SDL_BUTTON_LEFT, SDL_BUTTON_RIGHT
from random import randint


def free_tile_handle_click(button, mode):
    '''
    Send a arena tile event to the commands fsm which will use
    it to initiiate the correct commands to send to the server
    '''
    if mode == SDL_BUTTON_LEFT:
        print('enqueue arena tile event')
        CommandFSM.get_queue().put(Event(
            'arena_tile', position=button.position))


def jumon_tile_handle_click(button, mode):
    '''
    Send a jum on event to the commands fsm which will use
    it to initiiate the correct commands to send to the server
    '''
    if mode == SDL_BUTTON_LEFT:
        print('enqueue jumon tile event (left)')
        CommandFSM.get_queue().put(Event(
            'jumon_tile_left', position=button.position, jumon_id=button.id))
    if mode == SDL_BUTTON_RIGHT:
        print('enqueue jumon tile event (right)')
        CommandFSM.get_queue().put(Event(
            'jumon_tile_right', position=button.position, jumon_id=button.id))


class Arena:
    def __init__(self, pos, button_dimensions, width, height):
        self.pos = pos
        self.button_dimensions = button_dimensions
        self.width = width
        self.height = height
        self.jumon_buttons = []
        self.active = True
        self.tiles = []
        self.tile_texturenames = []
        for x in range(width):
            collumn = []
            for y in range(height):
                collumn.append(-1)
                self.tile_texturenames.append('ground' + str(randint(1, 6)))
            self.tiles.append(collumn)

    def activate(self):
        for button in self.jumon_buttons:
            button.observer.activate()
        self.active = True

    def deactivate(self):
        for button in self.jumon_buttons:
            button.observer.deactivate()
        self.active = False

    def set_tiles(self, tiles):
        self.tiles = tiles

    def set_tile(self, x, y, _id):
        self.tiles[x][y] = _id

    def generate(self):
        '''
        Generate the button representation
        '''
        # Clear the old representation entirely
        for button in self.jumon_buttons:
            button.observer.free()
        self.jumon_buttons.clear()
        offset = (
            self.pos[0] - self.width * self.button_dimensions[0] // 2,
            self.pos[1] - self.height * self.button_dimensions[1] // 2)
        for x in range(self.width):
            for y in range(self.height):
                rect = (
                    offset[0] + x * self.button_dimensions[0],
                    offset[1] + y * self.button_dimensions[1],
                    self.button_dimensions[0],
                    self.button_dimensions[1],
                    0)
                jumon_id = self.tiles[x][y]
                callback = None
                if jumon_id == -1:
                    callback = free_tile_handle_click
                    texture_name = self.tile_texturenames[x + y * self.width]
                    texture = Assets.get_texture(texture_name)
                elif jumon_id == -2:
                    callback = free_tile_handle_click
                    texture = Assets.get_texture('wasted')
                else:
                    callback = jumon_tile_handle_click
                    texture = Assets.get_texture(
                        MeepleDict.get_jumon(jumon_id)[0])
                sprite = TexturedSprite(Assets.get_shader(), rect, texture)
                button = Button(jumon_id, sprite, callback)
                # Append position information to the button so the
                # command fsm can use it to generate correct move commands
                # to send to the server
                button.position = (x, y)
                button.init()
                self.jumon_buttons.append(button)
        print(len(Observer.observers))

    def draw(self):
        '''
        Draw all buttons
        '''
        for button in self.jumon_buttons:
            button.draw()


if __name__ == '__main__':
    import sdl2
    import sdl2.ext
    from sdl2 import video
    import glm
    from OpenGL.GL import (
        glClear, GL_COLOR_BUFFER_BIT,
        GL_DEPTH_BUFFER_BIT, glBindVertexArray)
    from Akuga.Anudem2D.Util.EventInterface import\
        convert_sdl_to_anudem_events

    sdl2.SDL_Init(sdl2.SDL_INIT_VIDEO)
    window = sdl2.SDL_CreateWindow(
        b"Button Demo",
        sdl2.SDL_WINDOWPOS_UNDEFINED,
        sdl2.SDL_WINDOWPOS_UNDEFINED, 1220, 686,
        sdl2.SDL_WINDOW_OPENGL)
    video.SDL_GL_SetAttribute(video.SDL_GL_CONTEXT_MAJOR_VERSION, 4)
    video.SDL_GL_SetAttribute(video.SDL_GL_CONTEXT_MINOR_VERSION, 5)
    video.SDL_GL_SetAttribute(
        video.SDL_GL_CONTEXT_PROFILE_MASK,
        video.SDL_GL_CONTEXT_PROFILE_CORE)
    context = sdl2.SDL_GL_CreateContext(window)

    # Set view matrix
    view_matrix = glm.lookAt(
        glm.vec3(0, 0, 0),
        glm.vec3(0, 0, -1),
        glm.vec3(0, 1, 0))
    # projection_matrix = glm.perspective(35.0, 1.0, 0.1, 100.0)
    projection_matrix = glm.ortho(0, 1220, 686, 0)

    Assets.init_shader('default.vs', 'default.fs')
    Assets.get_shader().use()
    Assets.get_shader().upload_v_matrix(view_matrix)
    Assets.get_shader().upload_p_matrix(projection_matrix)

    Assets.add_texture('Blauta', 'jumons')
    Assets.add_texture('Plodher', 'jumons')
    Assets.add_texture('ground')

    MeepleDict.add_jumon(0, ('Blauta',))
    MeepleDict.add_jumon(1, ('Plodher',))

    arena = Arena((300, 300), (50, 50), 3, 3)
    arena.set_tile(0, 0, 0)
    arena.set_tile(1, 1, 1)
    # Even if the generate methode is invoked twice the buttons
    # wont trigger twice (not like in the buggy code I set on fire)
    # as the observers seems to be removed succesfully
    arena.generate()
    arena.generate()

    running = True
    while running:
        events = sdl2.ext.get_events()
        for event in events:
            if event.type == sdl2.SDL_QUIT:
                running = False
            convert_sdl_to_anudem_events(event)

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        arena.draw()

        glBindVertexArray(0)
        sdl2.SDL_GL_SwapWindow(window)
        sdl2.SDL_Delay(10)

    sdl2.SDL_GL_DeleteContext(context)
    sdl2.SDL_DestroyWindow(window)
    sdl2.SDL_Quit()
