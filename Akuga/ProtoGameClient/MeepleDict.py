'''
This module contains both the jumon and the artefact dictionary
which will store the jumons and artefacts in play with theire id as key
'''

JUMON_DICT = {}
ARTEFACT_DICT = {}


def add_jumon(_id, jumon):
    '''
    Add a jumon to the JUMON_DICT
    '''
    JUMON_DICT[_id] = jumon


def add_artefact(_id, artefact):
    '''
    Add an artefact to the ARTEFACT_DICT
    '''
    ARTEFACT_DICT[_id] = artefact


def get_jumon(_id):
    '''
    Get a jumon from the JUMON_DICT
    '''
    return JUMON_DICT[_id]


def get_artefact(_id):
    '''
    Get an artefact from the ARTEFACT_DICT
    '''
    return ARTEFACT_DICT[_id]
