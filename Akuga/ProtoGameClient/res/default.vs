#version 450

layout (location=0) in vec3 in_position;
layout (location=1) in vec2 in_uvcoords;

out vec2 uvcoords;

layout (location=0) uniform mat4 m_matrix; // ANUDEM2D_UFF: OBJ_DEPENDEND;
layout (location=1) uniform mat4 v_matrix; // ANUDEM2D_UFF: OBJ_INDEPENDEND;
layout (location=2) uniform mat4 p_matrix; // ANUDEM2D_UFF: OBJ_INDEPENDEND;

void main()
{
	uvcoords = in_uvcoords;
	gl_Position = (p_matrix * v_matrix * m_matrix) * vec4(in_position, 1.0);
}
