'''
This module provides access to the ressources
'''
import numpy as np
from PIL import Image
from Akuga.Anudem2D.Util.Shader import Shader
from Akuga.Anudem2D.Util.Texture2D import Texture2D
from os import getcwd, path

NAME_TEXTURE_DICT = {}
BASE_PATH = path.join(getcwd(), 'Akuga', 'ProtoGameClient', 'res')
SHADER = None


def add_texture(name, width, height, subdir=''):
    '''
    Add a texture to NAME_TEXTURE_DICT
    '''
    image = Image.open(path.join(BASE_PATH, subdir, name + '.png'))
    image_data = np.array(list(image.getdata()), dtype=np.uint8)
    texture = Texture2D(width, height, image_data)
    texture.init()
    NAME_TEXTURE_DICT[name] = texture


def get_texture(name):
    '''
    Get a texture from NAME_TEXTURE_DICT
    '''
    return NAME_TEXTURE_DICT[name]


def init_shader(vs_path, fs_path, subdir=''):
    global SHADER
    with open(path.join(BASE_PATH, subdir, vs_path)) as vs_fd:
        with open(path.join(BASE_PATH, subdir, fs_path)) as fs_fd:
            SHADER = Shader(vs_fd.read(), fs_fd.read())


def get_shader():
    return SHADER
