class StateMachieneState:
    def __init__(self, name, fsm=None):
        """
        Set the name of the state and init the state_variables as
        an empty dictionary. It will be set by the state machiene
        """
        self.name = name
        self.fsm = fsm
        self.state_variables = {}

    def set_fsm(self, fsm):
        """
        Set the fsm
        """
        self.fsm = fsm

    def run(self):
        """
        The function invoked by the state machiene if the current
        state is active.
        Returns: A tupel containing the state the state machiene should
        jump to and the informations which should be passed to
        the next state
        """
        pass
