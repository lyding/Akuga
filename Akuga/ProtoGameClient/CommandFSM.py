'''
This module will use the StateMachiene module to create a fsm
which will listen receive events from the callback functions
of the gui elements and will use them to initiate the right
commands to send to the server like 'pick jumon', summon jumon' etc...
'''
import queue
from copy import copy
from Akuga.ProtoGameClient.StateMachiene import StateMachiene
from Akuga.ProtoGameClient.StateMachieneState import StateMachieneState


command_fsm = None


class IdleState(StateMachieneState):
    def __init__(self):
        super().__init__('idle_state')

    def run(self):
        try:
            event = self.fsm.queue.get_nowait()
            if event.type == 'pick_jumon':
                return (
                    self.fsm.pick_jumon_state,
                    {'jumon_id': event.jumon_id})
            if event.type == 'jumon_to_summon_tile':
                return (
                    self.fsm.choose_summon_position_state,
                    {'jumon_id': event.jumon_id})
            if event.type == 'jumon_tile_left':
                return (
                    self.fsm.choose_move_position_state,
                    {'jumon_id': event.jumon_id})
            if event.type == 'jumon_tile_right':
                return (
                    self.fsm.choose_special_move_targets_state,
                    {'jumon_id': event.jumon_id})

        except queue.Empty:
            pass
        return None


class PickJumonState(StateMachieneState):
    '''
    Send the pick jumon command to the server and return to the idle state
    '''
    def __init__(self):
        super().__init__('pick_jumon_state')

    def run(self):
        jumon_id = self.state_variables['jumon_id']
        self.fsm.communicator.send_packet(['PICK_JUMON', jumon_id])
        return (self.fsm.idle_state, {})


class ChooseSummonPositionState(StateMachieneState):
    '''
    Choose a poisition where to summon the selected jumon
    '''
    def __init__(self):
        super().__init__('choose_summon_position_state')

    def run(self):
        try:
            event = self.fsm.queue.get_nowait()
            print('got an event (choose summon position)')
            if event.type == 'arena_tile':
                jumon_id = self.state_variables['jumon_id']
                position = event.position
                return (
                    self.fsm.summon_jumon_state,
                    {'jumon_id': jumon_id, 'position': position})
        except queue.Empty:
            pass
        return None


class SummonJumonState(StateMachieneState):
    '''
    Send the summon jumon command to the server and return to the idle state
    '''
    def __init__(self):
        super().__init__('summon_jumon_state')

    def run(self):
        jumon_id = self.state_variables['jumon_id']
        xpos, ypos = self.state_variables['position']
        self.fsm.communicator.send_packet([
            'SUMMON_JUMON',
            jumon_id,
            xpos,
            ypos])
        print('send summon jumon command')
        return (self.fsm.idle_state, {})


class ChooseMovePositionState(StateMachieneState):
    '''
    Choose a position where to move the selected jumon to
    '''
    def __init__(self):
        super().__init__('choose_move_position_state')

    def run(self):
        try:
            event = self.fsm.queue.get_nowait()
            print('got an event (choose move position)')
            if event.type == 'arena_tile' or event.type == 'jumon_tile_left':
                jumon_id = self.state_variables['jumon_id']
                position = event.position
                return (
                    self.fsm.move_jumon_state,
                    {'jumon_id': jumon_id, 'position': position})
        except queue.Empty:
            pass
        return None


class MoveJumonState(StateMachieneState):
    '''
    Send the move jumon command to the server and return to the idle state
    '''
    def __init__(self):
        super().__init__('move_jumon_state')

    def run(self):
        jumon_id = self.state_variables['jumon_id']
        position = self.state_variables['position']
        print('send move command')
        self.fsm.communicator.send_packet([
            'MOVE_JUMON',
            jumon_id,
            position[0],
            position[1]])
        return (self.fsm.idle_state, {})


class ChooseSpecialMoveTargetsState(StateMachieneState):
    '''
    Choose targets for the special move of the selected jumon
    '''
    def __init__(self):
        super().__init__('choose_special_move_targets_state')
        self.targets = []

    def run(self):
        try:
            event = self.fsm.queue.get_nowait()
            print('got an event (choose special move targets)')
            if event.type == 'arena_tile' or event.type == 'jumon_tile_left':
                self.targets.append(event.position)
            if event.type == 'jumon_tile_right':
                # The targets have to be copied to a local variabel
                # so the state variables can be cleared for the next
                # time this state is entered
                targets = copy(self.targets)
                self.targets = []
                jumon_id = self.state_variables['jumon_id']
                return (self.fsm.special_move_jumon_state, {
                    'jumon_id': jumon_id,
                    'targets': targets})
        except queue.Empty:
            pass
        return None


class SpecialMoveJumonState(StateMachieneState):
    '''
    Send the special move jumon command to the server
    and return to the idle state
    '''
    def __init__(self):
        super().__init__('special_move_jumon_state')

    def run(self):
        jumon_id = self.state_variables['jumon_id']
        targets = self.state_variables['targets']
        print('send special move command')
        self.fsm.communicator.send_packet([
            'SPECIAL_MOVE_JUMON',
            jumon_id] + targets)
        return (self.fsm.idle_state, {})


def init_command_fsm(communicator):
    '''
    Init the command fsm
    '''
    global command_fsm
    idle_state = IdleState()
    pick_jumon_state = PickJumonState()
    choose_summon_position_state = ChooseSummonPositionState()
    summon_jumon_state = SummonJumonState()
    choose_move_position_state = ChooseMovePositionState()
    move_jumon_state = MoveJumonState()
    choose_special_move_targets_state = ChooseSpecialMoveTargetsState()
    special_move_jumon_state = SpecialMoveJumonState()
    command_fsm = StateMachiene(idle_state)
    command_fsm.add_state(pick_jumon_state)
    command_fsm.add_state(choose_summon_position_state)
    command_fsm.add_state(summon_jumon_state)
    command_fsm.add_state(choose_move_position_state)
    command_fsm.add_state(move_jumon_state)
    command_fsm.add_state(choose_special_move_targets_state)
    command_fsm.add_state(special_move_jumon_state)
    command_fsm.add_data('queue', queue.Queue())
    command_fsm.add_data('communicator', communicator)


def get_queue():
    '''
    Get the command queue the fsm is listen on
    '''
    return command_fsm.queue
