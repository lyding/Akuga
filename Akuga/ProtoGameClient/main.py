import Akuga.ProtoGameClient.Assets as Assets
import Akuga.ProtoGameClient.GameState as GameState
import Akuga.ProtoGameClient.MeepleDict as MeepleDict
import Akuga.ProtoGameClient.CommandFSM as CommandFSM
from Akuga.ProtoGameClient.Network import StreamSocketCommunicator


def callback_recv_packet(communicator, callback, args):
    """
    Receive a packet and invoke the callback function with the
    received packet and args as arguments
    """
    try:
        packet = communicator.recv_packet()
    except socket.timeout:
        # If the socket runs into a timeout the time a user has to make
        # a decision has passed, so construct a packet which will trigger
        # the timeout branch within the rule building fsm
        packet = ["TIMEOUT"]
    if packet != []:
        callback(packet, *args)


def handle_server_messages(tokens):
    '''
    Handle the server messages
    '''
    cmd = tokens[0]
    print(f'Received {tokens}')
    if cmd == 'INIT_MATCH':
        # The first two tokens after the command represent
        # the arenad with and height as integers
        GameState.init_gamestate(tokens[1], tokens[2])

    if cmd == 'ADD_JUMON':
        jumon = tokens[1]
        _id = tokens[2]
        Assets.add_texture(jumon[0], 640, 768, 'jumons')
        MeepleDict.add_jumon(_id, jumon)

    if cmd == 'ADD_ARTEFACT':
        name = tokens[1]
        _id = tokens[2]
        Assets.add_textur(name, 640, 768, 'artefacts')
        MeepleDict.add_artefact(_id, name)

    if cmd == 'TURN_STARTS':
        GameState.turn_start()

    if cmd == 'TURN_ENDS':
        GameState.turn_end()

    if cmd == 'GAMESTATE':
        GameState.parse_gamestate(tokens[1])


if __name__ == '__main__':
    import sdl2
    import sdl2.ext
    from sdl2 import video
    import glm
    from OpenGL.GL import (
        glClear, GL_COLOR_BUFFER_BIT,
        GL_DEPTH_BUFFER_BIT, glBindVertexArray)
    from Akuga.Anudem2D.Util.EventInterface import\
        convert_sdl_to_anudem_events
    import socket

    connection = socket.socket(
            socket.AF_INET,
            socket.SOCK_STREAM)
    connection.connect(('akuga.nsupdate.info', 12345))
    communicator = StreamSocketCommunicator(connection, 1024)
    communicator.connection.setblocking(0)

    # Initialise the command fsm
    CommandFSM.init_command_fsm(communicator)

    sdl2.SDL_Init(sdl2.SDL_INIT_VIDEO)
    window = sdl2.SDL_CreateWindow(
        b"ProtoGameClient",
        sdl2.SDL_WINDOWPOS_UNDEFINED,
        sdl2.SDL_WINDOWPOS_UNDEFINED, 1920, 1080,
        sdl2.SDL_WINDOW_OPENGL)
    video.SDL_GL_SetAttribute(video.SDL_GL_CONTEXT_MAJOR_VERSION, 4)
    video.SDL_GL_SetAttribute(video.SDL_GL_CONTEXT_MINOR_VERSION, 5)
    video.SDL_GL_SetAttribute(
        video.SDL_GL_CONTEXT_PROFILE_MASK,
        video.SDL_GL_CONTEXT_PROFILE_CORE)
    context = sdl2.SDL_GL_CreateContext(window)

    # Set view matrix
    view_matrix = glm.lookAt(
        glm.vec3(0, 0, 0),
        glm.vec3(0, 0, -1),
        glm.vec3(0, 1, 0))
    # projection_matrix = glm.perspective(35.0, 1.0, 0.1, 100.0)
    projection_matrix = glm.ortho(0, 1920, 1080, 0)

    Assets.init_shader('default.vs', 'default.fs')
    Assets.get_shader().use()
    Assets.get_shader().upload_v_matrix(view_matrix)
    Assets.get_shader().upload_p_matrix(projection_matrix)
    Assets.add_texture('ground1', width=512, height=512)
    Assets.add_texture('ground2', width=512, height=512)
    Assets.add_texture('ground3', width=512, height=512)
    Assets.add_texture('ground4', width=512, height=512)
    Assets.add_texture('ground5', width=512, height=512)
    Assets.add_texture('ground6', width=512, height=512)
    Assets.add_texture('wasted', width=512, height=512)

    running = True
    while running:
        # SDL and Anudem event handling
        events = sdl2.ext.get_events()
        for event in events:
            if event.type == sdl2.SDL_QUIT:
                running = False
            convert_sdl_to_anudem_events(event)

        # Receive and handle server messages
        callback_recv_packet(communicator, handle_server_messages, [])
        # Run the command fsm controlled by the gui elements to
        # implement user actions
        CommandFSM.command_fsm.run()

        # Rendering
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        GameState.draw()

        glBindVertexArray(0)
        sdl2.SDL_GL_SwapWindow(window)
        sdl2.SDL_Delay(10)

    sdl2.SDL_GL_DeleteContext(context)
    sdl2.SDL_DestroyWindow(window)
    sdl2.SDL_Quit()
