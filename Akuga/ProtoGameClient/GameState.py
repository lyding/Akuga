'''
This module represents the gamestate
'''
from Akuga.ProtoGameClient.Arena import Arena
from Akuga.ProtoGameClient.PickPool import PickPool
from Akuga.ProtoGameClient.PlayerHand import PlayerHand

arena = None
pick_pool = None
player1_hand = None
player2_hand = None

arena_pos = (1920 // 2, 1080 // 2 + 25)
arena_dimension = 800
pp_pos = (1920 // 2, 0)
pp_button_dimension = (50, 50)
player1_hand_pos = (50, 686 // 2)
player1_button_dimensions = (50, 50)
player2_hand_pos = (1920 - 50, 1080 // 2)
player2_button_dimensions = (50, 50)

init = False


def turn_start():
    '''
    Activate all gui elements
    '''
    arena.activate()
    pick_pool.activate()
    player1_hand.activate()
    player2_hand.activate()


def turn_end():
    '''
    Deactivate all gui elements
    '''
    arena.deactivate()
    pick_pool.deactivate()
    player1_hand.deactivate()
    player2_hand.deactivate()


def init_gamestate(arena_width, arena_height):
    '''
    Init the gamestate representation
    '''
    global arena
    global pick_pool
    global player1_hand
    global player2_hand
    global init
    arena = Arena(
            arena_pos,
            (arena_dimension // arena_width, arena_dimension // arena_height),
            arena_width, arena_height)
    pick_pool = PickPool(pp_pos, pp_button_dimension)
    player1_hand = PlayerHand(player1_hand_pos, player1_button_dimensions)
    player2_hand = PlayerHand(player2_hand_pos, player2_button_dimensions)
    init = True


def parse_gamestate(gamestate_dict):
    '''
    Parse and therefor create the gamestate representation
    from a json_dictionary representation as it is received
    from the server
    '''
    # Get and set the arena tiles
    arena_tiles = gamestate_dict['ARENA_TILES']
    print(arena_tiles)
    arena.set_tiles(arena_tiles)
    # Get and set the jumons in the pick pool
    pp_jumons = gamestate_dict['PP_JUMONS']
    pick_pool.jumons = pp_jumons
    # Get and set the jumons in the hand of player 1
    player1_jumons = gamestate_dict['PLAYER1_HAND']
    player1_hand.jumons = player1_jumons
    # Get and set the jumons in the hand of player 2
    player2_jumons = gamestate_dict['PLAYER2_HAND']
    player2_hand.jumons = player2_jumons
    # Generate the representation with updated data
    arena.generate()
    pick_pool.generate()
    player1_hand.generate()
    player2_hand.generate()
    arena.deactivate()
    pick_pool.deactivate()
    player1_hand.deactivate()
    player2_hand.deactivate()


def draw():
    if init:
        pick_pool.draw()
        player1_hand.draw()
        player2_hand.draw()
        arena.draw()
